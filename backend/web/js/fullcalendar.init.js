var data_year = '';
var data_month = '';

function formatShowDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('-');
}

function getCalendar(){
    $('#calendar').fullCalendar({
        timeZone: 'asia/bangkok',  
        locale: 'th',
        defaultView: 'month',
        displayEventTime: false,        
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'basicWeek,month'
        },
//list-car-in-repair
//list-car-appoint-send
        events: url_list_data,

        select: function(start) {
            var selectdate = formatShowDate(start);
            $("#jmrepair-appoint_repair_date").val(selectdate);
            $(".modal-title").html("เพิ่มนัด วันที่ "+selectdate);
            $("#model-edit").modal("show");
        },

        eventClick: function(calEvent, jsEvent, view) {  
            var _uid = calEvent['id'];
            $.post(url_view_data, {
				id:_uid,
			}, function(data) {
				$("#modal-body-view").html(data);
				$("#modelView").modal("show");
			});
        },
        eventColor: '#3a87ad',
        eventLimit: true,
        selectable: true,
        eventRender: function(eventObj, $el) {
            $el.popover({
              title: eventObj.time,
              content: eventObj.title,
              trigger: 'hover',
              placement: 'top',
              container: 'body'
            });
        },
    });
}

$(document).ready(function() {
    getCalendar();
});