$(function(){
	var footer_height = $('.main-footer').outerHeight() || 0;
	var window_height = $(window).height();
	var height = window_height - footer_height - 100;

	//$('.content.body').slimscroll({height: height+'px'});
});

function LoadingShow() {
    $.LoadingOverlay("show");
}

function LoadingHide() {
    $.LoadingOverlay("hide");
}

/***********************************
 *          Sortable Input         *
 ***********************************/
function sortableData(nameID) {
    // alert($('#sale_team_9-sortable').attr('data-key'));
    var UlArray = $('#'+nameID+' li').map(function(){
        return $(this).data('key');
    });

    var acc = [];
    $.each(UlArray, function(index, value) {
        acc.push(value);
    });

    var str = JSON.stringify(acc);
    var res = str.replace('[', '');
    var res = res.replace(']', '');

    var inputName = nameID.replace('-sortable', '');
    $('#'+inputName).val(res);
}

function refreshData() {
    var ulID = [];
    $.each($('ul'), function() {
        ulID.push(this.id);
    });

    var dataUl = [];
    $.each(ulID, function(index, value) {
        if (value != '') {
            sortableData(value);
        }
    });
}

function formatDateDMY(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
}

function formatDateYMD(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}