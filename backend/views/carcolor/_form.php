<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TmColor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tm-color-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="box <?php  echo ($model->isNewRecord)?'box-success':'box-warning'?>">
    <div class="box-header with-border">
        <i class="fa <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?>"></i>
    </div>
    <div class="box-body">
    <div class="container-fluid">

	<div class="row">
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'color_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'isactive')->dropDownList(Yii::$app->Utilities->getItemIsActive()); ?>
        </div>
    </div>

    </div>
    </div>
    <div class="box-footer text-right">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app', 'Update'), ['class' =>'btn btn-success']); ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
