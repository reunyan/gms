<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TmColor */

$this->title = Yii::t('app','Create').Yii::t('app','Car Color');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Color'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tm-color-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
