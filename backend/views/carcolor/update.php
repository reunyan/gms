<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TmColor */

$this->title = Yii::t('app','Update').' ' . $model->color_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Color'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->color_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tm-color-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
