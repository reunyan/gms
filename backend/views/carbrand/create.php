<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TmBrand */

$this->title = Yii::t('app','Create').Yii::t('app','Car Brand');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Brand'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tm-brand-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
