<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TmBrand */

$this->title = Yii::t('app','Car Brand').': ' . $model->brand_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Brand'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brand_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tm-brand-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
