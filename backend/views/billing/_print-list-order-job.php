<?php
$intNumList = 15;
?>
<div class="layout_page">
<h2>ใบสั่งงานสำหรับฝ่ายช่าง</h2>
<table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="80%">
        	<center>
            <h4><?php echo Yii::$app->user->identity->comp->comp_name; ?></h4>
            <p class="pdf_company_address"><?php echo Yii::$app->user->identity->compBr->br_addr; ?></p>
            <p class="pdf_company_address">
                โทร <?php echo Yii::$app->user->identity->compBr->br_telno; ?>
                แฟกซ์ <?php echo Yii::$app->user->identity->compBr->br_faxno; ?>
                มือถือ <?php echo Yii::$app->user->identity->compBr->br_mobileno; ?>
                </p></center>
        </td>
        <td width="20%" style="padding: 10px;">
        	<strong>Job No.</strong> <?php echo $model->job_no; ?>
        </td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td width="55%">
            <table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <th width="25%" style="padding: 3px;text-align: left;">รถ</th>
                    <td width="75%" style="padding: 3px;"><?php echo $model->CarDetail; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 3px;text-align: left;">ทะเบียนรถ</th>
                    <td width="75%" style="padding: 3px;"><?php echo $model->car_license_no; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 3px;text-align: left;">การชำระเงิน</th>
                    <td width="75%" style="padding: 3px;"><?php echo Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'PAYMENTTYPE', $model->payment_type); ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 3px;text-align: left;">ประกัน</th>
                    <td width="75%" style="padding: 3px;"><?php echo (!empty($model->insurance))?$model->insurance->insure_name:''; ?></td>
                </tr>
            </table>
        </td>
        <td width="45%">
            <table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <th width="35%" style="padding: 3px;text-align: left;">วันที่นัดเข้าซ่อม</th>
                    <td width="65%" style="padding: 3px;"><?php echo $model->appoint_repair_date; ?></td>
                </tr>
                <tr>
                    <th width="35%" style="padding: 3px;text-align: left;">วันที่รถเข้าซ่อม</th>
                    <td width="65%" style="padding: 3px;"><?php echo $model->in_repair_date; ?></td>
                </tr>
                <tr>
                    <th width="35%" style="padding: 3px;text-align: left;">วันที่นัดรับรถ</th>
                    <td width="65%" style="padding: 3px;"><?php echo $model->appoint_sent_date; ?></td>
                </tr>
                <tr>
                    <th width="35%" style="padding: 3px;">&nbsp;</th>
                    <td width="65%" style="padding: 3px;">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" style="margin-top: 2px;">
	<tr>
		<td width="5%" style="font-size: 12px;"><center>เคาะ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>โป้ว</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>เตรียมพื้น</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>พ่นสีจริง</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>ประกอบ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>ขัดสี</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="5%" style="font-size: 12px;"><center>ล้างรถ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>วันที่</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>เวลา</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;">&nbsp;</td>
		<td width="5%" style="font-size: 12px;"><center>ผู้ตรวจ</center></td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
</table>

<table width="100%">
    <tr>
        <td width="40%" style="vertical-align: top;">
            <table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <th colspan="3" style="padding: 1px;">รายการซ่อม</th>
                </tr>
                <tr>
                    <th width="10%" style="padding: 1px;font-size: 12px;">ลำดับ</th>
                    <th width="70%" style="padding: 1px;font-size: 12px;">รายการ</th>
                    <th width="20%" style="padding: 1px;font-size: 12px;">หมายเหตุ</th>
                </tr>
                <?php
			    $i=1;
			    foreach ($model->repairDetail as $valRepairDetail) {
			    ?>
                <tr>
                    <td style="padding: 3px;font-size: 14px;"><center><?php echo $i++; ?></center></td>
			    	<td style="padding: 3px;font-size: 14px;"><?php echo $valRepairDetail->repair_name; ?></td>
			    	<td style="padding: 3px;font-size: 14px;"><?php echo $valRepairDetail->repair_remark; ?></td>
                </tr>
                <?php } ?>
                <?php for ($l=$i;$l <= 15;$l++) { ?>
			    <tr>
			    	<td style="padding-left: 1px;font-size: 12px;"><center><?php echo $l; ?></center></td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    </tr>
			    <?php } ?>
            </table>
        </td>
        <td width="60%" style="vertical-align: top;">
            <table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <th colspan="5" style="padding: 1px;">รายการอะไหล่</th>
                </tr>
                <tr>
                    <th width="10%" style="padding: 1px;font-size: 12px;">ลำดับ</th>
                    <th width="51%" style="padding: 1px;font-size: 12px;">รายการ</th>
                    <th width="12%" style="padding: 1px;font-size: 12px;">ประเภท</th>
                    <th width="12%" style="padding: 1px;font-size: 12px;">ผู้จัด</th>
                    <th width="20%" style="padding: 1px;font-size: 12px;">หมายเหตุ</th>
                </tr>
                <?php
			    $j=1;
			    foreach ($model->repairPart as $valRepairPart) {
			    ?>
			    <tr>
			    	<td style="padding: 3px;font-size: 14px;"><center><?php echo $j++; ?></center></td>
			    	<td style="padding: 3px;font-size: 14px;"><?php echo $valRepairPart->part_name; ?></td>
			    	<td style="padding: 3px;font-size: 14px;"><center><?php echo Yii::$app->Utilities->getDescReferance('PART', 'PARTTYPE', $valRepairPart->part_type); ?></center></td>
			    	<td style="padding: 3px;font-size: 14px;"><center><?php echo $valRepairPart->part_owner_name; ?></center></td>
			    	<td style="padding: 3px;font-size: 14px;"><?php echo $valRepairPart->part_remark; ?></td>
			    </tr>
			    <?php } ?>
                <?php for ($p=$j;$p <= 15;$p++) { ?>
			    <tr>
			    	<td style="padding-left: 1px;font-size: 12px;"><center><?php echo $p; ?></center></td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    	<td style="padding: 1px;font-size: 12px;">&nbsp;</td>
			    </tr>
			    <?php } ?>
            </table>
        </td>
    </tr>
</table>

<table width="100%" style="border: 1px solid;">
	<tr>
		<th width="35%" style="padding: 1px;border-bottom:1px solid;"><center>Note การถ่ายรูป</center></th>
		<th colspan="2" width="65%" style="padding: 1px;border-bottom:1px solid;border-left:1px solid;"><center>QC</center></th>
	</tr>
	<tr>
		<td style="vertical-align: top;">
			<table width="100%" style="font-size: 13px;">
				<tr>
					<td width="45%"><input type="checkbox"> ก่อนซ่อม</td>
					<td width="65%"><input type="checkbox"> เทียบอะไหล่เปลี่ยน</td>
				</tr>
				<tr>
					<td><input type="checkbox"> ทำสี</td>
					<td><input type="checkbox"> รถเสร็จ</td>
				</tr>
				<tr>
					<td colspan="2" style="font-size: 18px;"><input type="checkbox"> คืนซากทุกชิ้น</td>
				</tr>
			</table>
		</td>
		<td style="border-left:1px solid;vertical-align: top;" >
			<table width="100%" style="font-size: 12px;">
				<tr>
					<td width="50%"><input type="checkbox"> สี</td>
					<td width="50%"><input type="checkbox"> ความสะอาด</td>
				</tr>
				<tr>
					<td width="50%"><input type="checkbox"> การประกอบ</td>
					<td width="50%"><input type="checkbox"> เปลี่ยนอะไหล่</td>
				</tr>
				<tr>
					<td width="50%"><input type="checkbox"> ผ้ายาง</td>
					<td width="50%"><input type="checkbox"> ระบบไฟฟ้า</td>
				</tr>
			</table>
		</td>
		<td style="vertical-align: top;" >
			<table width="100%" style="font-size: 12px;">
				<tr>
					<td width="50%"><input type="checkbox"> กันแมลง+กาบ</td>
					<td width="50%"><input type="checkbox"> ยางอะไหล่/เครื่องมือ</td>
				</tr>
				<tr>
					<td width="50%"><input type="checkbox"> วิทยุ+แอร์</td>
					<td width="50%"><input type="checkbox"> เครื่องยนต์</td>
				</tr>
				<tr>
					<td width="50%"><input type="checkbox"> สติ๊กเกอร์-ตัวหนังสือ</td>
					<td width="50%"><input type="checkbox"> สเกิร์ต</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="100%" style="border: 1px solid;">
	<tr>
		<th width="60%" style="padding: 3px;border-bottom:1px solid;"><center>Note ทั่วไป/รายการเพิ่มเติมพิเศษ</center></th>
		<td rowspan="2" style="padding: 3px;vertical-align: bottom;border-left:1px solid;"><center>ผู้ตรวจ QC</center></td>
		<td rowspan="2" style="padding: 3px;vertical-align: bottom;border-left:1px solid;"><center><?php echo (!empty($model->reciveCarEmp))?$model->reciveCarEmp->profile->first_name:''; ?><br><br>พนักงานรับรถ</center></td>
	</tr>
	<tr>
		<td>
			<table width="100%">
				<tr>
					<td width="50%">&nbsp;</td>
					<td width="50%">&nbsp;</td>
				</tr>
				<tr>
					<td width="50%">&nbsp;</td>
					<td width="50%">&nbsp;</td>
				</tr>
				<tr>
					<td width="50%">&nbsp;</td>
					<td width="50%">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>