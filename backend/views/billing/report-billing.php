<?php

use yii\helpers\Html;

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JmRepairSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Report Billing');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jm-repair-index">
    <?php echo $this->render('_search-report-billing', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            // 'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            '{export}',
            '{toggleData}',
        ],
        'exportConfig' => [
            GridView::CSV => true,
            GridView::EXCEL=> true,
        ],
        // 'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'showPageSummary' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'label'=>'Job no',
                'value'=>function($model){
                    return $model->jmRepair->job_no;
                }
            ],
            [
                'label'=>'ทะเบียนรถ',
                'value'=>function($model){
                    return $model->jmRepair->car_license_no;
                }
            ],
            [
                'label'=>'ข้อมูลรถ',
                'value'=>function($model){
                    return ($model->jmRepair->CarDetail)?$model->jmRepair->CarDetail:'';
                }
                // 'mergeHeader' => true,
            ],
            [
                'label'=>'วันที่เข้าซ่อม',
                'value'=>function($model){
                    return $model->jmRepair->in_repair_date;
                }
            ],
            [
                'label'=>'วันที่รส่งรถ',
                'value'=>function($model){
                    return $model->jmRepair->sent_car_date;
                }
            ],
            [
                'label'=>'ประกันภัย',
                'value'=>function($model){
                    return ($model->jmRepair->insurance)?$model->jmRepair->insurance->insure_name:'';
                }
            ],
            'policy_no',
            'notify_no',
            'claim_no',
            [
                'attribute' => 'repair_amt',
                'hAlign' => 'right',
                'vAlign' => 'middle',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            [
                'attribute' => 'part_amt',
                'hAlign' => 'right',
                'vAlign' => 'middle',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            'before_billing_date',
            'billing_no',
            'billing_date',
            'invoice_no',
            'receive_date',
            'receive_at',
            [
                'attribute'=>'receive_by',
                'value'=>function($model){
                    return (!empty($model->receiveBy))?$model->receiveBy->nickname:null;
                }
            ],
            [
                'label'=>'พนักงานรับรถ',
                'value'=>function($model){
                    return !empty($model->jmRepair->reciveCarEmp)?$model->jmRepair->reciveCarEmp->nickname:null;
                }
            ],
            [
                'attribute'=>'billing_status',
                'value'=>function($model){
                    return (!empty($model->billingStatus))?$model->billingStatus->ref_desc:$model->billing_status;
                }
            ],
        ],
    ]); ?>

</div>
