<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;

$this->title = Yii::t('app', 'List Billing');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jm-repair-index">
    <?php echo $this->render('_search_biiling_claim', ['model' => $searchModel]); ?>

    <?php
    $form = ActiveForm::begin([
        'id' => 'form-index-billing',
        'type' => ActiveForm::TYPE_HORIZONTAL, //TYPE_HORIZONTAL
        'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
        'options'=>['target'=>(($searchModel->billing_status==3)?'_blank':'')],
    ]);
    ?>

    <?php switch ($searchModel->billing_status) {
    	case 2:
            $dateReceive = $form->field($searchModel, 'receive_date')->widget(DatePicker::classname(), [
                'options' => [
                    'placeholder' => 'วัน-เดือน-ปี',
                    'value'=>date('d-m-Y')
                ],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
			$buttonText = 'รับเงินแล้ว';
			// $button     = '<div class="text-right">'.Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', $buttonText), ['class' =>'btn btn-success']).'</div>';
            $button     = '<div class="row">'.
                            '<div class="col-sm-12 col-md-5 text-left">'.
                            Html::a('<i class="fa fa-save"></i> '.Yii::t('app', 'พิมพ์ใบแจ้งหนี้'), 'javascript:void(0);', ['class' =>'btn btn-info', 'onClick'=>'printBilling();']).
                            '&nbsp;'.
                            Html::a('<i class="fa fa-save"></i> '.Yii::t('app', 'พิมพ์ใบกำกับภาษี/ใบเสร็จรับเงิน'), 'javascript:void(0);', ['class' =>'btn btn-primary', 'onClick'=>'printReceipts();']).
                            '</div>'.
                            '<div class="col-sm-8 col-md-5 text-left">'.$dateReceive.'</div>'.
                            '<div class="col-sm-4 col-md-2 text-right">'.Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', $buttonText), ['class' =>'btn btn-success']).'</div>'.
                        '</div>';
			$title      = Yii::t('app', 'On Billing');
    	break;

    	case 3:
            $buttonText = '';
			$button = '';
			$title  = Yii::t('app', 'Complete Billing');
    	break;

        case 4:
            $dateBilling = $form->field($searchModel, 'billing_date')->widget(DatePicker::classname(), [
                'options' => [
                    'placeholder' => 'วัน-เดือน-ปี',
                    'value'=>date('d-m-Y')
                ],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
            $buttonText = 'ดำเนินการวางบิล';
            $button     = '<div class="row">'.
                            '<div class="col-md-6 test-lect">'.Html::a('<i class="fa fa-save"></i> '.Yii::t('app', 'พิมพ์ใบแจ้งหนี้'), 'javascript:void(0);', ['class' =>'btn btn-info', 'onClick'=>'printBilling();']).'</div>'.
                            '<div class="col-md-4">'.$dateBilling.'</div>'.
                            '<div class="col-md-2 text-center">'.Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', $buttonText), ['class' =>'btn btn-success']).'</div>'.
                        '</div>';
            $title      = Yii::t('app', 'Wait Billing');
        break;

    	case 1:
    	default:
    		$dateBilling = $form->field($searchModel, 'process_date')->widget(DatePicker::classname(), [
                'options' => [
                	'placeholder' => 'วัน-เดือน-ปี',
                	'value'=>date('d-m-Y')
                ],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
            $selectBilling = $form->field($searchModel, 'select_process_status')->dropDownList(Yii::$app->Utilities->getItemClaimBillingProcess())->label(false);
			$buttonText = 'ดำเนินการ';
			$button     = '<div class="row">'.
							'<div class="col-md-4"></div>'.
                            '<div class="col-md-6 form-group">'.
                                '<div class="col-md-4 text-right">'.$selectBilling.'</div>'.
                                '<div class="col-md-8">'.$dateBilling.'</div>'.
                            '</div>'.
							'<div class="col-md-2 text-left">'.Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', $buttonText), ['class' =>'btn btn-success']).'</div>'.
						'</div>';
			$title      = Yii::t('app', 'Wait Billing');
    	break;
    }
    ?>

    <?php echo $form->field($searchModel, 'billing_status')->hiddenInput()->label(false); ?>

	<div class="row">
		<div class="col-sm-4 col-md-3">
			<div class="info-box">
				<span class="info-box-icon bg-purple"><i class="fa fa-clock-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text"><?php echo Html::a(Yii::t('app', 'Wait Billing'), ['index', 'JmRepairClaimSearch[billing_status]'=>1]); ?></span>
					<span class="info-box-number"><?php echo $searchModel->numStatusWait(); ?></span>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="info-box">
				<span class="info-box-icon bg-blue"><i class="fa fa-share-square-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text"><?php echo Html::a(Yii::t('app', 'On Invoice'), ['index', 'JmRepairClaimSearch[billing_status]'=>4]); ?></span>
					<span class="info-box-number"><?php echo $searchModel->numStatusOnInv(); ?></span>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-orange"><i class="fa fa-share-square-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text"><?php echo Html::a(Yii::t('app', 'On Billing'), ['index', 'JmRepairClaimSearch[billing_status]'=>2]); ?></span>
					<span class="info-box-number"><?php echo $searchModel->numStatusOn(); ?></span>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-md-3">
			<div class="info-box">
				<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-green"><i class="fa fa-check-circle-o"></i></span>
				<div class="info-box-content">
					<span class="info-box-text"><?php echo Html::a(Yii::t('app', 'Complete Billing'), ['index', 'JmRepairClaimSearch[billing_status]'=>3]); ?></span>
					<span class="info-box-number"><?php echo $searchModel->numStatusComplete(); ?></span>
				</div>
			</div>
		</div>
	</div>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> 'รายการ'.$title,
            // 'before'=> false,
            'after'=> $button,
        ],
        'toolbar'=> [
            '{toggleData}',
        ],
        // 'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
        	[
			    'class' => 'kartik\grid\CheckboxColumn',
			    'headerOptions' => ['class' => 'kartik-sheet-style'],
			    'visible'=>function($model){
			    	return ($model->billing_status == 3) ? false : true;
			    },
			],
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'label'=>'Job no',
                'value'=>function($model){
                    return $model->jmRepair->job_no;
                }
            ],
            [
                'label'=>'ทะเบียนรถ',
                'value'=>function($model){
                    return $model->jmRepair->car_license_no;
                }
            ],
            [
                'label'=>'ข้อมูลรถ',
                'value'=>function($model){
                    return ($model->jmRepair->CarDetail)?$model->jmRepair->CarDetail:'';
                }
                // 'mergeHeader' => true,
            ],
            [
                'label'=>'วันที่เข้าซ่อม',
                'value'=>function($model){
                    return $model->jmRepair->in_repair_date;
                }
            ],
            [
                'label'=>'วันที่รส่งรถ',
                'value'=>function($model){
                    return $model->jmRepair->sent_car_date;
                }
            ],
            [
                'label'=>'ประกันภัย',
                'value'=>function($model){
                    return ($model->jmRepair->insurance)?$model->jmRepair->insurance->insure_name:'';
                }
            ],
            'policy_no',
            'notify_no',
            'claim_no',
            'repair_amt',
            'part_amt',
            'first_claim_amt',
            'before_billing_date',
            'billing_no',
            'billing_date',
            'invoice_no',
            'receive_date',
            [
                'attribute'=>'billing_status',
                'value'=>function($model){
                    return (!empty($model->billingStatus))?$model->billingStatus->ref_desc:$model->billing_status;
                }
            ],

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {edit} {print-invoice} {print}',// {cancel} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="fa fa-eye"></span>', [
                                    'title' => Yii::t('app', 'แสดงข้อมูล'),
                                    'class' => 'btn btn-info btn-xs',
                                    'onClick' => 'OpenViewDetail('.$model->id.')',
                        ]);
                    },

                    'edit' => function ($url, $model) {
                        return Html::button('<span class="fa fa-edit"></span>', [
                                    'title' => Yii::t('app', 'แก้ไขข้อมูล'),
                                    'class' => 'btn btn-warning btn-xs',
                                    'onClick' => 'OpenEditBilling('.$model->id.')',
                        ]);
                    },

                    'print' => function ($url, $model) {
                        return Html::a('<span class="fa fa-print"></span>',['/billing/print-billing', 'id'=>$model->id], [
                                    'title' => Yii::t('app', 'ใบเสร็จ/ใบกำกับภาษี'),
                                    'class' => 'btn btn-warning btn-xs',
                                    'target' => '_blank',
                        ]);
                    },

                    'print-invoice' => function ($url, $model) {
                        return Html::a('<span class="fa fa-print"></span>',['/billing/print-invoice', 'id'=>$model->id], [
                                    'title' => Yii::t('app', 'ใบแจ้งหนี้'),
                                    'class' => 'btn btn-info btn-xs',
                                    'target' => '_blank',
                        ]);
                    },

                    /*'cancel' => function ($url, $model) {
                        return Html::a('<span class="fa fa-remove"></span>',['/billing/cancel-billing', 'id'=>$model->id], [
                                    'title' => Yii::t('app', 'cancel'),
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to cancel this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                        ]);
                    },*/

                    /*'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-trash"></span>',['/billing/delete-billing', 'id'=>$model->id], [
                                    'title' => Yii::t('app', 'delete'),
                                    'class' => 'btn btn-danger btn-xs',
									'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
									'data-method' => 'post',
									'data-pjax' => '0',
                        ]);
                    },*/
                ],
                'visibleButtons' => [
                    'print' => function($model, $key, $index) {
                        return (empty($model->invoice_no)) ? false : true;
                    },
                    'print-invoice' => function($model, $key, $index) {
                        return (empty($model->billing_no)) ? false : true;
                    },
                	'edit' => function($model, $key, $index) {
				    	return ($model->billing_status === 1 || $model->billing_status === 2) ? true : false;
                	},
                	/*'delete' => function($model, $key, $index) {
				    	return $model->billing_status === 1 ? true : false;
                	},*/
                    /*'cancel' => function($model, $key, $index) {
                        return $model->billing_status === 2 ? true : false;
                    },*/
                ],
            ],
        ],
    ]); ?>

<?php ActiveForm::end(); ?>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalViewDetail">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">รายละเอียด</h4>
        </div>
        <div class="modal-body">Detail</div>
        <div class="modal-footer">
            <?php echo Html::button(Yii::t('app','Save'), ['class'=>'btn btn-success', 'style'=>'display: none;', 'id'=>'btnSaveBilling', 'onClick'=>'saveUpdateBilling();']); ?>
            <?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function OpenViewDetail(id){
        $("#modalViewDetail").modal("show");
        $.post("<?php echo Url::to(['/repaircar/view']);?>", {id:id}, function(data) {
            $("#btnSaveBilling").css('display','none');
            $(".modal-title").html('รายละเอียด');
            $(".modal-body").html(data);
        });

    }

    function OpenEditBilling(id){
        $("#modalViewDetail").modal("show");
        $.post("<?php echo Url::to(['/billing/update-billing']);?>", {id:id}, function(data) {
            $("#btnSaveBilling").css('display','');
            $(".modal-title").html('แก้ไขข้อมูล');
            $(".modal-body").html(data);
        });
    }

    function saveUpdateBilling(){
        if (confirm('ยืนยันการแก้ไขใช่หรือไม่?')) {
            $.post("<?php echo Url::to(['/billing/save-update-billing']);?>", $('#form-update-billing').serialize(), function(data) {
                $("#btnSaveBilling").css('display','none');
                $("#modalViewDetail").modal("hide");
            }).success(function() {
                window.location.reload();
            });
        }
    }

    function printReceipts(){
        var receipts = [];
        $.each($("input[name='selection[]']:checked"), function(){
            receipts.push($(this).val());
        });
        $.session.set("print_id", receipts);
        console.log($.session.get('print_id'));
        window.open("<?php echo Url::to(['/billing/print-billing-select']);?>");
    }

    function printBilling(){
        var receipts = [];
        $.each($("input[name='selection[]']:checked"), function(){
            receipts.push($(this).val());
        });
        $.session.set("print_id", receipts);
        console.log($.session.get('print_id'));
        window.open("<?php echo Url::to(['/billing/print-invoice-select']);?>");
    }
    
</script>