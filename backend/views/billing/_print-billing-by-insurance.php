<div class="layout_page">
<table width="100%" style="border-bottom: solid 1px;">
    <tr>
        <td><center>
            <h2><?php echo Yii::$app->user->identity->comp->comp_name; ?></h2>
            <p class="pdf_company_address"><?php echo Yii::$app->user->identity->compBr->br_addr; ?></p>
            <!-- <p class="pdf_company_address">
                โทร <?php //echo Yii::$app->user->identity->compBr->br_telno; ?>
                แฟกซ์ <?php //echo Yii::$app->user->identity->compBr->br_faxno; ?>
                มือถือ <?php //echo Yii::$app->user->identity->compBr->br_mobileno; ?>
                E-Mail <?php //echo Yii::$app->user->identity->compBr->br_email; ?>
                </p> -->
            </center>
        </td>
    </tr>
</table>
<table width="100%" style="margin-top: 30px;">
    <tr>
        <td><h2><center>ใบวางบิล</center></h2></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td style="padding: 10px;" width="70%"><?php echo $model[0]['insurance']; ?></td>
        <td style="padding: 10px;" width="30%">วันที่ <?php echo date('d-m-Y');//echo date('d-m-Y', strtotime($model[0]['billing_date'])); ?></td>
    </tr>
</table>
<table class="table_bordered" style="margin-top: 10px;" width="100%" border="0" cellpadding="2" cellspacing="0">
    <thead>
        <tr>
            <th style="padding: 5px;" width="5%"><center>ลำดับ</center></th>
            <th style="padding: 5px;" width="13%"><center>เลขที่เคลม</center></th>
            <th style="padding: 5px;" width="13%"><center>กรมธรรม์</center></th>
            <th style="padding: 5px;" width="10%"><center>ป/ค</center></th>
            <th style="padding: 5px;" width="13%"><center>ทะเบียนรถ</center></th>
            <th style="padding: 5px;" width="15%"><center>จำนวนเงิน</center></th>
            <th style="padding: 5px;" width="13%"><center>ใบภาษี</center></th>
            <th style="padding: 5px;" width="15%"><center>ค่า Ex.</center></th>
            <th style="padding: 5px;" width="13%"><center>หมายเหตุ</center></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $i=0;
        $sumTotal=0;
        $sumFirstclaimTotal=0;
        foreach ($model as $valData) {
            $sumRepair     = $valData['repair_amt'];
            $sumFirstclaim = $valData['first_claim_amt'];
    ?>
        <tr>
            <td style="padding: 3px;"><center><?php echo ++$i;?></center></td>
            <td style="padding: 3px;"><center><?php echo $valData['claim_no'];?></center></td>
            <td style="padding: 3px;"><center><?php echo $valData['policy_no'];?></center></td>
            <td style="padding: 3px;"><center><?php echo Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'CARCLAIMTYPE', $valData['car_claim_type']);?></center></td>
            <td style="padding: 3px;"><center><?php echo $valData['car_license_no'];?></center></td>
            <td style="padding: 3px;text-align: right;"><?php echo number_format($sumRepair, 2);?></td>
            <td style="padding: 3px;"><center><?php echo $valData['invoice_no'];?></center></td>
            <td style="padding: 3px;text-align: right;"><?php echo number_format($sumFirstclaim, 2);?></td>
            <td style="padding: 3px;"><center><?php echo $valData['job_no'];?></center></td>
        </tr>
    <?php
        $sumFirstclaimTotal += $sumFirstclaim;
        $sumTotal += $sumRepair;
    }?>
    </tbody>
    <tfoot>
        <tr>
            <th style="padding: 5px;" colspan="4"><center>รวมทั้งหมด <?php echo $i;?> รายการ</center></th>
            <th style="padding: 5px;"><center>รวมสุทธิ</th>
            <th style="padding: 5px;text-align: right;"><?php echo number_format($sumTotal, 2);?></th>
            <th style="padding: 5px;">&nbsp;</th>
            <th style="padding: 5px;text-align: right;"><?php echo number_format($sumFirstclaimTotal, 2);?></th>
            <th style="padding: 5px;">&nbsp;</th>
        </tr>
    </tfoot>
</table>
<table width="100%" style="margin-top: 50px;">
    <tr>
        <td width="13%" style="text-align: right;">ลงชื่อ</td>
        <td width="20%" style="border-bottom: dotted 1px;">&nbsp;</td>
        <td width="13%" style="text-align: left;">ผู้วางบิล</td>
        <td width="8%">&nbsp;</td>
        <td width="13%" style="text-align: right;">ลงชื่อ</td>
        <td width="20%" style="border-bottom: dotted 1px;">&nbsp;</td>
        <td width="13%" style="text-align: left;">ผู้นับวางบิล</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right;">วันที่วางบิล</td>
        <td style="border-bottom: dotted 1px;">&nbsp;</td>
        <td >&nbsp;</td>
        <td >&nbsp;</td>
        <td style="text-align: right;">วันที่รับวางบิล</td>
        <td style="border-bottom: dotted 1px;">&nbsp;</td>
        <td >&nbsp;</td>
    </tr>
</table>
</center>