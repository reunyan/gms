<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepairSearch */
/* @var $form yii\widgets\ActiveForm */
$layout = <<< HTML
    {input1}
    {separator}
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="fa fa-remove"></i>
    </span>
HTML;
?>

<div class="jm-repair-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-billing'],
        'method' => 'get',
    ]); ?>

    <div class="box box-default">
    <div class="box-header with-border"><i class="fa fa-search"></i></div>

    <div class="box-body">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                    'data'=> Yii::$app->Utilities->getItemInsureName(),
                    ]);
                ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'policy_no')->textInput(['maxlength' => true])->label('เลขที่กรมธรรม์'); ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'claim_no')->textInput(['maxlength' => true])->label('เลขที่เคลม'); ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'car_license_no')->textInput(['maxlength' => true])->label('ทะเบียนรถ'); ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'job_no')->textInput(['maxlength' => true])->label('Job No'); ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php
                    echo $form->field($model, 'dateBillingStart')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_RANGE,
                        'attribute2' => 'dateBillingEnd',
                        'separator' => 'ถึง',
                        'layout' => $layout,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ])->label('วันที่วางบิล');
                ?>
            </div>
            <div class="col-sm-4 col-md-3">
                <?php
                echo $form->field($model, 'dateReceiveStart')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_RANGE,
                    'attribute2' => 'dateReceiveEnd',
                    'separator' => 'ถึง',
                    'layout' => $layout,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ])->label('วันที่รับเงิน');
            ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php
                echo $form->field($model, 'in_repair_date_start')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_RANGE,
                    'attribute2' => 'in_repair_date_end',
                    'separator' => 'ถึง',
                    'layout' => $layout,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ])->label('วันที่จอดซ่อม');
            ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'in_repair_emp')->widget(Select2::classname(), [
                    'data'=> Yii::$app->Utilities->getItemEmployeeName(),
                    ])->label('ชื่อพนักงานรับรถ');;
                ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'billing_status')->dropDownList(Yii::$app->Utilities->getItemBillingStatus()); ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php
                echo $form->field($model, 'receive_at_start')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_RANGE,
                    'attribute2' => 'receive_at_end',
                    'separator' => 'ถึง',
                    'layout' => $layout,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd-mm-yyyy'
                    ]
                ])->label('วันที่กดรับเงิน');
            ?>
            </div>
        </div>
    </div>

    </div>

    <div class="box-footer">
        <div class="col-md-12 text-right">
            <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
