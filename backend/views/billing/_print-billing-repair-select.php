<div class="layout_page">
<?php 
$intPage        = 0;
$intNumPrint    = count($models);
foreach ($models as $model) {
    $intPage++;
?>
<table width="100%">
    <tr>
        <!-- <td style="padding: 10px;" align="right"><img src="<?php //echo Yii::getAlias('@web').'/imgs/logo_comp.png' ?>" style="margin-top: 20px;"></td> -->
        <td>
            <center>
            <h1><?php echo Yii::$app->user->identity->comp->comp_name; ?></h1>
            <br>
            <p class="pdf_company_address">
                <?php echo Yii::$app->user->identity->compBr->br_name; ?>
                <?php echo Yii::$app->user->identity->compBr->br_addr; ?>
            </p>
            <br>
            <!-- <p class="pdf_company_address">
                โทร <?php //echo Yii::$app->user->identity->compBr->br_telno; ?>
                แฟกซ์ <?php //echo Yii::$app->user->identity->compBr->br_faxno; ?>
                E-mail <?php //echo Yii::$app->user->identity->compBr->br_email; ?>
            </p> -->
            </center>
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td style="padding: 10px;" width="100%"><h3><center>ใบกำกับภาษี/ใบเสร็จรับเงิน</center></h3></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td width="75%" style="border: 1px solid #000;padding-top: 3px;padding-bottom: 20px;" >
            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="16%" style="font-weight: bold;padding: 5px;vertical-align: top;">นามผู้ซื้อ</td>
                    <td width="84%" style="padding: 5px;vertical-align: top;"><?php echo ($model->jmRepair->insurance)?$model->jmRepair->insurance->insure_name:''; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;padding: 5px;vertical-align: top;">ที่อยู่</td>
                    <td style="padding: 5px;vertical-align: top;"><?php echo ($model->jmRepair->insurance)?nl2br($model->jmRepair->insurance->insure_addr):''; ?></td>
                </tr>
            </table>
        </td>
        <td style="padding: 5px;vertical-align: top;" width="25%">
            <center>
                <h4>เลขที่ <?php echo $model->invoice_no; ?></h4>
                <br>
                เลขประจำตัวผู้เสียภาษี
                <br><br>
                <?php echo Yii::$app->user->identity->compBr->br_tax_id; ?>
                <br><br>
                <?php echo date('d/m/Y', strtotime($model->billing_date)); ?>
            </center>
        </td>
    </tr>
</table>
<table class="table_bordered" style="margin-top: 10px;" width="100%" border="0" cellpadding="2" cellspacing="0">
    <thead>
    <tr>
        <th style="padding: 10px 5px;" width="10%"><center>ลำดับที่</center></th>
        <th style="padding: 10px 5px;" width="50%"><center>รายการ</center></th>
        <th style="padding: 10px 5px;" width="20%"><center>จำนวนหน่วย</center></th>
        <th style="padding: 10px 5px;" width="20%"><center>จำนวนเงิน</center></th>
    </tr>
    </thead>
    <?php
            if ($model->repair_amt > 0) {
                $intSum = ($model->repair_amt/107)*100;
    ?>
                <tr>
                    <td style="padding: 10px;"><center>1</center></td>
                    <td style="padding: 10px;">ค่าแรงซ่อมรถ <?php echo $model->jmRepair->car_license_no;?> <?php echo (!empty($model->jmRepair->carProvince))?$model->jmRepair->carProvince->province_name:'';?></td>
                    <td style="padding: 10px;"><center>1</center></td>
                    <td style="padding: 10px;text-align: right;"><?php echo number_format($intSum, 2);?></td>
                </tr>
    <?php }
        // if (!empty($model->jmRepair->insurance) && in_array($model->jmRepair->insurance->id, [20, 34, 36])) {
    ?>
        <tr>
            <td style="padding: 10px;">&nbsp;</td>
            <td style="padding: 10px;">เคลมเลขที่ <?php echo $model->claim_no ?></td>
            <td style="padding: 10px;">&nbsp;</td>
            <td style="padding: 10px;">&nbsp;</td>
        </tr>
    <?php
        // }
        for ($i=1; $i <= 4; $i++) {
    ?>
        <tr>
            <td style="padding: 10px;">&nbsp;</td>
            <td style="padding: 10px;">&nbsp;</td>
            <td style="padding: 10px;">&nbsp;</td>
            <td style="padding: 10px;">&nbsp;</td>
        </tr>
    <?php }

        $intVat = ($intSum * 7)/100;
        $intNet = $intSum + $intVat;
    ?>
    <tr>
        <td colspan="3" style="padding: 10px;text-align: right;font-weight: bold;">รวมราคาทั้งสิ้น</td>
        <td style="padding: 10px;border:1px solid;text-align: right;"><?php echo number_format($intSum, 2);?></td>
    </tr>
    <tr>
        <td colspan="3" style="padding: 10px;text-align: right;font-weight: bold;">จำนวนภาษีมูลค่าเพิ่ม</td>
        <td style="padding: 10px;border:1px solid;text-align: right;"><?php echo number_format($intVat, 2);?></td>
    </tr>
    <tr>
        <td colspan="3" style="padding: 10px;text-align: right;font-weight: bold;">จำนวนเงินรวมทั้งสิ้น</td>
        <td style="padding: 10px;border:1px solid;text-align: right;"><?php echo number_format($intNet, 2);?></td>
    </tr>
</table>
<table width="100%" style="margin-top: 20px;">
    <tr>
        <td colspan="4" style="padding: 10px;text-align: left;font-weight: bold;padding-left: 50px;">(<?php echo Yii::$app->Helpers->thaiAmount(number_format($intNet,2)); ?>)</td>
    </tr>
</table>
<table width="100%" style="margin-top: 50px;">
    <tr>
        <td width="5%">&nbsp;</td>
        <td width="35%"><center>.........................................................................</center></td>
        <td width="20%">&nbsp;</td>
        <td width="35%"><center>.........................................................................</center></td>
        <td width="5%">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="font-size: 16px;"><center>ผู้รับสินค้า</center></td>
        <td>&nbsp;</td>
        <td style="font-size: 16px;"><center>ผู้ได้รับมอบอำนาจ/ผู้รับเงิน</center></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><center>....................../....................../......................</center></td>
        <td>&nbsp;</td>
        <td><center>.......<span style="font-size: 16px;"><?php echo date('d')?></span>........./..........<span style="font-size: 16px;"><?php echo date('m')?></span>........./..........<span style="font-size: 16px;"><?php echo date('Y')?></span>..........</center></td>
        <td>&nbsp;</td>
    </tr>
</table>

<?php 
    if ($intPage < $intNumPrint) {echo '<pagebreak>';}
}
?>
</div>