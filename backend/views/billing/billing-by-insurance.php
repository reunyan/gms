<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JmRepairSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List Billing By Insurance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Billing'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('_search-billing-insurance', ['model'=>$model]) ?>

<?php 
$form = ActiveForm::begin([
    'action' => ['print-billing-by-insurance'],
    'method' => 'post',
    // 'target' => '_blank',
    // 'type' => ActiveForm::TYPE_HORIZONTAL, //TYPE_HORIZONTAL
    // 'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_SMALL],
    //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
]);

if (!empty($model->insurance_id)) {
    $strAfterPanel = '<div class="text-right">'.Html::submitButton('<span class="fa fa-print"></span> พิมพ์', ['class'=>'btn btn-success', 'formtarget'=>'_blank']).'</div>';
} else {
    $strAfterPanel = false;
}

echo Html::hiddenInput('insurance', $model->insurance_id);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'panel'=>[
        'type'=>GridView::TYPE_PRIMARY,
        'heading'=> 'รายการ',
        'before'=> false,
        'after'=> $strAfterPanel,
    ],
    'bordered' => true,
    'striped' => true,
    'responsive' => false,
    'responsiveWrap' => false,
    'floatHeader' => true,
    'floatOverflowContainer' => true,
    // 'perfectScrollbar'=>true,
    'headerRowOptions' => ['class' => 'info'],
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'columns' => [
        [
            'class' => '\kartik\grid\CheckboxColumn',
            'name'=> 'billing_date',
            'checkboxOptions'=> function ($model) {
                return [
                    'value' => date('Y-m-d', strtotime($model->billing_date)),
                    'style' => 'width:20px; height:20px;',
                    'disabled' => empty($model->insurance_id),
                ];
            }
        ],
        'billing_date',
        'insurance',
        'sum_repair_amt',
        'sum_part_amt',
        'count_claim',
        ['class' => 'kartik\grid\ActionColumn',
            'template'=>'{print} ',
            'buttons' => [
                'print' => function ($url, $model) {
                    return Html::a('<span class="fa fa-print"></span>',['/billing/print-billing-by-insurance', 'billing_date'=>date('Y-m-d', strtotime($model->billing_date)), 'insurance'=>$model->insurance_id], [
                                'title' => Yii::t('app', 'print'),
                                'class' => 'btn btn-warning btn-xs',
                                'target' => '_blank',
                    ]);
                },
            ],
            'visibleButtons' => [
            	'print' => function($model, $key, $index) {
			    	return $model->billing_status === 1 ? false : true;
            	},
            	'cancel' => function($model, $key, $index) {
			    	return $model->billing_status === 2 ? true : false;
            	}
            ],
        ],
    ],
]); ?>
<?php ActiveForm::end();?>