<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
?>

<?php
$layout = <<< HTML
    {input1}
    {separator}
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="fa fa-remove"></i>
    </span>
HTML;

$form = ActiveForm::begin([
    'action' => ['billing-by-insurance'],
    'method' => 'post',
    // 'type' => ActiveForm::TYPE_HORIZONTAL, //TYPE_HORIZONTAL
    // 'formConfig' => ['labelSpan' => 4, 'deviceSize' => ActiveForm::SIZE_SMALL],
    //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
]);
?>

<div class="jm-repair-search">

    <div class="box box-default">
    <div class="box-header with-border"><i class="fa fa-search"></i></div>

    <div class="box-body">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                    'data'=> Yii::$app->Utilities->getItemInsureName(),
                    ]);
                ?>
            </div>
            <div class="col-sm-6 col-md-4">
                <?php
                    echo $form->field($model, 'dateBillingStart')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_RANGE,
                        'attribute2' => 'dateBillingEnd',
                        'separator' => 'ถึง',
                        'layout' => $layout,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ])->label('วันที่วางบิล');
                ?>
            </div>
        </div>
    </div>

    </div>

    <div class="box-footer">
        <div class="col-md-12 text-right">
            <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default', 'onClick'=>'ClearSearch();']) ?>
            <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    </div>

</div>

<?php echo $form->field($model, 'billing_status')->hiddenInput()->label(false); ?>

<?php ActiveForm::end(); ?>

<script>
    function ClearSearch()
    {
        $('#jmrepairclaimsearch-insurance_id').val('');
        $('#jmrepairclaimsearch-dateBillingStart').val('');
        $('#jmrepairclaimsearch-datebillingend').val('');
    }
</script>
