<?php
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'id' => 'form-update-billing',
    'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
    //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
    //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
]); ?>
<div class="row">
	<div class="col-md-12">
		<h3><span class="label label-default">Job No : <?php echo $model->jmRepair->job_no; ?></span></h3>
	</div>
	<div class="col-md-6">
		<?php echo $form->field($model, 'repair_amt')->textInput(); ?>
	</div>
	<div class="col-md-6">
		<?php echo $form->field($model, 'part_amt')->textInput(); ?>
	</div>
</div>
<?php echo $form->field($model, 'id')->hiddenInput()->label(false); ?>
<?php ActiveForm::end(); ?>