<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelClaim, 'policy_no')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelClaim, 'claim_no')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelClaim, 'repair_amt')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelClaim, 'part_amt')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelClaim, 'first_claim_amt')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2" style="margin-top: 25px;">
		<?php echo Html::button('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Claim'), ['class' => 'btn btn-warning', 'onClick'=>'addRepairClaim();']) ?>
	</div>
</div>

<div class="row" id="list-repair-claim">
<?php echo $this->render('_list-repair-claim', ['listRepairClaim'=>$listRepairClaim]); ?>
</div>

<script>
	function addRepairClaim()
	{
		if ($("#jmrepairclaim-repair_amt").val() == '' && $("#jmrepairclaim-part_amt").val() == '') {
			Swal.fire({
			  title: 'กรุณาระบุข้อมูลให้ครบถ้วน!',
			  icon: 'error'
			});
		} else if ($("#jmrepairclaim-policy_no").val() == '') {
			Swal.fire({
			  title: 'กรุณาระบุข้อมูล เลขที่กรมธรรม์!',
			  icon: 'error'
			});
		} else if ($("#jmrepairclaim-claim_no").val() == '' && $("#jmrepair-payment_type").val() != '1') {
			Swal.fire({
			  title: 'กรุณาระบุข้อมูล เลขที่เคลม!',
			  icon: 'error'
			});
		} else {
			$.post( "<?php echo Url::to(['/repaircar/add-repair-claim']);?>", {
				repair_id:<?php echo $model->id;?>,
				policy_no:$("#jmrepairclaim-policy_no").val(),
				claim_no:$("#jmrepairclaim-claim_no").val(),
				repair_amt:$("#jmrepairclaim-repair_amt").val(),
				part_amt:$("#jmrepairclaim-part_amt").val(),
				first_claim_amt:$("#jmrepairclaim-first_claim_amt").val(),
			}, function(data) {
			  $("#list-repair-claim").html(data);
			});
			//RESET VALUES
			resetInputClaim();
		}
	}

	function deleteRepairClaim(id)
	{
		if (confirm("ยืนยันลบรายการใช่หรือไม่?")) {
			$.post( "<?php echo Url::to(['/repaircar/delete-repair-claim']);?>", {id:id}, function(data) {
				$("#list-repair-claim").html(data);
			});
		}
	}

	function resetInputClaim()
	{
		$("#jmrepairclaim-policy_no").val('');
		$("#jmrepairclaim-claim_no").val('');
		$("#jmrepairclaim-repair_amt").val('');
		$("#jmrepairclaim-part_amt").val('');
		$("#jmrepairclaim-first_claim_amt").val('');
	}
</script>