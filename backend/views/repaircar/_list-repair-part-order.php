
<?php
use yii\helpers\Html;

if (!empty($listRepairPartOrder)) {
?>
<div class="col-md-12">
	<table class="table table-bordered table-striped">
		<tr class="info">
			<th width="5%">ลำดับ</th>
			<th width="15%">เลขที่ใบสั่งอะไหล่</th>
			<th width="30%">ชื่อร้านอะไหล่</th>
			<th width="15%">วันที่สั่ง</th>
			<th width="10%">ครั้งที่สั่ง</th>
			<th width="15%">ชื่อผู้สั่ง</th>
			<th width="5%" class="text-center">พิมพ์</th>
			<th width="5%" class="text-center">ลบ</th>
		</tr>
		<?php
		$i = 0;
		foreach ($listRepairPartOrder as $valPartOrder) {
		?>
		<tr>
			<td><?php echo ++$i; ?></td>
			<td><?php echo $valPartOrder->order_part_no; ?></td>
			<td><?php echo $valPartOrder->partShop->shop_name; ?></td>
			<td><?php echo date('d-m-Y', strtotime($valPartOrder->order_part_date)); ?></td>
			<td><?php echo $valPartOrder->order_part_seqno; ?></td>
			<td><?php echo $valPartOrder->user->firstname; ?></td>
			<td class="text-center">
				<?php echo Html::button('<i class="fa fa-print"></i>', ['class'=>'btn btn-primary btn-xs', 'onClick'=>'printPartOrder('.$valPartOrder->id.')']); ?>
			</td>
			<td class="text-center">
				<?php echo Html::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger btn-xs', 'onClick'=>'deletePartOrder('.$valPartOrder->id.')']); ?>
			</td>
		</tr>
		<?php } ?>
	</table>
</div>
<?php } else { 
	echo "<center>ไม่มีรายการสั่งอะไหล่</center>";
} ?>