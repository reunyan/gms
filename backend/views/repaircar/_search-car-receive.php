<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;

$layout = <<< HTML
    {input1}
    {separator}
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="fa fa-remove"></i>
    </span>
HTML;
?>

<div class="search-car-receive">

    <?php $form = ActiveForm::begin([
        'action' => ['search-car-receive'],
        'method' => 'get',
        'id' => 'form-search-car-receive',
    ]); ?>

    <div class="container-fluid">
        <div class="row">

        <div class="col-sm-4 col-md-4">
            <?php echo $form->field($model, 'car_license_no') ?>
        </div>

        <div class="col-sm-4 col-md-4">
            <?php echo $form->field($model, 'search_contact_repair_name')->label('ชื่อ หรือ นามสกุล ลูกค้าติดต่อ') ?>
        </div>

        <div class="col-sm-4 col-md-4 text-right v-center">
            <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            <?php echo Html::Button(Yii::t('app', 'Search'), ['class' => 'btn btn-primary', 'onClick'=>'SearchCarAppointment();']) ?>
        </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if (!empty($dataProvider)) { ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            //'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            [
                'content'=>Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Repair Car Not Appointment'), ['create'], ['class' => 'btn btn-success']),
            ],
            // '{export}',
            //'{toggleData}',
        ],
        // 'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'car_license_no',
            [
                'attribute'=>'insurance_id',
                'value'=>function($model){
                    return ($model->insurance)?$model->insurance->insure_name:'';
                }
            ],
            'appoint_repair_date',
            'contact_repair_fullname',
            'status_repair_name',

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => Yii::t('app', 'edit'),
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php } ?>

</div>
<script>
    function SearchCarAppointment(id){
        $.post("<?php echo Url::to(['/repaircar/search-car-receive']);?>", $('#form-search-car-receive').serialize(), function(data) {
            $("#body-search-car").html(data);
        });

    }
</script>