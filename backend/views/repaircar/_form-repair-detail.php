<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
	<div class="col-sm-4 col-md-4">
		<?php echo $form->field($modelDetail, 'repair_name')->textInput() ?>
	</div>
	<!-- <div class="col-sm-2 col-md-2">
		<?php //echo $form->field($modelDetail, 'repair_price')->textInput() ?>
	</div> -->
	<div class="col-sm-4 col-md-4">
		<?php echo $form->field($modelDetail, 'repair_remark')->textInput() ?>
	</div>
	<div class="col-sm-2 col-md-2" style="margin-top: 25px;">
		<?php echo Html::button('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Repair'), ['class' => 'btn btn-warning', 'onClick'=>'addRepairDetail();']) ?>
	</div>
</div>

<div class="row" id="list-repair-detail">
<?php echo $this->render('_list-repair-detail', ['listRepairDetail' => $listRepairDetail]); ?>
</div>

<script>
	function addRepairDetail()
	{
		if ($("#jmrepairdetail-repair_name").val() == '') {
			Swal.fire({
				title: 'กรุณาระบุข้อมูลรายการซ่อม!',
				icon: 'error'
			});
		} else {
			$.post( "<?php echo Url::to(['/repaircar/add-repair-detail']);?>", {
				repair_id:<?php echo $model->id;?>,
				repair_name:$("#jmrepairdetail-repair_name").val(),
				// repair_price:$("#jmrepairdetail-repair_price").val(),
				repair_remark:$("#jmrepairdetail-repair_remark").val()
			}, function(data) {
			  $("#list-repair-detail").html(data);
			});
			//RESET VALUES
			resetInputRepairDetail();
		}
	}

	function deleteRepairDetail(id)
	{
		$.post( "<?php echo Url::to(['/repaircar/delete-repair-detail']);?>", {id:id}, function(data) {
		  	$("#list-repair-detail").html(data);
		});
	}

	function resetInputRepairDetail()
	{
		$("#jmrepairdetail-repair_name").val('');
		// $("#jmrepairdetail-repair_price").val('');
		$("#jmrepairdetail-repair_remark").val('');
	}
</script>