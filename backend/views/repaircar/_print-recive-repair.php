<div class="layout_page">
<table width="100%">
    <tr>
        <td style="padding: 10px;"><img src="<?php echo Yii::getAlias('@web').'/imgs/logo_comp.png' ?>" style="margin-top: 20px;"></td>
        <td><center>
            <h2><?php echo Yii::$app->user->identity->comp->comp_name; ?></h2>
            <h3><?php echo Yii::$app->user->identity->comp->comp_name_en; ?></h3>
            <h4><?php echo Yii::$app->user->identity->compBr->br_addr; ?></h4>
            <h4>
                โทร <?php echo Yii::$app->user->identity->compBr->br_telno; ?>
                แฟกซ์ <?php echo Yii::$app->user->identity->compBr->br_faxno; ?>
                E-Mail <?php echo Yii::$app->user->identity->compBr->br_email; ?>
                </h4></center>
        </td>
    </tr>
</table>
<table width="100%" style="margin: 25px 0px 25px 0px;">
    <tr>
        <td width="20%">&nbsp;</td>
        <td width="60%"><center><h2><?php echo Yii::t('app', 'Recive Repair Car'); ?></h2></center></td>
        <td width="20%"><h4>เลขที่ <?php echo $model->job_no; ?></h4></td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td class="col-md-8">
            <table width="100%" style="border: solid 1px;">
                <tr>
                    <th width="25%" style="padding: 10px;text-align: left;">ชื่อลูกค้า</th>
                    <td width="75%" style="padding: 10px;"><?php echo $model->in_repair_firstname.' '.$model->in_repair_lastname; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 10px;text-align: left;">เบอร์ติดต่อ</th>
                    <td width="75%" style="padding: 10px;"><?php echo $model->in_repair_tel; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 10px;text-align: left;">ทะเบียนรถ</th>
                    <td width="75%" style="padding: 10px;"><?php echo $model->car_license_no; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 10px;text-align: left;">รถ</th>
                    <td width="75%" style="padding: 10px;"><?php echo $model->CarDetail; ?></td>
                </tr>
                <tr>
                    <th width="25%" style="padding: 10px;text-align: left;">ประกัน</th>
                    <td width="75%" style="padding: 10px;"><?php echo (!empty($model->insurance))?$model->insurance->insure_name:''; ?></td>
                </tr>
            </table>
        </td>
        <td class="col-md-4" style="border: solid 1px;">
            <table width="100%">
                <tr>
                    <th class="col-md-5" style="padding: 10px;text-align: left;">วันที่รถเข้าซ่อม</th>
                    <td class="col-md-7" style="padding: 10px;"><?php echo $model->in_repair_date; ?></td>
                </tr>
                <tr>
                    <th class="col-md-5" style="padding: 10px;text-align: left;">วันที่นัดรับรถ</th>
                    <td class="col-md-7" style="padding: 10px;"><?php echo $model->appoint_sent_date; ?></td>
                </tr>
                <tr>
                    <th class="col-md-5" style="padding: 10px;text-align: left;">ค่าเสียหายส่วนแรก</th>
                    <td class="col-md-7" style="padding: 10px;"><?php echo number_format($model->first_claim_amt, 2); ?></td>
                </tr>
                <tr>
                    <th class="col-md-5" style="padding: 10px;text-align: left;">เพิ่มเติม</th>
                    <td class="col-md-7" style="padding: 10px;"><?php echo number_format($model->other_amt, 2); ?></td>
                </tr>
                <tr>
                    <th colspan="2" style="padding: 10px;">โปรดนำใบเอกสารนี้มาในวันรับรถ</th>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" style="margin-top: 100px;">
    <tr>
        <td width="10%"></td>
        <td width="35%" style="border-bottom: dotted;">&nbsp;</td>
        <td width="10%"></td>
        <td width="35%" style="border-bottom: dotted;">&nbsp;</td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="padding: 10px 0px 10px 0px;"><center>(<?php echo $model->in_repair_firstname.' '.$model->in_repair_lastname; ?>)</center></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><center>ผู้ตรวจรับรถ</center></td>
        <td></td>
        <td><center>ลูกค้านำรถเข้าซ่อม</center></td>
        <td></td>
    </tr>
</table>
<table width="100%" style="margin-top: 30px;">
    <tr>
        <td width="10%"></td>
        <td width="35%" style="border-bottom: dotted;">&nbsp;</td>
        <td width="10%"></td>
        <td width="35%" style="border-bottom: dotted;">&nbsp;</td>
        <td width="10%"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td</td>>(<?php echo $model->in_repair_firstname.' '.$model->in_repair_lastname; ?>)</td>
        <td></td>
    </tr>
</table>
<table width="100%" style="margin-top: 40px;">
    <tr>
        <td colspan="5"><center>กรุณานำทรัพย์สินมีค่าออก-พระเครื่อง-อื่นๆ ก่อนนำรถเข้าซ่อม บริษัทจะไม่รับผิดชอบใดๆ ทั้งสิ้นหากเกิดการสูญเสีย</center></td>
    </tr>
</table>
</div>
</div>