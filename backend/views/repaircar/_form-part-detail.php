<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="row">
	<div class="col-sm-3 col-md-3">
		<?php echo $form->field($modelPart, 'part_name')->textInput() ?>
	</div>

	<div class="col-sm-2 col-md-1">
		<?php echo $form->field($modelPart, 'part_type')->dropDownList(Yii::$app->Utilities->getItemPartType()); ?>
	</div>

	<div class="col-sm-2 col-md-1">
		<?php echo $form->field($modelPart, 'part_owner')->dropDownList(Yii::$app->Utilities->getItemPartOwner()); ?>
	</div>

	<div class="col-sm-2 col-md-1">
		<?php echo $form->field($modelPart, 'part_return')->dropDownList(Yii::$app->Utilities->getItemPartReturn()); ?>
	</div>

	<div class="col-sm-2 col-md-1">
		<?php echo $form->field($modelPart, 'check_pic')->dropDownList(Yii::$app->Utilities->getItemCheckPic()); ?>
	</div>

	<div class="col-sm-2 col-md-2">
		<?php echo $form->field($modelPart, 'part_remark')->textInput() ?>
	</div>

	<div class="col-sm-1 col-md-1" style="margin-top: 25px;">
		<?php echo Html::button('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add Part'), ['class' => 'btn btn-warning', 'onClick'=>'addRepairPart();']) ?>
	</div>
</div>

<div class="row" id="list-repair-part">
	<?php echo $this->render('_list-repair-part', ['listRepairPart' => $listRepairPart]); ?>
</div>

<div class="row text-right" id="list-repair-part" style="margin-bottom:5px;">
<div class="col-md-12">
	<?php echo Html::button('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Order Part'), ['class' => 'btn btn-warning', 'onClick'=>'OpenModalListPartOrder('.$model->id.');']) ?>
</div>
</div>

<div class="row" id="list-repair-partorder">
	<?php echo $this->render('_list-repair-part-order', ['listRepairPartOrder' => $listRepairPartOrder]); ?>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalPartOrder">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header" id="modal-partorder-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modal-partorder-title">รายละเอียด</h4>
        </div>
        <div class="modal-body" id="modal-partorder-body">Detail</div>
        <div class="modal-footer" id="modal-partorder-footer">
            <?php echo Html::button(Yii::t('app','Save'), ['class'=>'btn btn-success', 'id'=>'btnSavePartOrder', 'onClick'=>'saveCreatePartOrder();']); ?>
            <?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function addRepairPart()
	{
		if ($("#jmrepairpart-part_name").val() == '') {
			Swal.fire({
				title: 'กรุณาระบุข้อมูลรายการอะไหล่!',
				icon: 'error'
			});
		} else {
			$.post( "<?php echo Url::to(['/repaircar/add-repair-part']);?>", {
				repair_id:<?php echo $model->id;?>,
				part_name:$("#jmrepairpart-part_name").val(),
				part_type:$("#jmrepairpart-part_type").val(),
				check_pic:$("#jmrepairpart-check_pic").val(),
				// part_price:$("#jmrepairpart-part_price").val(),
				part_remark:$("#jmrepairpart-part_remark").val(),
				part_owner:$("#jmrepairpart-part_owner").val(),
				part_return:$("#jmrepairpart-part_return").val(),
			}, function(data) {
			  $("#list-repair-part").html(data);
			});
			//RESET VALUES
			resetInputPartDetail();
		}
	}

	function deleteRepairPart(id)
	{
		Swal.fire({
            title: "ยืนยันการลบอะไหล่ ใช่หรือไม่",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: 'Reset',
        }).then((isConfirm) => {
            if (isConfirm.value) {
                $.post( "<?php echo Url::to(['/repaircar/delete-repair-part']);?>", {id:id}, function(data) {
					$("#list-repair-part").html(data);
				});
            }
        });
	}

	function resetInputPartDetail()
	{
		$("#jmrepairpart-part_name").val('');
		// $("#jmrepairpart-part_price").val('');
		$("#jmrepairpart-part_remark").val('');
		// $("#jmrepairpart-part_type").val('').trigger( "change" );
	}

	function OpenModalListPartOrder(id){
        $("#modalPartOrder").modal("show");
        $.post("<?php echo Url::to(['/repaircar/partorder-list-item-part']);?>", {id:id}, function(data) {
            $("#modal-partorder-title").html('สร้างใบสั่งอะไหล่');
            $("#modal-partorder-body").html(data);
        });
	}

	function deletePartOrder(id)
	{
		Swal.fire({
			title: "ยืนยันการลบรายการสั่งอะไหล่ ใช่หรือไม่",
			icon: "warning",
			showCancelButton: true,
			confirmButtonText: 'Reset',
		}).then((isConfirm) => {
			if (isConfirm.value) {
				$.post( "<?php echo Url::to(['/repaircar/delete-repair-partorder']);?>", {id:id}, function(data) {
					$("#list-repair-partorder").html(data);
				});
			}
		});
	}

function printPartOrder(id)
{
	window.open("../../repaircar/print-repair-partorder/?id="+id, '_blank');
}


	
</script>