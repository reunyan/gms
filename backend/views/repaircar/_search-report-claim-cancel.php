<?php

use yii\helpers\Html;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

$layout = <<< HTML
    {input1}
    {separator}
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="fa fa-remove"></i>
    </span>
HTML;
?>

<div class="jm-repair-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-claim-cancel'],
        'method' => 'get',
    ]); ?>

    <div class="box box-default">
    <div class="box-header with-border"><i class="fa fa-search"></i></div>

    <div class="box-body">

        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'job_no') ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'car_license_no')->label('ทะเบียนรถ') ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'policy_no') ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'claim_no') ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php
                    echo $form->field($model, 'dateClaimCancelStart')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_RANGE,
                        'attribute2' => 'dateClaimCancelEnd',
                        'separator' => 'ถึง',
                        'layout' => $layout,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ])->label('วันที่ยกเลิกเคลม');
                ?>
                </div>

            </div>
        </div>

    </div>

    <div class="box-footer">
        <div class="col-md-12 text-right">
            <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
