<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="panel panel-info">
    <div class="panel-heading"><i class="fa fa-list"></i></div>
    <table class="table table-striped">
        <tr>
            <th>วันที่นัดเข้าซ่อม</th>
            <td><?php echo $model->appoint_repair_date;?></td>
        </tr>
        <tr>
            <th>วันที่นัดรับรถซ่อมเสร็จ</th>
            <td><?php echo $model->appoint_sent_date;?></td>
        </tr>
        <tr>
            <th>เลขที่ทะเบียนรถ</th>
            <td><?php echo $model->car_license_no;?></td>
        </tr>
        <tr>
            <th>ชื่อผู้ติดต่อ</th>
            <td><?php echo $model->contact_repair_firstname;?></td>
        </tr>
        <tr>
            <th>สกุลผู้ติดต่อ</th>
            <td><?php echo $model->contact_repair_lastname;?></td>
        </tr>
        <tr>
            <th>เบอร์ติดต่อ</th>
            <td><?php echo $model->contact_repair_tel;?></td>
        </tr>
        <tr>
            <th>บริษัทประกันภัย</th>
            <td><?php echo ($model->insurance_id)?$model->insurance->insure_name:null;?></td>
        </tr>
        <tr>
            <th>จำนวนชิ้นซ่อม</th>
            <td><?php echo $model->num_repair;?></td>
        </tr>
    </table>
</div>

<div class="container-fluid">
    <div class="row text-right">
    <?php if ($model->status_repair==1 && empty($model->job_no)) { ?>
    <?php echo Html::button(Yii::t('app','Delete'), ['class'=>'btn btn-danger btn-sm', 'onClick'=>'DeleteDayAppoint('.$model->id.');']); ?>
    <?php } ?>
    </div>
</div>

<script>
    function DeleteDayAppoint(id)
    {
        if (confirm('ยืนยันการลบนัดเข้าซ่อมนี้ ใช่หรือไม่?'))
        {
            $.post("<?php echo Url::to(['/repaircar/delete-repair-appoint']);?>",{id:id}, function(data) {
            }).success(function() {
                window.location.reload();
            });
        }
    }
</script>