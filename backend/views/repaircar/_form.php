<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jm-repair-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="box <?php  echo ($model->isNewRecord)?'box-success':'box-warning'?>">
    <div class="box-header with-border">
        <i class="fa <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?>"></i>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <?php if (!empty($moel->job_no)) { ?>
            	<div class="row">
                    <div class="col-sm-4 col-md-3">
                        <?php echo $form->field($model, 'job_no')->textInput(['maxlength' => true, 'readOnly'=>true]) ?>
                    </div>
                </div>
            <?php } ?>
            <div class="row">

                <div class="panel panel-primary">
                    <div class="panel-heading">ข้อมูลรถ</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_license_no')->textInput(['maxlength' => true, 'placeholder'=>'ตัวอย่าง กก-999']) ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_license_province')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemProvinceName(),
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_type_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarType(),
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_brand_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarBrandName(),
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_model_id')->widget(DepDrop::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarModelName(),
                                    'type' => DepDrop::TYPE_SELECT2,
                                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                    'pluginOptions'=>[
                                        'depends'=>['jmrepair-car_brand_id'],
                                        'url'=>Url::to(['/jsoncar/getcarmodel'])
                                    ]
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_color_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarColorName(),
                                    ]);
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="row">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#information" aria-controls="information" role="tab" data-toggle="tab"><?php echo Yii::t('app', 'Information Repair') ?></a></li>
                    <li role="presentation"><a href="#claim-detail" aria-controls="claim-detail" role="tab" data-toggle="tab"><?php echo Yii::t('app', 'Claim Detail') ?></a></li>
                    <li role="presentation"><a href="#repair-detail" aria-controls="repair-detail" role="tab" data-toggle="tab"><?php echo Yii::t('app', 'Repair Detail') ?></a></li>
                    <li role="presentation"><a href="#part-detail" aria-controls="part-detail" role="tab" data-toggle="tab"><?php echo Yii::t('app', 'Part Detail') ?></a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="information">
                        <br>
                        <div class="container-fluid">
                            <?php echo $this->render('_form-information-detail', ['model' => $model, 'form'=>$form]); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="claim-detail">
                        <br>
                        <div class="container-fluid">
                            <?php echo $this->render('_form-claim-detail', ['model' => $model, 'modelClaim'=>$modelClaim, 'listRepairClaim'=>$listRepairClaim,'form'=>$form]); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="repair-detail">
                        <br>
                        <div class="container-fluid">
                            <?php echo $this->render('_form-repair-detail', ['model' => $model, 'modelDetail'=>$modelDetail, 'listRepairDetail'=>$listRepairDetail, 'form'=>$form]); ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="part-detail">
                        <br>
                        <div class="container-fluid">
                            <?php echo $this->render('_form-part-detail', ['model' => $model, 'modelPart'=>$modelPart, 'listRepairPart'=>$listRepairPart, 'listRepairPartOrder'=> $listRepairPartOrder, 'form'=>$form]); ?>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

    <div class="box-footer">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo Html::a('<i class="fa fa-print"></i> '.Yii::t('app', 'Recive Repair Car'), ['/repaircar/print-recive-repair', 'id'=>$model->id], ['class' =>'btn btn-primary', 'target'=>'_blank']); ?>
            <?php echo Html::a('<i class="fa fa-print"></i> '.Yii::t('app', 'List Order Job'), ['/repaircar/list-order-job', 'id'=>$model->id], ['class' =>'btn btn-primary', 'target'=>'_blank']); ?>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-4 text-right"><?php echo $form->field($model, 'status_repair')->dropDownList(Yii::$app->Utilities->getItemRepairStatus(false))->label(false) ?></div>
        <div class="col-xs-6 col-sm-2 col-md-2 text-left">
            <?php echo Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', 'Save'), ['class' =>'btn btn-success']); ?>
        </div>
    </div>
                                    
    <?php echo Html::hiddenInput("hidRepairId", $model->id, ['id'=>'hidRepairId']);?>
    
    <?php ActiveForm::end(); ?>

</div>
