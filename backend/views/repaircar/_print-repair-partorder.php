<div class="layout_page">

<table width="100%" style="margin-top:20px;">
    <tr>
        <td width="100%">
            <table width="100%" style="margin-top:15px;margin-top:10px;">
                <tr>
                    <td>
                        <h2><?php echo $modelComp->comp->comp_name; ?></h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pdf_company_address"><?php echo $modelComp->br_addr; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                    <p class="pdf_company_address">
                        โทร <?php echo $modelComp->br_telno; ?>
                        แฟกซ์ <?php echo $modelComp->br_faxno; ?>
                        มือถือ <?php echo $modelComp->br_mobileno; ?>
                    </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" style="margin-top:20px;margin-bottom:15px;">
    <tr>
        <td align="center">
            <h3>ใบสั่งซื้อ</h3>
        </td>
    </tr>
</table>

<table width="100%" border="1" cellpadding="0" cellspacing="0">
    <tr>
        <td width="60%" valign="top" >
            <table width="100%" style="margin-top:5px;margin-left:5px;" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="20%"><span style="font-weight: bold;">ชื่อร้านค้า </span></td>
                    <td width="80%"><?php echo $model->partShop->shop_name; ?></td>
                </tr>
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">ที่อยู่ </span></p>
                    </td>
                    <td>
                        <p><?php echo $model->partShop->address; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">เบอร์ติดต่อ </span></p>
                    </td>
                    <td>
                        <p><?php echo $model->partShop->contact_telno; ?></p>
                    </td>
                </tr>
            </table>
        </td>
        <td width="40%" valign="top">
            <table width="100%" style="margin-top:5px;margin-left:5px;">
                <tr>
                    <td width="40%">
                        <span style="font-weight: bold;">เลขที่เอกสาร </span>
                    </td>
                    <td width="60%">
                        <?php echo $model->order_part_no; ?>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">วันที่สั่งอะไหล่ </span></p>
                    </td>
                    <td>
                        <p><?php echo date('d-m-Y', strtotime($model->order_part_date)); ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">ยี่ห้อ </span></p>
                    </td>
                    <td>
                        <p><?php echo (!empty($modelRepair->carBrand))?$modelRepair->carBrand->brand_name:''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">รุ่น </span></p>
                    </td>
                    <td>
                        <p><?php echo (!empty($modelRepair->carModel))?$modelRepair->carModel->model_name:''; ?></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p><span style="font-weight: bold;">ทะเบียน </span></p>
                    </td>
                    <td>
                        <p><?php echo $modelRepair->car_license_no; ?></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%" class="table_bordered" border="0" cellpadding="2" cellspacing="0" style="margin-top:10px;">
    <tr>
        <th style="padding:10px;" width="5%"><center>ลำดับ</center></th>
        <th width="80%"><center>รายการ</center></th>
        <th width="15%"><center>ประเภท</center></th>
    </tr>
    <?php 
    $i = 0;
    foreach ($model->partorderItem as $valPart) { 
    ?>
    <tr>
        <td style="padding:7px;"><center><?php echo ++$i; ?></center></td>
        <td style="padding:7px;"><?php echo $valPart->repairPart->part_name; ?></td>
        <td style="padding:7px;"><center><?php echo $valPart->repairPart->part_type_name; ?></center></td>
    </tr>
    <?php } ?>
</table>

<table width="100%" style="margin-top:40px;">
	<tr>
        <td width="15%" >&nbsp;</td>
		<td width="15%" >&nbsp;</td>
		<td width="15%" >&nbsp;</td>
		<td width="15%" style="border-bottom: 0.2px solid #000;padding-right: 10px;">&nbsp;</td>
	</tr>
    <tr>
        <td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td style="font-size: 12px;"><center>ผู้สั่ง</center></td>
	</tr>
</table>
</div>