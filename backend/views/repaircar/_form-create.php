<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jm-repair-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
        //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="box <?php  echo ($model->isNewRecord)?'box-success':'box-warning'?>">
    <div class="box-header with-border">
        <i class="fa <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?>"></i>
    </div>
    <div class="box-body">
        <div class="container-fluid">
            <div class="row">

                <div class="panel panel-primary">
                    <div class="panel-heading">ข้อมูลรถ</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_license_no')->textInput(['maxlength' => true, 'placeholder'=>'ตัวอย่าง กก-999']) ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_license_province')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemProvinceName(),
                                    ]);
                                ?>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_brand_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarBrandName(),
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_model_id')->widget(DepDrop::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarModelName(),
                                    'type' => DepDrop::TYPE_SELECT2,
                                    'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
                                    'pluginOptions'=>[
                                        'depends'=>['jmrepair-car_brand_id'],
                                        'url'=>Url::to(['/jsoncar/getcarmodel'])
                                    ]
                                    ]);
                                ?>
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <?php echo $form->field($model, 'car_color_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemCarColorName(),
                                    ]);
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <div class="row">

                <div class="panel panel-success">
                    <div class="panel-heading">ข้อมูลประกัน</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                <?php echo $form->field($model, 'payment_type')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemPaymentType(),
                                    ]);
                                ?>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                                    'data'=> Yii::$app->Utilities->getItemInsureName(),
                                    ]);
                                ?>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <?php echo $form->field($model, 'insurance_type')->dropDownList(Yii::$app->Utilities->getItemInsureType()); ?>
                            </div>

                            <div class="col-sm-3 col-md-3">
                                <?php echo $form->field($model, 'car_claim_type')->dropDownList(Yii::$app->Utilities->getItemCarClaimType()); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_type')->dropDownList(Yii::$app->Utilities->getItemInRepairType()); ?>
                </div>

                <!-- <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'appoint_repair_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]);
                    ?>
                </div> -->

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'contact_repair_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]);
                    ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]);
                    ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'appoint_sent_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'removeButton' => false,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd-mm-yyyy'
                            ]
                        ]);
                    ?>
                </div>
            </div>

            <div class="row">
               <div class="col-md-12"><h4><span class="label label-default">ค่าใช้จ่าย</span></h4></div>

                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'first_claim_amt')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'other_amt')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'other_amt_desc')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <!--
                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'contact_repair_firstname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'contact_repair_lastname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'contact_repair_tel')->textInput(['maxlength' => true]) ?>
                </div>
            </div> -->

            <div class="row">
                <div class="col-md-12"><h4><span class="label label-default">เข้าซ่อม</span></h4></div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_firstname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_lastname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_tel')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-4 col-md-3">
                    <?php echo $form->field($model, 'in_repair_emp')->widget(Select2::classname(), [
                        'data'=> Yii::$app->Utilities->getItemEmployeeName(),
                        ]);
                    ?>
                </div>

            </div>
        </div>
    </div>

    <div class="box-footer text-right">
        <div class="col-md-7"></div>
        <div class="col-md-3 text-right"><?php echo $form->field($model, 'status_repair')->dropDownList(Yii::$app->Utilities->getItemRepairStatusCreate(false))->label(false) ?></div>
        <div class="col-md-2 text-left"><?php echo Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', 'Save'), ['class' =>'btn btn-success']); ?></div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
