<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */

$this->title = Yii::t('app', 'Create Repair Car');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Repair Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jm-repair-create">

    <?php echo $this->render('_form-create', [
        'model' => $model,
    ]) ?>

</div>
