<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Report Claim Cancel');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-claim-cancel">
    <?php echo $this->render('_search-report-claim-cancel', ['model' => $searchModel]); ?>

    <?php 
    if (!empty($dataProvider)) {
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'panel'=>[
                'type'=>GridView::TYPE_PRIMARY,
                'heading'=> '<i class="fa fa-list"></i>',
                //'before'=> false,
                'after'=> false,
            ],
            'toolbar'=> [
                '{export}',
                '{toggleData}',
            ],        
            'exportConfig' => [
                GridView::CSV => true,
                GridView::EXCEL=> ['filename'=>'export-claim-cancel',],
                
            ],
            // 'filterModel' => $searchModel,
            // 'showPageSummary' => true,
            'headerRowOptions'=> ['class'=>'info'],
            'filterRowOptions'=> ['class'=>'info'],
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                'job_no',
                [
                    'attribute' => 'car_license_no',
                    'label' => 'ทะเบียนรถ',
                ],
                [
                    'attribute' => 'policy_no',
                    'label' => 'เลขที่กรมธรรม์',
                ],
                [
                    'attribute' => 'claim_no',
                    'label' => 'เลขที่เคลม',
                ],
                [
                    'attribute' => 'first_claim_amt',
                    'label' => 'ค่า Ex.',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute'=>'repair_amt',
                    'label' => 'จำนวนเงินค่าแรง',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute'=>'part_amt',
                    'label' => 'จำนวนเงินอะไหล่',
                    'hAlign' => 'right',
                    'vAlign' => 'middle',
                    'format' => ['decimal', 2],
                ],
                [
                    'label' => 'วันที่ยกเลิก',
                    'value'=> function($model){
                        return date('d-m-Y H:i', strtotime($model['updated_at']));
                    },
                ],
                [
                    'label' => 'ผู้ยกเลิก',
                    'value'=> function($model){
                        return $model['firstname'].' ('.$model['nickname'].')';
                    },
                ],
            ],
        ]);
    } ?>
</div>
