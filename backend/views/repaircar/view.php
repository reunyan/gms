<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */

$this->title = $model->car_license_no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Repair Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jm-repair-view">
    
    <div class="panel panel-info">
        <div class="panel-heading"><i class="fa fa-list"></i></div>
        <table class="table table-striped">
            <tr>
                <th>Job No</th>
                <td colspan="3"><?php echo $model->job_no;?></td>
            </tr>
            <tr>
                <th colspan="4" class="warning">ข้อมูลรถ</th>
            </tr>
            <tr>
                <th>เลขที่ทะเบียนรถ</th>
                <td><?php echo $model->car_license_no;?></td>
                <th>จังหวัดทะเบียนรถ</th>
                <td><?php echo (!empty($model->carProvince))?$model->carProvince->province_name:null;?></td>
            </tr>
            <tr>
                <th>ยี่ห้อรถ</th>
                <td><?php echo (!empty($model->carBrand))?$model->carBrand->brand_name:null;?></td>
                <th>รุ่นรถ</th>
                <td><?php echo (!empty($model->carModel))?$model->carModel->model_name:null;?></td>
            </tr>
            <tr>
                <th>สีรถ</th>
                <td><?php echo (!empty($model->carColor))?$model->carColor->color_name:null;?></td>
                <th>ประเภทรถ</th>
                <td><?php echo (!empty($model->carType))?$model->carType->type_name:null;?></td>
            </tr>
            <tr>
                <th colspan="4"  class="warning">ข้อมูลประกัน</th>
            </tr>
            <tr>
                <th>บริษัทประกันภัย</th>
                <td><?php echo (!empty($model->insurance))?$model->insurance->insure_name:null;?></td>
                <th>ประเภทบริษัทประกัน</th>
                <td><?php echo (!empty($model->insurance_type))?Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'INSURETYPE', $model->insurance_type):null;?></td>
            </tr>
            <tr>
                <th>การชำระเงิน</th>
                <td><?php echo (!empty($model->payment_type))?Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'PAYMENTTYPE', $model->payment_type):null;?></td>
                <th>ประเภทรถซ่อม</th>
                <td><?php echo (!empty($model->car_claim_type))?Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'CARCLAIMTYPE', $model->car_claim_type):null;?></td>
            </tr>
            <tr>
                <th colspan="4"  class="warning">ค่าใช้จ่าย</th>
            </tr>
            <tr>
                <th>ค่า Ex.</th>
                <td><?php echo $model->first_claim_amt;?></td>
                <th>เพิ่มเติม</th>
                <td><?php echo $model->other_amt;?></td>
            </tr>
            <tr>
                <th>รายละเอียดเพิ่มเติม</th>
                <td colspan="3"><?php echo $model->other_amt_desc;?></td>
            </tr>
            <tr>
                <th colspan="4"  class="warning">ข้อมูลลูกค้า</th>
            </tr>
            <tr>
                <th>วันที่นัดเข้าซ่อม</th>
                <td><?php echo $model->appoint_repair_date;?></td>
                <th>วันที่นัดรับรถซ่อมเสร็จ</th>
                <td><?php echo $model->appoint_sent_date;?></td>
            </tr>
            <tr>
                <th>ชื่อผู้ติดต่อ</th>
                <td><?php echo $model->contact_repair_firstname;?></td>
                <th>สกุลผู้ติดต่อ</th>
                <td><?php echo $model->contact_repair_lastname;?></td>
            </tr>
            <tr>
                <th>เบอร์ติดต่อ</th>
                <td><?php echo $model->contact_repair_tel;?></td>
                <th>วันที่ติดต่อซ่อม</th>
                <td><?php echo $model->contact_repair_date;?></td>
            </tr>
            <tr>
                <th>การจอดซ่อม</th>
                <td colspan="3"><?php echo (!empty($model->in_repair_type))?Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'CARSTOP', $model->in_repair_type):null;?></td>
            </tr>
            <tr>
                <th>ชื่อผู้นำรถเข้าซ่อม</th>
                <td><?php echo $model->in_repair_firstname;?></td>
                <th>นามสกุลผู้นำรถเข้าซ่อม</th>
                <td><?php echo $model->in_repair_lastname;?></td>
            </tr>
            <tr>
                <th>เบอร์ติดต่อ</th>
                <td><?php echo $model->in_repair_tel;?></td>
                <th>วันที่จอดซ่อม</th>
                <td><?php echo $model->in_repair_date;?></td>
            </tr>
            <tr>
                <th>ชื่อผู้รับรถ</th>
                <td><?php echo $model->sent_car_firstname;?></td>
                <th>นามสกุลผู้รับรถ</th>
                <td><?php echo $model->sent_car_lastname;?></td>
            </tr>
            <tr>
                <th>เบอร์ติดต่อ</th>
                <td><?php echo $model->sent_car_tel;?></td>
                <th>วันที่ส่งรถ</th>
                <td><?php echo $model->sent_car_date;?></td>
            </tr>
        </table>
    </div>

</div>
