<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Print').' '.Yii::t('app', 'repair part order');
?>

<div class="row">

<div class="col-md-12">
    <div clss="form-group">
        <label class="control-label has-star" for="selPartshop">ร้านอะไหล่</label>
        <?php echo Html::dropDownList("selPartshop", null, Yii::$app->Utilities->getItemPartshop(), ['class'=>'form-control', 'id'=>'selPartshop']);?>
    </div>
</div>

<div class="col-md-12">&nbsp;</div>

<div class="col-md-12">
	<table class="table table-bordered table-striped">
		<tr class="info">
			<th width="5%"><?php echo Html::checkbox("ckbPartsAll", false, ['id'=>'ckbPartsAll', 'onClick'=>'selectPartAll()']); ?></th>
			<th width="30%">รายการอะไหล่</th>
			<th width="15%">ประเภท</th>
			<th width="15%">ผู้จัด</th>
			<th width="15%">คืนซาก</th>
			<th width="20%">หมายเหตุ</th>
		</tr>
        <?php
        $i = 0;
        foreach ($model as $valPart) {
        ?>
        <tr>
            <td><?php echo Html::checkbox("ckbParts", false, ['value'=>$valPart->id, 'onClick'=>'selectPartManual()']); ?></td>
            <td><?php echo $valPart->part_name; ?></td>
            <td><?php echo $valPart->part_type_name; ?></td>
            <td><?php echo $valPart->part_owner_name; ?></td>
            <td><?php echo $valPart->part_return_name; ?></td>
            <td><?php echo $valPart->part_remark; ?></td>
        </tr>
        <?php } ?>
    </table>
</div>
</div>

<?php echo Html::hiddenInput("hidSetCheckPartAll", 0, ['id'=>'hidSetCheckPartAll']);?>
<script>
    function selectPartAll(){
        if ($("#hidSetCheckPartAll").val() == 0) {
            $("#hidSetCheckPartAll").val(1);
            $("input[name='ckbParts']").prop("checked",true );
        } else {
            $("#hidSetCheckPartAll").val(0);
            $("input[name='ckbParts']").prop("checked",false );
        }
    }

    function selectPartManual(){
        $("#hidSetCheckPartAll").val(0);
        $("input[name='ckbPartsAll']").prop("checked",false );
    }

    function saveCreatePartOrder(){
        var intCheckPart = $("input[name='ckbParts']:checkbox:checked").length;
        if ($("#selPartshop").find(":selected").val()=='') {
            Swal.fire({
                title: "ต้องทำการเลือกร้านอะไหล่",
                icon: "error",
            })
        } else if (intCheckPart == 0) {
            Swal.fire({
                title: "ต้องทำการเลือกรายการอะไหล่อย่างน้อย 1 รายการ",
                icon: "error",
            })
        } else {
            Swal.fire({
                title: "ยืนยันการสร้างใบสั่งอะไหล่ ใช่หรือไม่",
                icon: "question",
                showCancelButton: true,
                confirmButtonText: 'Reset',
            }).then((isConfirm) => {
                if (isConfirm.value) {
                    var PartItems = [];
                    $.each($("input[name='ckbParts']:checked"), function(){
                        PartItems.push($(this).val());
                    });

                    $.ajax({
                        url  : "<?php echo Url::to(['/repaircar/partorder-save-list-part']);?>",
                        type : 'post',
                        data : {repair_id:$('#hidRepairId').val(), partshop_id:$("#selPartshop").find(":selected").val(), part_items:PartItems},
                        success: function(res)
                        {
                            $('#list-repair-partorder').html(res);
                            $("#modalPartOrder").modal("hide");
                        }
                    });
                }
            });
        }
    }
</script>