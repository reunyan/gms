<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;


$this->title = Yii::t('app','Calendar Repair Cars');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Repair Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

backend\assets\AppointmentAsset::register($this);
?>

<?php
$JSEventViewClick = <<<EOF
function(calEvent, jsEvent, view) {
    ViewAppoint(calEvent.id);
}
EOF;
?>
<div class="box box-success">
    <div class="box-header with-border">
        <i class="fa fa-calendar"></i>
    </div>
    <div class="box-body">
        <div class="container-fluid">
        	<div class="row">
                <div class="col-md-12">
				  	<?php echo \yii2fullcalendar\yii2fullcalendar::widget([
				    	'events'=> $events,
				    	'clientOptions' => [
		                    'language' => 'th',
		                	'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {
		                		$("#jmrepair-appoint_repair_date").val(formatDateDMY(cellInfo._d));
		                		$(".modal-title").html("เพิ่มนัด วันที่ "+formatDateDMY(cellInfo._d));
		                		$("#model-edit").modal("show");
		                	}'),
		                	'eventClick' => new JsExpression($JSEventViewClick),
		                ],
				    ]
				  	);?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $form = ActiveForm::begin([
	'id'=>'frm-add-appoint-repair',
    'type' => ActiveForm::TYPE_VERTICAL, //TYPE_HORIZONTAL
    //'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
    //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
]); ?>

<div class="modal fade" tabindex="-1" role="dialog" id="model-edit">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Modal title</h4>
		</div>
		<div class="modal-body">
			<div class="row">

				<div class="col-sm-6 col-md-6">
			        <?php
			        /*echo $form->field($model, 'appoint_repair_date')->widget(DatePicker::classname(), [
			                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
			                'type' => DatePicker::TYPE_COMPONENT_APPEND,
			                'removeButton' => false,
			                'pluginOptions' => [
			                    'autoclose'=>true,
			                    'format' => 'dd-mm-yyyy'
			                ]
			            ]);*/
			        ?>
			        <?php echo $form->field($model, 'appoint_repair_date')->textInput(['maxlength' => true, 'readOnly'=>true]) ?>
			    </div>

	            <div class="col-sm-6 col-md-6">
	                <?php echo $form->field($model, 'car_license_no')->textInput(['maxlength' => true, 'placeholder'=>'ตัวอย่าง กก-999']) ?>
	            </div>

	            <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'contact_repair_firstname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'contact_repair_lastname')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'contact_repair_tel')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                        'data'=> Yii::$app->Utilities->getItemInsureName(),
                        ]);
                    ?>
                </div>

                <div class="col-sm-6 col-md-6">
                    <?php echo $form->field($model, 'num_repair')->textInput(['maxlength' => true]) ?>
                </div>
			</div>
		</div>
		<div class="modal-footer">
			<?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
			<?php echo Html::button(Yii::t('app','Save'), ['class'=>'btn btn-success', 'onClick'=>'SaveDayAppoint();']); ?>
		</div>
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php ActiveForm::end(); ?>

<div class="modal fade" tabindex="-1" role="dialog" id="modelView">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">รายละเอียด</h4>
		</div>
		<div class="modal-body" id="modal-body-view"></div>
		<!-- <div class="modal-footer">
			<?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
			<?php echo Html::button(Yii::t('app','Save'), ['class'=>'btn btn-success', 'onClick'=>'SaveDayAppoint();']); ?>
		</div> -->
	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function SaveDayAppoint(){
		if ($('#appoint_license_no').val() == '') {
			alert('กรุณาระบุ เลขทะเบียนรถ');
		} else if ($('#appoint_first_name').val() == '') {
			alert('กรุณาระบุ ชื่อผู้ติดต่อ');
		} else {
			$.post( "<?php echo Url::to(['/repaircar/add-repair-appoint']);?>", {
				appoint_date:$("#jmrepair-appoint_repair_date").val(),
				appoint_license_no:$("#jmrepair-car_license_no").val(),
				appoint_first_name:$("#jmrepair-contact_repair_firstname").val(),
				appoint_last_name:$("#jmrepair-contact_repair_lastname").val(),
				appoint_telno:$("#jmrepair-contact_repair_tel").val(),
				repair_insure:$("#jmrepair-insurance_id").val(),
				num_repair:$("#jmrepair-num_repair").val(),
			}, function(data) {
			  alert('บันทึกข้อมูลเรียบร้อย');
			  $("#model-edit").modal("hide");
			  window.location.reload();
			});
		}
	}

	function ViewAppoint(id){
			$.post( "<?php echo Url::to(['/repaircar/view-repair-appoint']);?>", {
				id:id,
			}, function(data) {
				$("#modal-body-view").html(data);
				$("#modelView").modal("show");
			});
	}
</script>