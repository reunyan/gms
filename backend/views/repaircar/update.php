<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */

$this->title = Yii::t('app', 'Update Repair Car'). ' ' . $model->car_license_no;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Repair Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->car_license_no, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="jm-repair-update">

    <?php echo $this->render('_form', [
		'model'            => $model,
		'modelDetail'      => $modelDetail,
		'listRepairDetail' => $listRepairDetail,
		'modelPart'        => $modelPart,
		'listRepairPart'   => $listRepairPart,
		'modelClaim'       => $modelClaim,
		'listRepairClaim'  => $listRepairClaim,
		'listRepairPartOrder'=> $listRepairPartOrder,
    ]) ?>

</div>
