<?php
use yii\helpers\Html;
?>
<div class="">
<?php if (!empty($listRepairClaim)) { ?>
	<table class="table table-bordered table-striped">
		<tr class="info">
			<th width="5%">ลำดับ</th>
			<th width="20%">เลขที่กรมธรรม์</th>
			<th width="20%">เลขที่เคลม</th>
			<th width="15%">จำนวนเงินค่าแรง</th>
			<th width="15%">จำนวนเงินค่าอะไหล่</th>
			<th width="15%">ค่า Ex.</th>
			<th width="15%">จำนวนเงินรวม</th>
			<th width="10%">จัดการ</th>
		</tr>
		<?php
		$i               = 0;
		$intSumFirstAmt  = 0;
		$intSumRepairAmt = 0;
		$intSumPartAmt   = 0;
		$intSumTotalAmt  = 0;
		$intSumAllAmt    = 0;
		foreach ($listRepairClaim as $valClaim) {
			if ($valClaim->isactive == 'Y') {
				$intSumFirstAmt  += $valClaim->first_claim_amt;
				$intSumRepairAmt += $valClaim->repair_amt;
				$intSumPartAmt   += $valClaim->part_amt;
				$intSumTotalAmt  = $valClaim->repair_amt+$valClaim->part_amt;
				$intSumAllAmt    += $valClaim->repair_amt+$valClaim->part_amt;
			}
		?>
		<?php if ($valClaim->isactive == 'N') { ?>
		<tr>
			<td class="text-del"><?php echo ++$i; ?></td>
			<td class="text-del"><?php echo $valClaim->policy_no; ?></td>
			<td class="text-del"><?php echo $valClaim->claim_no; ?></td>
			<td class="text-del text-right"><?php echo number_format($valClaim->repair_amt,2); ?></td>
			<td class="text-del text-right"><?php echo number_format($valClaim->part_amt,2); ?></td>
			<td class="text-del text-right"><?php echo number_format($valClaim->first_claim_amt,2); ?></td>
			<td class="text-del text-right"><?php echo number_format($intSumTotalAmt,2); ?></td>
			<td class="text-del">-</td>
		</tr>
		<?php } else { ?>
		<tr>
			<td><?php echo ++$i; ?></td>
			<td><?php echo $valClaim->policy_no; ?></td>
			<td><?php echo $valClaim->claim_no; ?></td>
			<td class="text-right"><?php echo number_format($valClaim->repair_amt,2); ?></td>
			<td class="text-right"><?php echo number_format($valClaim->part_amt,2); ?></td>
			<td class="text-right"><?php echo number_format($valClaim->first_claim_amt,2); ?></td>
			<td class="text-right"><?php echo number_format($intSumTotalAmt,2); ?></td>
			<td><?php echo Html::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger btn-xs', 'onClick'=>'deleteRepairClaim('.$valClaim->id.')']); ?></td>
		</tr>
		<?php } } ?>
		<tr class="warning">
			<th colspan="3" class="text-right">รวมเงิน</th>
			<th class="text-right"><?php echo number_format($intSumRepairAmt,2); ?></th>
			<th class="text-right"><?php echo number_format($intSumPartAmt,2); ?></th>
			<th class="text-right"><?php echo number_format($intSumFirstAmt,2); ?></th>
			<th class="text-right"><?php echo number_format($intSumAllAmt,2); ?></th>
			<th>&nbsp;</th>
		</tr>
	</table>
<?php } ?>
</div>