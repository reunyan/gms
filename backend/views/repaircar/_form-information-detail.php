<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">

    <div class="panel panel-success">
        <div class="panel-heading">ข้อมูลประกัน</div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'payment_type')->widget(Select2::classname(), [
                        'data'=> Yii::$app->Utilities->getItemPaymentType(),
                        ]);
                    ?>
                </div>
                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                        'data'=> Yii::$app->Utilities->getItemInsureName(),
                        ]);
                    ?>
                </div>
                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'insurance_type')->dropDownList(Yii::$app->Utilities->getItemInsureType()); ?>
                </div>

                <div class="col-sm-3 col-md-3">
                    <?php echo $form->field($model, 'car_claim_type')->dropDownList(Yii::$app->Utilities->getItemCarClaimType()); ?>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'in_repair_type')->dropDownList(Yii::$app->Utilities->getItemInRepairType()); ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'appoint_repair_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
        ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'contact_repair_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
        ?>
    </div>

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'in_repair_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
        ?>
    </div>

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'appoint_sent_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
        ?>
    </div>

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'num_repair')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
   <div class="col-md-12"><h4><span class="label label-default">ค่าใช้จ่าย</span></h4></div>

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'first_claim_amt')->textInput(['maxlength' => true, 'readOnly'=>true]) ?>
    </div>

    <div class="col-sm-3 col-md-3">
        <?php echo $form->field($model, 'other_amt')->textInput(['maxlength' => true, 'readOnly'=>(((intval($model->other_amt)>0) && !Yii::$app->Authcontrol->UserIsAdmin())||($model->receive_money_at!=""))]) ?>
    </div>

    <div class="col-sm-4 col-md-4">
        <?php echo $form->field($model, 'other_amt_desc')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-2 col-md-2" style="margin-top: 20px;">
        <?php if (intval($model->first_claim_amt)+intval($model->other_amt)>0) { ?>
            <?php if (Yii::$app->Authcontrol->UserIsAdmin() && $model->receive_money_at=="") { ?>
            <?php echo Html::button('<i class="fa fa-circle"></i> รับเงิน', ['class'=>'btn btn-warning', 'onClick'=>'SaveReceiveMoney();']);?>
            <?php } else { echo '<h4><span class="label label-success"><i class="fa fa-check-circle"></i> รับเงินแล้ว</span></h4>';} ?>
        <?php } ?>
    </div>
</div>

<!-- <div class="row">
   <div class="col-md-12"><h4><span class="label label-default">ติดต่อเข้าซ่อม</span></h4></div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'contact_repair_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ]);
        ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'contact_repair_firstname')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'contact_repair_lastname')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'contact_repair_tel')->textInput(['maxlength' => true]) ?>
    </div>
</div> -->

<div class="row">
   <div class="col-md-12"><h4><span class="label label-default">เข้าซ่อม</span></h4></div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'in_repair_firstname')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'in_repair_lastname')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'in_repair_tel')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-4 col-md-3">
        <?php echo $form->field($model, 'in_repair_emp')->widget(Select2::classname(), [
            'data'=> Yii::$app->Utilities->getItemEmployeeName(false, (empty($model->in_repair_emp))?Yii::$app->user->getId():null),
            'options' => ['disabled' => (!empty($model->in_repair_emp))],
            ]);
        ?>
    </div>

</div>

<div class="row">
    <div class="panel panel-danger">
        <div class="panel-heading">ข้อมูลส่งรถ</div>
        <div class="panel-body">

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'sent_car_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ]);
                ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'sent_car_firstname')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'sent_car_lastname')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'sent_car_tel')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-sm-4 col-md-3">
                <?php echo $form->field($model, 'sent_car_emp')->widget(Select2::classname(), [
                    'data'=> Yii::$app->Utilities->getItemEmployeeName(),
                    ]);
                ?>
            </div>

        </div>
    </div>
</div>
<script>
    function OpenReceiveMoney()
    {
        $("#modalReceiveMoney").modal("show");
    }

    function SaveReceiveMoney()
    {
        if(confirm("ยืนยันการรับเงิน ใช่หรือไม่?")) {
            $.post("<?php echo Url::to(['/repaircar/save-receive-money']);?>",
            {id:<?php echo $model->id;?>}, 
            function(data) {}).success(function() {
                window.location.reload();
            });
        }
    }
</script>

<div class="modal fade" tabindex="-1" role="dialog" id="modalReceiveMoney">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">รายละเอียดรับเงิน</h4>
        </div>
        <div class="modal-body">
            <?php echo $form->field($model, 'receive_money_at')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'วัน-เดือน-ปี'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-mm-yyyy',
                            'todayHighlight' => true
                        ]
                    ]);
                ?>
        </div>
        <div class="modal-footer">
            <?php echo Html::button(Yii::t('app','Save'), ['class'=>'btn btn-success', 'id'=>'btnSaveReceiveMoney', 'onClick'=>'SaveReceiveMoney();']); ?>
            <?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->