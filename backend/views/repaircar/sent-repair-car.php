<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;

$this->title = Yii::t('app', 'List Sent Cars');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jm-repair-index">
    <?php echo $this->render('_search-sent-repair-car', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            //'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            [
                'content'=>Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Create Repair Car'), ['create'], ['class' => 'btn btn-success']),
            ],
            // '{export}',
            //'{toggleData}',
        ],
        // 'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'job_no',
            'car_license_no',
            [
                'attribute'=>'CarDetail',
                'mergeHeader' => true,
            ],
            [
                'attribute'=>'insurance_id',
                'value'=>function($model){
                    return ($model->insurance)?$model->insurance->insure_name:'';
                }
            ],
            [
                'attribute'=>'in_repair_type',
                'value'=>function($model){
                    return Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'CARSTOP', $model->in_repair_type);
                }
            ],
            // 'contact_repair_date',
            'in_repair_date',
            'appoint_sent_date',
            'sent_car_date',
            [
                'attribute' => 'first_claim_amt',
                'hAlign' => 'right',
                'vAlign' => 'middle',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            [
                'attribute' => 'other_amt',
                'hAlign' => 'right',
                'vAlign' => 'middle',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            [
                'label'=>'สถานะรับเงิน',
                'value'=>function($model){
                    if (($model->first_claim_amt+$model->other_amt)>0) {
                        if ($model->receive_money_at!="") {
                            return '<span class="label label-success">รับเงินแล้ว</span>';
                        } else {
                            return '<span class="label label-danger">ยังไม่ได้รับ</span>';
                        }
                    } else {
                        return null;
                    }
                },
                'format' => 'raw',
            ],
            'other_amt_desc',
            [
                'attribute'=>'labor_amt',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            [
                'attribute'=>'part_amt',
                'format' => ['decimal', 2],
                'pageSummary' => true
            ],
            [
                'attribute'=>'in_repair_emp',
                'value'=>function($model){
                    return !empty($model->reciveCarEmp)?$model->reciveCarEmp->nickname:null;//$model->reciveCarEmp->profile->first_name
                }
            ],
            'status_repair_name',

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::button('<span class="fa fa-eye"></span>', [
                                    'title' => Yii::t('app', 'view'),
                                    'class' => 'btn btn-info btn-xs',
                                    'onClick' => 'OpenViewDetail('.$model->id.')',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => Yii::t('app', 'edit'),
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalViewDetail">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">รายละเอียด</h4>
        </div>
        <div class="modal-body">Detail</div>
        <div class="modal-footer">
            <?php echo Html::button(Yii::t('app','Close'), ['class'=>'btn btn-default', 'data-dismiss'=>'modal']); ?>
        </div>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function OpenViewDetail(id){
        $("#modalViewDetail").modal("show");
        $.post("<?php echo Url::to(['/repaircar/view']);?>", {id:id}, function(data) {
            $(".modal-body").html(data);
        });

    }
</script>
