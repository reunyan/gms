
<?php
use yii\helpers\Html;
?>
<div class="">
	<table class="table table-bordered table-striped">
		<tr class="info">
			<th width="5%">ลำดับ</th>
			<th width="50%">รายการซ่อม</th>
			<!-- <th width="20%">ราคา</th> -->
			<th width="35%">หมายเหตุ</th>
			<th width="10%">จัดการ</th>
		</tr>
		<?php
		$i              = 0;
		foreach ($listRepairDetail as $valDetail) {
		?>
		<tr>
			<td><?php echo ++$i; ?></td>
			<td><?php echo $valDetail->repair_name; ?></td>
			<!-- <td class="text-right"><?php //echo $valDetail->repair_price; ?></td> -->
			<td><?php echo $valDetail->repair_remark; ?></td>
			<td><?php echo Html::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger btn-xs', 'onClick'=>'deleteRepairDetail('.$valDetail->id.')']); ?></td>
		</tr>
		<?php } ?>
	</table>
</div>