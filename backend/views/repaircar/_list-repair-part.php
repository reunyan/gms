
<?php
use yii\helpers\Html;
?>
<div class="col-md-12">
	<table class="table table-bordered table-striped">
		<tr class="info">
			<th width="5%">ลำดับ</th>
			<th width="35%">รายการอะไหล่</th>
			<th width="15%">ประเภท</th>
			<th width="15%">ผู้จัด</th>
			<th width="15%">คืนซาก</th>
			<th width="15%">ถ่ายรูปเทียบ</th>
			<th width="20%">หมายเหตุ</th>
			<th width="10%">จัดการ</th>
		</tr>
		<?php
		$i = 0;
		foreach ($listRepairPart as $valPart) {
		?>
		<tr>
			<td><?php echo ++$i; ?></td>
			<td><?php echo $valPart->part_name; ?></td>
			<td><?php echo $valPart->part_type_name; ?></td>
			<td><?php echo $valPart->part_owner_name; ?></td>
			<td><?php echo $valPart->part_return_name; ?></td>
			<td><?php echo $valPart->check_pic_desc; ?></td>
			<td><?php echo $valPart->part_remark; ?></td>
			<td><?php echo Html::button('<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger btn-xs', 'onClick'=>'deleteRepairPart('.$valPart->id.')']); ?></td>
		</tr>
		<?php } ?>
	</table>
</div>