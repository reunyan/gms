<?php

use yii\helpers\Html;
use yii\helpers\Url;

use kartik\widgets\ActiveForm;

$this->title = Yii::t('app', 'Recive Repair Car');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'List Repair Cars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
/* @var $model common\models\JmRepair */
/* @var $form yii\widgets\ActiveForm */
// echo '<pre>';print_r(Yii::$app->user->identity->comp->comp_name);echo '</pre>';
?>

<div class="panel panel-default">
    <div class="panel-body" style="padding: 20px;">
    <table width="100%">
        <tr>
            <td class="col-md-2 text-right"><img src="<?php echo Yii::getAlias('@web').'/imgs/logo_comp.png' ?>" style="margin-top: 20px;"></td>
            <td class="col-md-10 text-center">
                <h2><?php echo Yii::$app->user->identity->comp->comp_name; ?></h2>
                <h3><?php echo Yii::$app->user->identity->comp->comp_name_en; ?></h3>
                <h4><?php echo Yii::$app->user->identity->compBr->br_addr; ?></h4>
                <h4>
                    โทร <?php echo Yii::$app->user->identity->compBr->br_telno; ?>
                    แฟกซ์ <?php echo Yii::$app->user->identity->compBr->br_faxno; ?>
                    มือถือ <?php echo Yii::$app->user->identity->compBr->br_mobileno; ?>
                    E-Mail <?php echo Yii::$app->user->identity->compBr->br_email; ?>
                    </h4>
            </td>
        </tr>
    </table>
    <table width="100%" style="margin: 25px 0px 25px 0px;">
        <tr>
            <td class="col-md-2"></td>
            <td class="col-md-8 text-center"><h3><?php echo Yii::t('app', 'Recive Repair Car'); ?></h3></td>
            <td class="col-md-2"><h4>เลขที่ <?php echo $model->job_no; ?></h4></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="col-md-8">
                <table width="100%" style="border: solid 1px;">
                    <tr>
                        <th class="col-md-2" style="padding: 10px;">ชื่อลูกค้า</th>
                        <td class="col-md-10" style="padding: 10px;"><?php echo $model->in_repair_firstname.' '.$model->in_repair_lastname; ?></td>
                    </tr>
                    <tr>
                        <th class="col-md-2" style="padding: 10px;">เบอร์ติดต่อ</th>
                        <td class="col-md-10" style="padding: 10px;"><?php echo $model->in_repair_tel; ?></td>
                    </tr>
                    <tr>
                        <th class="col-md-2" style="padding: 10px;">รถ</th>
                        <td class="col-md-10" style="padding: 10px;"><?php echo $model->CarDetail; ?></td>
                    </tr>
                    <tr>
                        <th class="col-md-2" style="padding: 10px;">ประกัน</th>
                        <td class="col-md-10" style="padding: 10px;"><?php echo (!empty($model->insurance))?$model->insurance->insure_name:''; ?></td>
                    </tr>
                </table>
            </td>
            <td class="col-md-4" style="border: solid 1px;">
                <table width="100%">
                    <tr>
                        <th class="col-md-5" style="padding: 10px;">วันที่รถเข้าซ่อม</th>
                        <td class="col-md-7" style="padding: 10px;"><?php echo $model->in_repair_date; ?></td>
                    </tr>
                    <tr>
                        <th class="col-md-5" style="padding: 10px;">วันที่นัดรับรถ</th>
                        <td class="col-md-7" style="padding: 10px;"><?php echo $model->appoint_sent_date; ?></td>
                    </tr>
                    <tr>
                        <th class="col-md-5" style="padding: 10px;">&nbsp;</th>
                        <td class="col-md-7" style="padding: 10px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <th class="col-md-5" style="padding: 10px;">&nbsp;</th>
                        <td class="col-md-7" style="padding: 10px;">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%" style="margin-top: 40px;">
        <tr>
            <td class="col-md-2"></td>
            <td class="col-md-3 text-center" style="border-bottom: dotted;">&nbsp;</td>
            <td class="col-md-2"></td>
            <td class="col-md-3 text-center" style="border-bottom: dotted;">&nbsp;</td>
            <td class="col-md-2"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center"></td>
            <td></td>
            <td class="text-center" style="padding: 10px 0px 10px 0px;">(<?php echo $model->in_repair_firstname.' '.$model->in_repair_lastname; ?>)</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center">ผู้ตรวจรับรถ</td>
            <td></td>
            <td class="text-center">ลูกค้านำรถเข้าซ่อม</td>
            <td></td>
        </tr>
    </table>
    </div>
    <div class="panel-footer text-right">
        <?php echo Html::a('<i class="fa fa-print"></i> '.Yii::t('app', 'Print'), ['print-recive-repair', 'id'=>$model->id], ['class'=>'btn btn-primary btn-md', 'target'=>'_blank']); ?>
    </div>
</div>