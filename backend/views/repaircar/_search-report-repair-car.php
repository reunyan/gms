<?php

use yii\helpers\Html;

use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

$layout = <<< HTML
    {input1}
    {separator}
    {input2}
    <span class="input-group-addon kv-date-remove">
        <i class="fa fa-remove"></i>
    </span>
HTML;
?>

<div class="jm-repair-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-repair-car'],
        'method' => 'get',
    ]); ?>

    <div class="box box-default">
    <div class="box-header with-border"><i class="fa fa-search"></i></div>

    <div class="box-body">

    <div class="container-fluid">
        <div class="row">

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'job_no') ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'car_license_no') ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'insurance_id')->widget(Select2::classname(), [
                'data'=> Yii::$app->Utilities->getItemInsureName(),
                ]);
            ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'car_claim_type')->dropDownList(Yii::$app->Utilities->getItemCarClaimType()); ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'in_repair_type')->dropDownList(Yii::$app->Utilities->getItemInRepairType()); ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php
            echo $form->field($model, 'in_repair_date_start')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_RANGE,
                'attribute2' => 'in_repair_date_end',
                'separator' => 'ถึง',
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ])->label('วันที่จอดซ่อม');
        ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php
            echo $form->field($model, 'sent_car_date_start')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_RANGE,
                'attribute2' => 'sent_car_date_end',
                'separator' => 'ถึง',
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ])->label('วันที่ส่งรถ');
        ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php
            echo $form->field($model, 'appoint_sent_date_start')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_RANGE,
                'attribute2' => 'appoint_sent_date_end',
                'separator' => 'ถึง',
                'layout' => $layout,
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd-mm-yyyy'
                ]
            ])->label('วันที่นัดรับรถซ่อมเสร็จ');
        ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'payment_amt')->dropDownList(['0'=>'ไม่ระบุ', '1'=>'มีค่าเสียหายส่วนแรก', '2'=>'มีค่าใช้จ่ายอื่น ๆ ', '3'=>'มีค่าเสียหายส่วนแรกและค่าใช้จ่ายอื่นๆ'])->label('ค่าใช้จ่าย'); ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php
            echo $form->field($model, 'status_receive_money')->dropDownList(['0'=>'ไม่ระบุ', '1'=>'รับเงินแล้ว', '2'=>'ยังไม่ได้รับ'])->label('สถานะการรับเงิน');
        ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php
            echo $form->field($model, 'status_repair')->dropDownList(Yii::$app->Utilities->getItemRepairStatus());
        ?>
        </div>

        <div class="col-sm-4 col-md-3">
            <?php echo $form->field($model, 'in_repair_emp')->widget(Select2::classname(), [
                'data'=> Yii::$app->Utilities->getItemEmployeeName(),
                ]);
            ?>
        </div>

        </div>
    </div>

    </div>

    <div class="box-footer">
        <div class="col-md-12 text-right">
            <?php echo Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            <?php echo Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
