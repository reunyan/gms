<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PartShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Part shop');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-part-shop-index">

<?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            //'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            [
                'content'=>Html::a('<i class="fa fa-plus"></i> '.Yii::t('app','Create').Yii::t('app','Part shop'), ['create'], ['class' => 'btn btn-success']),
            ],
            // '{export}',
            //'{toggleData}',
        ],
        'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'shop_name',
            'address:ntext',
            [
                'attribute'=>'isactive',
                'value'=>function($model){
                    return Yii::$app->Utilities->getIsActiveDesc($model->isactive);
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>', $url, [
                                    'title' => 'view',
                                    'class' => 'btn btn-info btn-xs',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => 'edit',
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
