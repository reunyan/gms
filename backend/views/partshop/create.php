<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuPartShop */
$this->title = Yii::t('app','Create').Yii::t('app','Part shop');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Part shop'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-part-shop-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
