<?php

use yii\helpers\Html;

$this->title = $model->shop_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Part shop'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-insure-view">

    <p>
        <?php echo Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

     <div class="panel panel-info">
        <div class="panel-heading"><i class="fa fa-list"></i></div>
        <table class="table table-striped">
            <tr>
                <th><?php echo Yii::t('app', 'Shop Name');?></th>
                <td><?php echo $model->shop_name;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app', 'Address');?></th>
                <td><?php echo $model->address;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app', 'Tax No');?></th>
                <td><?php echo $model->tax_no;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app', 'Contact Name');?></th>
                <td><?php echo $model->contact_name;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app', 'Contact Telno');?></th>
                <td><?php echo $model->contact_telno;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app','Isactive');?></th>
                <td><?php echo Yii::$app->Utilities->getIsActiveDesc($model->isactive);?></td>
            </tr>
        </table>
    </div>

</div>
