<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuPartShop */

$this->title = Yii::t('app','Update').' ' . $model->shop_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Part Shops'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->shop_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tu-part-shop-update">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
