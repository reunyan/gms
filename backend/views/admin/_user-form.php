<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TuUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tu-user-form">

    <?php $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL, //TYPE_HORIZONTAL, TYPE_VERTICAL
        'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        //SIZE_TINY = 'xs',SIZE_SMALL = 'sm',SIZE_MEDIUM = 'md',SIZE_LARGE = 'lg'
    ]); ?>

    <div class="box <?php  echo ($model->isNewRecord)?'box-success':'box-warning'?>">
    <div class="box-header with-border">
        <i class="fa <?php  echo ($model->isNewRecord)?'fa-plus-square':'fa-edit'?>"></i>
    </div>
    <div class="box-body">
    <div class="container-fluid">

	<div class="row">
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'username')->textInput(['maxlength' => true, 'readOnly'=>!($model->isNewRecord)]) ?>
        </div>
        <?php if ($model->isNewRecord) { ?>
            <div class="col-sm-6 col-md-6">
                <?php echo $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
            </div>
        <?php } else { ?>
            <div class="col-sm-6 col-md-6">
                <?php echo $form->field($model, 'password')->hiddenInput()->label(false); ?>
            </div>
        <?php } ?>
    </div>    

    <div class="row">
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'nickname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'comp_id')->dropDownList(Yii::$app->Utilities->getItemCompany(false)); ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'comp_br_id')->dropDownList(Yii::$app->Utilities->getItemCompanyBranch(false)); ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'dep_id')->dropDownList(Yii::$app->Utilities->getItemDepartment()); ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'role_id')->dropDownList(Yii::$app->Utilities->getItemRole()); ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <?php echo $form->field($model, 'isactive')->dropDownList(Yii::$app->Utilities->getItemIsActive()); ?>
        </div>
    </div>

    </div>
    </div>
    <div class="box-footer text-right">
        <?php echo Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('app', 'Save').($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app', 'Update')), ['class' =>'btn btn-success']); ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
