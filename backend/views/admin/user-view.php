<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TuUser */

$this->title = Yii::t('app','User').' : '.$model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Manage').Yii::t('app','User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tu-user-view">
    <div class="panel panel-primary">
        <div class="panel-heading"><?php echo Yii::t('app','User');?></div>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'username',
                // 'password',
                'firstname',
                'lastname',
                'nickname',
                [  
                    'attribute' => 'comp_id',
                    'value' => (!empty($model->comp))?$model->comp->comp_name:null,
                ],
                [  
                    'attribute' => 'dep_id',
                    'value' => (!empty($model->dep))?$model->dep->dep_name:null,
                ],
                [  
                    'attribute' => 'role_id',
                    'value' => (!empty($model->role))?$model->role->role_name:null,
                ],
                // 'created_at',
                // 'created_by',
                // 'updated_at',
                // 'updated_by',
            ],
        ]) ?>
        <div class="panel-footer text-right"><?= Html::a('<i class="fa fa-edit"></i> แก้ไข', ['user-update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?></div>
    </div>
    

</div>
