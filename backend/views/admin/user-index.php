<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TuUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Manage').Yii::t('app','User');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>'primary',
            'heading'=> '<i class="fa fa-list"></i>',
            //'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            [
                'content'=>Html::a('<i class="fa fa-plus"></i> '.Yii::t('app','Create').Yii::t('app','User'), ['user-create'], ['class' => 'btn btn-success']),
            ],
            // '{export}',
            //'{toggleData}',
        ],
        'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'username',
            'firstname',
            'lastname',
            'nickname',
            [
                'attribute'=>'comp_id',
                'value'=>function($model){
                    return (!empty($model->company))?$model->company->comp_name:null;
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],
            [
                'attribute'=>'dep_id',
                'value'=>function($model){
                    return (!empty($model->department))?$model->department->dep_name:null;
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],
            [
                'attribute'=>'role_id',
                'value'=>function($model){
                    return (!empty($model->role))?$model->role->role_name:null;
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],
            [
                'attribute'=>'isactive',
                'value'=>function($model){
                    return Yii::$app->Utilities->getIsActiveDesc($model->isactive);
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{user-view} {user-update} {reset-password}',
                'buttons' => [
                    'user-view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>', $url, [
                                    'title' => 'view',
                                    'class' => 'btn btn-info btn-xs',
                        ]);
                    },

                    'user-update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => 'edit',
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },

                    'reset-password' => function ($url, $model) {
                        return Html::a('<span class="fa fa-undo"></span>', 'javascript:void(0)', [
                                    'title' => 'reset password',
                                    'class' => 'btn btn-success btn-xs',
                                    'onClick' => 'ResetPassword('.$model->id.')'
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
<script>
    function ResetPassword(UserID)
    {
        Swal.fire({
            title: "ยืนยันการเปลี่ยน Password",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: 'Reset',
        }).then((isConfirm) => {
            if (isConfirm.value) {
                $.post( "<?php echo Url::to(['admin/reset-password']);?>", {id:UserID}, function(data) {
                    Swal.fire({
                        title: "เปลี่ยน Password แล้ว",
                        icon: "success",
                    });
                });
            }
        });
    }
</script>
