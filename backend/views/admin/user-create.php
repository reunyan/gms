<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuUser */

$this->title = Yii::t('app','Create').Yii::t('app','User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','User'), 'url' => ['user-manage']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-user-create">

    <?= $this->render('_user-form', [
        'model' => $model,
    ]) ?>

</div>
