<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuUser */

$this->title = Yii::t('app','Update').Yii::t('app','User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','User'), 'url' => ['user-manage']];
// $this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['user-view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = ['label' => $model->username];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tu-user-update">

    <?= $this->render('_user-form', [
        'model' => $model,
    ]) ?>

</div>
