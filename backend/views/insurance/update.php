<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuInsure */

$this->title = Yii::t('app','Update').' ' . $model->insure_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Insurance'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->insure_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tu-insure-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
