<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TuInsure */

$this->title = $model->insure_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Insurance'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-insure-view">

    <p>
        <?php echo Html::a(Yii::t('app','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('app','Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

     <div class="panel panel-info">
        <div class="panel-heading"><i class="fa fa-list"></i></div>
        <table class="table table-striped">
            <tr>
                <th>ชื่อบริษัทประกันภัย</th>
                <td><?php echo $model->insure_name;?></td>
            </tr>
            <tr>
                <th>ที่อยู่</th>
                <td><?php echo $model->insure_addr;?></td>
            </tr>
            <tr>
                <th><?php echo Yii::t('app','Isactive');?></th>
                <td><?php echo Yii::$app->Utilities->getIsActiveDesc($model->isactive);?></td>
            </tr>
        </table>
    </div>

</div>
