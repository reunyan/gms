<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TuInsure */

$this->title = Yii::t('app','Create').Yii::t('app','Insurance');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Insurance'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-insure-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
