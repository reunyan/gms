<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TmModel */

$this->title = Yii::t('app','Update').' ' . $model->tmBrand->brand_name.' '.$model->model_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Model'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->model_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app','Update');
?>
<div class="tm-model-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
