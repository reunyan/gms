<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TmModel */

$this->title = Yii::t('app','Create').Yii::t('app','Car Model');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app','Car Model'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tm-model-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
