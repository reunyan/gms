<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TmModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app','Car Model');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tm-model-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'panel'=>[
            'type'=>GridView::TYPE_PRIMARY,
            'heading'=> '<i class="fa fa-list"></i>',
            //'before'=> false,
            'after'=> false,
        ],
        'toolbar'=> [
            [
                'content'=>Html::a('<i class="fa fa-plus"></i> '.Yii::t('app','Create').Yii::t('app','Car Model'), ['create'], ['class' => 'btn btn-success']),
            ],
            // '{export}',
            //'{toggleData}',
        ],
        'filterModel' => $searchModel,
        'headerRowOptions'=> ['class'=>'info'],
        'filterRowOptions'=> ['class'=>'info'],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'attribute' => 'brand_id',
                'vAlign' => 'middle',
                'value'=>function($model){
                    return (!empty($model->tmBrand))?$model->tmBrand->brand_name:null;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' =>Yii::$app->Utilities->getItemCarBrandName(),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'format' => 'raw'
            ],
            'model_name',
            [
                'attribute'=>'isactive',
                'value'=>function($model){
                    return Yii::$app->Utilities->getIsActiveDesc($model->isactive);
                },
                'format'=>'raw',
                'mergeHeader' => true,
            ],

            ['class' => 'kartik\grid\ActionColumn',
                'template'=>'{view} {update}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>', $url, [
                                    'title' => 'view',
                                    'class' => 'btn btn-info btn-xs',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => 'edit',
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
