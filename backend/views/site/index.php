<?php
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Main Page');

// echo '<pre>';print_r($data);echo '</pre>';
// echo '<pre>';print_r([0=>0]+$arrCountInRepair);echo '</pre>';exit;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header">
                    <h3 class="box-title">ข้อมูลรถเข้าซ่อม <?php echo Yii::$app->Helpers->thaiFormatDate($searchRepair->data_date); ?></h3>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">นัดเข้าซ่อม</span>
                    <span class="info-box-number"><?php echo $numAppointInRepairDaily; ?> คัน</span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-calendar-check-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">นัดรับรถ</span>
                    <span class="info-box-number"><?php echo $numAppointOutRepairDaily; ?> คัน</span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-car"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">จอดซ่อม</span>
                    <span class="info-box-number"><?php echo $numInRepairDaily; ?> คัน</span>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-wrench"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">ส่งรถ</span>
                    <span class="info-box-number"><?php echo $numOutRepairDaily; ?> คัน</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo 'รถนัดเข้าซ่อม-นัดส่งรถรายเดือน'; ?></h3>

                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                    <?php echo Highcharts::widget([
                       'options' => [
                          'title' => ['text' => 'ปี พ.ศ. '.($searchRepair->data_year+543)],
                          'xAxis' => [
                             // 'title' => ['text' => 'เดือน'],
                             'categories' => $arrMonth,
                          ],
                          'yAxis' => [
                             'title' => ['text' => 'จำนวน (คัน)']
                          ],
                          'series' => [
                             ['type' => 'line','name' => 'นัดเข้าซ่อม', 'color'=>'#3a87ad', 'data' => $arrCountAppointInRepair],
                             ['type' => 'line','name' => 'นัดรับรถ', 'color'=>'#ff1a1a', 'data' => $arrCountAppointOutRepair],
                          ]
                       ]
                    ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo 'รถจอดซ่อม-ส่งรถรายเดือน'; ?></h3>

                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                    <?php echo Highcharts::widget([
                       'options' => [
                          'title' => ['text' => 'ปี พ.ศ. '.($searchRepair->data_year+543)],
                          'xAxis' => [
                             // 'title' => ['text' => 'เดือน'],
                             'categories' => $arrMonth,
                          ],
                          'yAxis' => [
                             'title' => ['text' => 'จำนวน (คัน)']
                          ],
                          'series' => [
                             ['type' => 'column','name' => 'จอดซ่อม', 'color'=>'#00a65a', 'data' => $arrCountInRepair],
                             ['type' => 'column','name' => 'ส่งรถ', 'color'=>'#ff9933', 'data' => $arrCountOutRepair],
                          ]
                       ]
                    ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>