<?php

namespace backend\controllers;

use Yii;
use common\models\TuUser;
use common\models\TuUserSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * AdminController implements the CRUD actions for TuUser model.
 */
class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['user-manage', 'user-create', 'user-update', 'user-view', 'reset-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TuUser models.
     * @return mixed
     */
    public function actionUserManage()
    {
        $searchModel = new TuUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('user-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TuUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUserView($id)
    {
        return $this->render('user-view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TuUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUserCreate()
    {
        $model = new TuUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['user-manage']);
        }

        return $this->render('user-create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TuUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUserUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['user-manage']);
        }

        return $this->render('user-update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TuUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->isactive = 'N';
        $model->save();

        return $this->redirect(['user-manage']);
    }

    public function actionResetPassword()
    {
        $param = Yii::$app->request->post();
        if (!empty($param['id'])) {
            $model = $this->findModel($param['id']);
            $model->password = md5('password');
            $model->save();
        }        
    }

    /**
     * Finds the TuUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TuUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TuUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
