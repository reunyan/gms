<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;
use yii\helpers\Json;

use common\models\JmRepair;
use common\models\JmRepairSearch;
use common\models\JmRepairDetail;
use common\models\JmRepairPart;
use common\models\JmRepairClaim;
use common\models\JmRepairClaimSearch;
use common\models\JmRepairPartOrder;
use common\models\JmRepairPartOrderItem;

/**
 * Site controller
 */
class RepaircarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'update', 'view', 'add-repair-detail', 'delete-repair-detail', 'add-repair-part', 'delete-repair-part', 'view-recive-repair', 'print-recive-repair', 'add-repair-claim', 'delete-repair-claim', 'list-order-job', 'calendar-repair', 'add-repair-appoint', 'report-repair-car', 'view-repair-appoint', 'sent-repair-car', 'save-receive-money', 'delete-repair-appoint', 'search-car-receive', 'testdelete', 'list-calendar-repair', 'report-claim-cancel', 'partorder-list-item-part', 'partorder-save-list-part', 'delete-repair-partorder', 'print-repair-partorder'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel  = new JmRepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_repair' => [1,2,3]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSentRepairCar()
    {
        $searchModel  = new JmRepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_repair' => [4]]);

        return $this->render('sent-repair-car', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReportRepairCar()
    {
        $searchModel = new JmRepairSearch();
        $queryParams = Yii::$app->request->queryParams;
        $dataProvider = $searchModel->search($queryParams);
        // $dataProvider->query->andWhere(['<>','status_repair', 1]);
        return $this->render('report-repair-car', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JmRepair model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        $dataParam = (!empty(Yii::$app->request->post()))?Yii::$app->request->post():Yii::$app->request->queryParams;
        return $this->renderAjax('view', [
            'model' => $this->findModel($dataParam['id']),
        ]);
    }

    public function actionCalendarRepair()
    {
        $model       = new JmRepair();
        return $this->render('calendar-repair', [
            'model'=>$model,
        ]);
    }

    public function actionListCalendarRepair()
    {
        $searchModel = new JmRepairSearch();
        $modelRepair = $searchModel->queryCarInRepair();
        $events = [];
        foreach ($modelRepair as $valData) {
            if (!empty($valData->appoint_repair_date)) {
                array_push($events, ['id'=>$valData->id,
                                    'title'=>$valData->car_license_no.' จำนวน '.intval($valData->num_repair).' ชิ้น',
                                    'start'=>date('Y-m-d',strtotime($valData->appoint_repair_date)), 
                                    'color'=>'#3a87ad',
                                    ]
                                );
            }
        }
        
        $modelSend = $searchModel->queryCarAppointSend();
        foreach ($modelSend as $valData) {
            if (!empty($valData->appoint_sent_date)) {
                array_push($events, ['id'=>$valData->id,
                                    'title'=>$valData->car_license_no.' จำนวน '.intval($valData->num_repair).' ชิ้น',
                                    'start'=>date('Y-m-d',strtotime($valData->appoint_sent_date)),
                                    'color'=>'#ff1a1a',
                                    ]
                                );
            }    
        }
        return json_encode($events);
    }
    
    public function actionAddRepairAppoint()
    {
        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost)) {
            $model                           = new JmRepair();
            $model->appoint_repair_date      = $dataPost['appoint_date'];
            $model->car_license_no           = $dataPost['appoint_license_no'];
            $model->contact_repair_firstname = $dataPost['appoint_first_name'];
            $model->contact_repair_lastname  = $dataPost['appoint_last_name'];
            $model->contact_repair_tel       = $dataPost['appoint_telno'];
            $model->insurance_id             = $dataPost['repair_insure'];
            $model->num_repair               = $dataPost['num_repair'];
            $model->status_repair            = '1';
            if ($model->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function actionViewRepairAppoint()
    {
        $dataPost = Yii::$app->request->post();
        $model    = JmRepair::findOne($dataPost['id']);
        return $this->renderAjax('view-repair-appoint', [
            'model' => $model,
        ]);
    }


    public function actionDeleteRepairAppoint()
    {
        $dataPost             = Yii::$app->request->post();
        $model                = JmRepair::findOne($dataPost['id']);
        $model->status_repair = 6;
        if ($model->save()) {
            return true;
        } else {
            return "<pre>".$model->getErrors()."</pre>";
        }
    }
    public function actionViewReciveRepair($id)
    {
        return $this->render('view-recive-repair', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPrintReciveRepair($id)
    {
        $model        =  $this->findModel($id);
        $content      = $this->renderPartial('_print-recive-repair', ['model' => $model]);
        $strTitleName = 'ใบรับรถเข้าซ่อม';
        $strFileName  = 'receipt_car_'.$model->job_no;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' =>Pdf::FORMAT_A4,
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-2{font-size:22px}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $strFileName.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
        // return $this->renderPartial('_pdf-print-request-receipt', ['model' => $model]);
    }

    /**
     * Creates a new JmRepair model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JmRepair();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-recive-repair', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JmRepair model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model       = $this->findModel($id);
        $modelDetail = new JmRepairDetail();
        $modelPart   = new JmRepairPart();
        $modelClaim  = new JmRepairClaim();
        // echo '<pre>';print_r(Yii::$app->request->post());echo '</pre>';exit;
        if ($model->load(Yii::$app->request->post())) {
            $intSumFiratAmt         = JmRepairClaim::find()->where(['jm_repair_id'=>$id])->sum('first_claim_amt');
            $model->first_claim_amt = $intSumFiratAmt;
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        $listRepairDetail   = $this->findListRepairDetail($id);
        $listRepairPart     = $this->findListRepairPart($id);
        $listRepairPartOrder = $this->findListRepairPartOrder($id);
        $listRepairClaim    = $this->findListRepairClaim($id);
        return $this->render('update', [
            'model'             => $model,
            'modelDetail'       => $modelDetail,
            'listRepairDetail'  => $listRepairDetail,
            'modelPart'         => $modelPart,
            'listRepairPart'    => $listRepairPart,
            'modelClaim'        => $modelClaim,
            'listRepairClaim'   => $listRepairClaim,
            'listRepairPartOrder'=> $listRepairPartOrder,
        ]);
    }

    /**
     * Deletes an existing JmRepair model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JmRepair model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JmRepair the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairDetail($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairDetail::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return new JmRepairDetail();
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairPart($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairPart::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairPartGarage($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairPart::find()->where(['jm_repair_id'=>$id])->andWhere(['part_owner'=>2])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairPartOrder($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairPartOrder::find()->where(['jm_repair_id'=>$id])->andWhere(['isactive'=>'Y'])->orderBy(['order_part_seqno'=>SORT_ASC])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findRepairPartOrder($id)
    {
        if (($model = JmRepairPartOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairClaim($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairClaim::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddRepairDetail()
    {
        $dataPost             = Yii::$app->request->post();
        $model                = new JmRepairDetail();
        $model->jm_repair_id  = $dataPost['repair_id'];
        $model->repair_name   = $dataPost['repair_name'];
        // $model->repair_price  = $dataPost['repair_price'];
        $model->repair_remark = $dataPost['repair_remark'];
        if ($model->save()) {
            $listRepairDetail = $this->findListRepairDetail($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-detail', [
                'listRepairDetail' => $listRepairDetail,
            ]);
        }
    }

    public function actionDeleteRepairDetail()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairDetail::findOne($dataPost['id']);
        $repair_id = $model->jm_repair_id;
        if ($model->delete()) {
            $listRepairDetail = $this->findListRepairDetail($repair_id);
            return $this->renderPartial('_list-repair-detail', [
                'listRepairDetail' => $listRepairDetail,
            ]);
        }
    }

    public function actionAddRepairPart()
    {
        $dataPost             = Yii::$app->request->post();
        $model                = new JmRepairPart();
        $model->jm_repair_id  = $dataPost['repair_id'];
        $model->part_name     = $dataPost['part_name'];
        $model->part_type     = $dataPost['part_type'];
        $model->check_pic     = $dataPost['check_pic'];
        $model->part_owner    = $dataPost['part_owner'];
        $model->part_return   = $dataPost['part_return'];
        // $model->part_price = $dataPost['part_price'];
        $model->part_remark   = $dataPost['part_remark'];
        if ($model->save()) {
            $listRepairPart = $this->findListRepairPart($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-part', [
                'listRepairPart' => $listRepairPart,
            ]);
        }
    }

    public function actionDeleteRepairPart()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairPart::findOne($dataPost['id']);
        $repair_id = $model->jm_repair_id;
        if ($model->delete()) {
            $listRepairPart = $this->findListRepairPart($repair_id);
            return $this->renderPartial('_list-repair-part', [
                'listRepairPart' => $listRepairPart,
            ]);
        }
    }

    public function actionAddRepairClaim()
    {
        $dataPost               = Yii::$app->request->post();
        $model                  = new JmRepairClaim();
        $model->jm_repair_id    = $dataPost['repair_id'];
        $model->policy_no       = $dataPost['policy_no'];
        $model->claim_no        = $dataPost['claim_no'];
        $model->repair_amt      = $dataPost['repair_amt'];
        $model->part_amt        = $dataPost['part_amt'];
        $model->first_claim_amt = $dataPost['first_claim_amt'];
        if ($model->save()) {
            $listRepairClaim = $this->findListRepairClaim($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-claim', [
                'listRepairClaim' => $listRepairClaim,
            ]);
        }
    }

    public function actionDeleteRepairClaim()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairClaim::findOne($dataPost['id']);
        $model->isactive = 'N';
        $repair_id = $model->jm_repair_id;
        if ($model->save()) {
            $listRepairClaim = $this->findListRepairClaim($repair_id);
            return $this->renderPartial('_list-repair-claim', [
                'listRepairClaim' => $listRepairClaim,
            ]);
        }
    }

    public function actionListOrderJob($id)
    {
        $model = $this->findModel($id);
        if (empty($model->car_license_no) || ($model->payment_type == 2 && empty($model->jmRepairClaims))) {
            return "<h1>กรุณาระบุข้อมูลเลขที่ทะเบียนรถและเลขที่เคลมให้ครบถ้วน</h1>";
        }
        $modelPartOrder     = JmRepairPartOrder::find()->where(['jm_repair_id'=>$id])->andWhere(['isactive'=>'Y'])->groupBy('tu_part_shop_id')->all();
        // return $this->render('_print-list-order-job', ['model' => $model, 'modelPartOrder'=>$modelPartOrder]);
        $content      = $this->renderPartial('_print-list-order-job', ['model' => $model, 'modelPartOrder'=>$modelPartOrder]);
        $strTitleName = 'ใบสั่งงาน';
        $strFileName  = 'list_order_'.$model->job_no;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' =>Pdf::FORMAT_A4,
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;}
                            .ar{text-align:right}
                            .imgbd{border:1px solid}
                            .text-center { text-align: center; }
                            @media print{
                                .page-break{display: block;page-break-before: always;}
                            }',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>['Job No '.$model->job_no.' [{PAGENO}]'],
            ],
            'filename' => $strFileName.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionSaveReceiveMoney()
    {
        $dataPost                = Yii::$app->request->post();
        $model                   = JmRepair::findOne($dataPost['id']);
        $model->receive_money_at = date('Y-m-d');
        $model->receive_money_by = Yii::$app->user->getId();
        if ($model->save()) {
            return true;
        } else {
            return "<pre>".$model->getErrors()."</pre>";
        }
    }

    public function actionSearchCarReceive()
    {
        $searchModel  = new JmRepairSearch();
        $dataPost     = Yii::$app->request->post();
        $dataProvider = [];
        if (!empty($dataPost['JmRepairSearch']['car_license_no']) || !empty($dataPost['JmRepairSearch']['search_contact_repair_name'])) {
            $dataProvider = $searchModel->search($dataPost);
            $dataProvider->query->andWhere(['status_repair' => [1,6]]);
        }

        return $this->renderAjax('_search-car-receive', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReportClaimCancel()
    {
        $searchModel = new JmRepairClaimSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!empty($queryParams)) {
            $searchModel->load($queryParams);
            $dataProvider = $searchModel->SearchClaimCancel($queryParams);
            return $this->render('report-claim-cancel', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        return $this->render('report-claim-cancel', [
            'searchModel' => $searchModel,
        ]);
    }

    public function actionTestdelete()
    {
        $model = JmRepair::find()->where(['status_repair'=>1])->orderBy(['car_license_no'=>SORT_ASC])->all();
        $i = 0;
        foreach ($model as $valModel) {
            $modelSearch = JmRepair::find()
                            ->where(['>','status_repair',1])
                            ->andWhere(['car_license_no'=>$valModel->car_license_no])
                            ->andWhere(['>=', 'date(in_repair_date)', date('Y-m-d', strtotime($valModel->appoint_repair_date))])
                            ->one();
            if (!empty($modelSearch)) {
                echo '<pre>';print_r(++$i.') '.$valModel->id.' => '.$valModel->car_license_no.' : '.$modelSearch->status_repair.' [ '.$modelSearch->in_repair_date.' > '.$valModel->appoint_repair_date.']');echo '</pre>';
                if (!empty($valModel->appoint_repair_date)) {
                    $modelSearch->appoint_repair_date = $valModel->appoint_repair_date;
                    $modelSearch->save();
                }
                if ($valModel->delete()) {
                    echo '<pre>';print_r('success');echo '</pre>';
                }
            }
        }
    }
    // partorder-list-item-part
    public function actionPartorderListItemPart()
    {
        $dataParam = (!empty(Yii::$app->request->post()))?Yii::$app->request->post():Yii::$app->request->queryParams;
        return $this->renderAjax('_list-partorder-part-item', [
            'model' => $this->findListRepairPartGarage($dataParam['id']),
            'repair_id' => $dataParam['id'],
        ]);
    }
    // partorder-save-list-part
    public function actionPartorderSaveListPart()
    {
        $dataParam = Yii::$app->request->post();
        if (!empty($dataParam['repair_id']) && !empty($dataParam['part_items'])) {
            $checkOrderPart = JmRepairPartOrder::find()->where(['jm_repair_id'=>$dataParam['repair_id']])->count();
            $modelOrderPart = new JmRepairPartOrder;
            $modelOrderPart->jm_repair_id = $dataParam['repair_id'];
            $modelOrderPart->order_part_no = Yii::$app->Utilities->getOrderPartNo();
            $modelOrderPart->order_part_seqno = intval($checkOrderPart) + 1;
            $modelOrderPart->order_part_date = date('Y-m-d');
            $modelOrderPart->order_part_by = Yii::$app->user->getId();
            $modelOrderPart->tu_part_shop_id = $dataParam['partshop_id'];
            if ($modelOrderPart->save()) {
                foreach ($dataParam['part_items'] as $valPart) {
                    $modelOrderPartItem = new JmRepairPartOrderItem;
                    $modelOrderPartItem->jm_repair_part_order_id = $modelOrderPart->id;
                    $modelOrderPartItem->jm_repair_part_id = $valPart;
                    $modelOrderPartItem->save();
                }
            }
        }

        $listRepairPartOrder = $this->findListRepairPartOrder($dataParam['repair_id']);

        return $this->renderPartial('_list-repair-part-order', [
            'listRepairPartOrder' => $listRepairPartOrder,
        ]);
    }

    // delete-repair-partorder
    public function actionDeleteRepairPartorder()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairPartOrder::findOne($dataPost['id']);
        $model->isactive = 'N';
        $repair_id = $model->jm_repair_id;
        if ($model->save()) {
            $listRepairPartOrder = $this->findListRepairPartOrder($repair_id);

            return $this->renderPartial('_list-repair-part-order', [
                'listRepairPartOrder' => $listRepairPartOrder,
            ]);
        }
    }
    // print-repair-partorder
    public function actionPrintRepairPartorder($id)
    {
        $model = $this->findRepairPartOrder($id);
        $modelComp = \common\models\TuCompBr::findOne(1);
        $modelRepair = $this->findModel($model->jm_repair_id);
        // return $this->render('_print-repair-partorder', ['model' => $model, 'modelComp'=>$modelComp]);
        $content      = $this->renderPartial('_print-repair-partorder', ['model' => $model, 'modelComp'=>$modelComp, 'modelRepair'=>$modelRepair]);
        $strTitleName = 'ใบสั่งอะไหล่';
        $strFileName  = 'part_order_'.$model->order_part_no;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' =>Pdf::FORMAT_A4,
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;}
                            .ar{text-align:right}
                            .imgbd{border:1px solid}
                            .text-center { text-align: center; }
                            @media print{
                                .page-break{display: block;page-break-before: always;}
                            }',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>['Document No '.$model->order_part_no.' [{PAGENO}]'],
            ],
            'filename' => $strFileName.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}
