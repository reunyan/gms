<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use \common\models\JmRepair;
use \common\models\JmRepairClaim;
use \common\models\JmRepairClaimSearch;

class BillingController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'print-billing', 'print-billing-select', 'cancel-billing', 'billing-by-insurance', 'print-billing-by-insurance', 'report-billing', 'delete-billing', 'update-billing', 'save-update-billing', 'print-invoice', 'print-invoice-select'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'cancel-billing' => ['post'],
                ],
            ],
        ];
    }

    protected function findModelClaim($id)
    {
        if (($model = JmRepairClaim::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionIndex()
    {
		$dataParams   = Yii::$app->request->post();
        // echo '<pre>';print_r($dataParams);echo '</pre>';exit;
        if (!empty($dataParams['JmRepairClaimSearch']['billing_status'])) {
        	if (!empty($dataParams['selection'])) {
                if ($dataParams['JmRepairClaimSearch']['billing_status'] == 3) {
                    $sessionPrint = Yii::$app->session;
                    $sessionPrint->open();
                    $sessionPrint['print_id'] = $dataParams['selection'];
                    return $this->redirect(['print-billing-select']);
                } else {
                    foreach ($dataParams['selection'] as $valSelect) {
                        $model = JmRepairClaim::findOne($valSelect);
                        switch ($dataParams['JmRepairClaimSearch']['billing_status']) {
                            case 1:
                                if ($dataParams['JmRepairClaimSearch']['select_process_status']=='B') {
                                    $model->billing_status = 2;
                                    $model->billing_date   = $dataParams['JmRepairClaimSearch']['process_date'];
                                    $model->billing_at     = date('Y-m-d H:i:s');
                                    $model->billing_by     = Yii::$app->user->getId();
                                } else if ($dataParams['JmRepairClaimSearch']['select_process_status']=='I') {
                                    $model->billing_status      = 4;
                                    $model->before_billing_date = $dataParams['JmRepairClaimSearch']['process_date'];
                                }
                                
                            break;
    
                            case 2:
                                $model->billing_status = 3;
                                $model->receive_date   = $dataParams['JmRepairClaimSearch']['receive_date'];
                                $model->receive_at     = date('Y-m-d H:i:s');
                                $model->receive_by     = Yii::$app->user->getId();
                            break;

                            case 4:
                                $model->billing_status = 2;
                                $model->billing_date   = $dataParams['JmRepairClaimSearch']['billing_date'];
                            break;
                        }
                        // echo "<pre>";print_r($model);exit;echo "</pre>";
                        $model->save();
                    }
                }	        	
        	} else {
        		Yii::$app->getSession()->setFlash('warning',[
	                'body'=>'กรุณาเลือกรายการอย่างน้อย 1 รายการ',
	            ]);
        	}
        }

		$searchModel  = new JmRepairClaimSearch();
        $dataParamsSearch   = Yii::$app->request->queryParams;
        if (empty($dataParams['JmRepairClaimSearch']['billing_status'])) {
        	$searchModel->billing_status = 1;
        }
        // echo '<pre>';print_r(Yii::$app->request->queryParams);echo '</pre>';exit;
        $dataProvider = $searchModel->searchBilling(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrintBilling($id)
    {
    	$model = $this->findModelClaim($id);
    	// return $this->render('_print-billing-repair', [
     //        'model' => $model,
     //    ]);
        // get your HTML raw content without any layouts or scripts
	    $content = $this->renderPartial('_print-billing-repair', ['model' => $model]);

	    $strTitleName = 'ใบกำกับภาษี/ใบเสร็จรับเงิน';
        $strFileName  = 'print_receipt_'.$id;

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 10,
            'marginRight' => 10,
            'marginTop' => 10,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $strFileName.'.pdf',
        ]);

	    // return the pdf output as per the destination setting
	    return $pdf->render();
    }

    public function actionPrintBillingSelect()
    {
        // $session = Yii::$app->session;
        // $session->open();
        // $arrId = $session['print_id'];
        // $session->remove('print_id');
        // $session->close();
        $dataParams = Yii::$app->request;
        $arrHeader = explode(':',$dataParams->headers['cookie']);
        foreach($arrHeader as $valHead){
            $arrCheckPrint = explode('=',$valHead);
            if ($arrCheckPrint[0]=='print_id') {
                $arrId = explode(',',$arrCheckPrint[1]);
                break;
            }
        }
        // echo "<pre>";print_r($arrID);echo "</pre>";exit;
        if (!empty($arrId)) {
            $models = JmRepairClaim::find()->where(['id'=>$arrId])->all();
            // return $this->render('_print-billing-repair-select', [
            //     'models' => $models,
            // ]);
            // get your HTML raw content without any layouts or scripts
            $content = $this->renderPartial('_print-billing-repair-select', ['models' => $models]);

            $strTitleName = 'ใบกำกับภาษี/ใบเสร็จรับเงิน';
            $strFileName  = 'print_receipts';

            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_UTF8,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                'marginLeft' => 10,
                'marginRight' => 10,
                'marginTop' => 10,
                'marginBottom' => false,
                'marginHeader' => false,
                'marginFooter' => 1,
                // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@backend/web/css/pdf.css',
                // any css to be embedded if required
                'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
                 // set mPDF properties on the fly
                'options' => [
                    'title' => $strTitleName,
                ],
                 // call mPDF methods on the fly
                'methods' => [
                    'SetHeader'=>false,
                    'SetFooter'=>false,
                ],
                'filename' => $strFileName.'.pdf',
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();
        } else {
            echo 'ไม่พบรายการที่เลือก';
        }
    }
    
    public function actionPrintInvoice($id)
    {
    	$model = $this->findModelClaim($id);
    	// return $this->render('_print-billing-repair', [
     //        'model' => $model,
     //    ]);
        // get your HTML raw content without any layouts or scripts
	    $content = $this->renderPartial('_print-invoice-repair', ['model' => $model]);

	    $strTitleName = 'ใบแจ้งหนี้';
        $strFileName  = 'print_invoice_'.$id;

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 10,
            'marginRight' => 10,
            'marginTop' => 10,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $strFileName.'.pdf',
        ]);

	    // return the pdf output as per the destination setting
	    return $pdf->render();
    }

    public function actionPrintInvoiceSelect()
    {
        // $session = Yii::$app->session;
        // $session->open();
        // $arrId = $session['print_id'];
        // $session->remove('print_id');
        // $session->close();
        $dataParams = Yii::$app->request;
        $arrHeader = explode(':',$dataParams->headers['cookie']);
        foreach($arrHeader as $valHead){
            $arrCheckPrint = explode('=',$valHead);
            if ($arrCheckPrint[0]=='print_id') {
                $arrId = explode(',',$arrCheckPrint[1]);
                break;
            }
        }
        // echo "<pre>";print_r($arrID);echo "</pre>";exit;
        if (!empty($arrId)) {
            $models = JmRepairClaim::find()->where(['id'=>$arrId])->andWhere(['IS NOT','billing_no', null])->all();
            // return $this->render('_print-billing-repair-select', [
            //     'models' => $models,
            // ]);
            // get your HTML raw content without any layouts or scripts
            $content = $this->renderPartial('_print-invoice-repair-select', ['models' => $models]);

            $strTitleName = 'ใบแจ้งหนี้';
            $strFileName  = 'print_invoice';

            // setup kartik\mpdf\Pdf component
            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_UTF8,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                'marginLeft' => 10,
                'marginRight' => 10,
                'marginTop' => 10,
                'marginBottom' => false,
                'marginHeader' => false,
                'marginFooter' => 1,
                // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting
                'cssFile' => '@backend/web/css/pdf.css',
                // any css to be embedded if required
                'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
                 // set mPDF properties on the fly
                'options' => [
                    'title' => $strTitleName,
                ],
                 // call mPDF methods on the fly
                'methods' => [
                    'SetHeader'=>false,
                    'SetFooter'=>false,
                ],
                'filename' => $strFileName.'.pdf',
            ]);

            // return the pdf output as per the destination setting
            return $pdf->render();
        } else {
            echo 'ไม่พบรายการที่เลือก';
        }
    }

    public function actionCancelBilling($id)
    {
		$model                 = $this->findModelClaim($id);
		$model->billing_status = 1;
		$model->billing_date   = null;
		$model->invoice_no     = null;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBillingByInsurance()
    {
        // echo "<pre>";print_r(Yii::$app->request->post());echo "</pre>";exit;
		$model        = new JmRepairClaimSearch();
        $dataParams   = Yii::$app->request->post();
        $model->load($dataParams);
        $dataProvider = $model->ClaimByInsurance($dataParams);

        return $this->render('billing-by-insurance', [
        	'model'=>$model,
        	'dataProvider'=>$dataProvider,
        ]);
    }

    public function actionPrintBillingByInsurance()
    {
		$dataParams          = !empty(Yii::$app->request->post())?Yii::$app->request->post():Yii::$app->request->queryParams;
		$model               = new JmRepairClaimSearch();
		$model->billing_date = $dataParams['billing_date'];
        $model->insurance_id = $dataParams['insurance'];
        // echo "<pre>";print_r($model);echo "</pre>";exit;
		$dataQuery           = $model->ClaimByInsuranceDetail();
        // echo "<pre>";print_r($dataQuery);echo "</pre>";exit;

        // get your HTML raw content without any layouts or scripts
	    $content = $this->renderPartial('_print-billing-by-insurance', ['model' => $dataQuery]);

	    $strTitleName = 'ใบวางบิล';
        $strFileName  = 'print_receipt_insurance_'.date('Ymd');

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            'marginLeft' => 5,
            'marginRight' => 5,
            'marginTop' => 10,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $strFileName.'.pdf',
        ]);

	    // return the pdf output as per the destination setting
	    return $pdf->render();
    }

    public function actionReportBilling()
    {
		$dataParams   = Yii::$app->request->queryParams;
		$searchModel  = new JmRepairClaimSearch();
        // if (empty($dataParams)) {
        //     $searchModel->dateBillingStart = date('d-m-Y');
        //     $searchModel->dateBillingEnd = date('d-m-Y');
        // }
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		// echo '<pre>';print_r($searchModel);echo '</pre>';exit;
        $dataProvider->setSort([
                'defaultOrder' => ['invoice_no' => SORT_ASC],
            ]);

        return $this->render('report-billing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDeleteBilling($id)
    {
        $model                 = $this->findModelClaim($id);
        $model->billing_status = null;
        $model->billing_date   = null;
        $model->invoice_no     = null;
        if ($model->save()) {
            $modelRepair = JmRepair::find()->where(['id'=>$model->jm_repair_id])->one();
            if (!empty($modelRepair)) {
                $modelRepair->status_repair = 3;
                $modelRepair->save();
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdateBilling()
    {
        $dataPost = Yii::$app->request->post();
        $model    = $this->findModelClaim($dataPost['id']);
        return $this->renderPartial('update-billing', [
            'model' => $model,
        ]);
    }

    public function actionSaveUpdateBilling()
    {
        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost['JmRepairClaim'])) {
            $model             = JmRepairClaim::findOne($dataPost['JmRepairClaim']['id']);
            $model->repair_amt = $dataPost['JmRepairClaim']['repair_amt'];
            $model->part_amt   = $dataPost['JmRepairClaim']['part_amt'];
            if ($model->save()) {
                return true;
            } else {
                echo '<pre>';print_r($model->getErrors());echo '</pre>';exit;
            }
        }
    }

}
