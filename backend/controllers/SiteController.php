<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use common\models\LoginForm;
use common\models\JmRepairSearch;
use common\models\JmRepairClaimSearch;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'admin-test'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataMonth = Yii::$app->Helpers->listMonth('TH', 'S', false);
        for ($i=0; $i <= 11 ; $i++) {
            $arrMonth[$i] = $dataMonth[$i+1];
        }
        $searchRepair             = new JmRepairSearch();
        $searchRepair->data_year  = date('Y');
        $searchRepair->data_date  = date('Y-m-d');
        $arrCountAppointInRepair  = $searchRepair->countCarAppointInRepair();
        $arrCountAppointOutRepair = $searchRepair->countCarAppointOutRepair();
        $arrCountInRepair         = $searchRepair->countCarInRepair();
        $arrCountOutRepair        = $searchRepair->countCarOutRepair();
        $numAppointInRepairDaily  = $searchRepair->countCarStatusRepairDaily(1);
        $numAppointOutRepairDaily = $searchRepair->countCarStatusRepairDaily(2);
        $numInRepairDaily         = $searchRepair->countCarStatusRepairDaily(3);
        $numOutRepairDaily        = $searchRepair->countCarStatusRepairDaily(4);

        return $this->render('index', [
            'arrMonth'                 => $arrMonth,
            'searchRepair'             => $searchRepair,
            'numAppointInRepairDaily'  => $numAppointInRepairDaily,
            'numAppointOutRepairDaily' => $numAppointOutRepairDaily,
            'numInRepairDaily'         => $numInRepairDaily,
            'numOutRepairDaily'        => $numOutRepairDaily,
            'arrCountAppointInRepair'  => $arrCountAppointInRepair,
            'arrCountAppointOutRepair' => $arrCountAppointOutRepair,
            'arrCountInRepair'         => $arrCountInRepair,
            'arrCountOutRepair'        => $arrCountOutRepair,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAdminTest(){
        echo Yii::getVersion();
    }
}
