<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use kartik\helpers\Enum;

class JsoncarController extends Controller
{
    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];

        foreach ($datas as $key => $value) {
            if (trim($value->{$fieldId}) != '') {
                array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
            }
        }

        return $obj;
    }

    public function actionGetcarmodel()
    {
        $out = [];

         if (isset($_POST['depdrop_parents'])) {
             $parents = $_POST['depdrop_parents'];

             if ($parents != null) {
                $strCarModel = $parents[0];

                $out         = $this->MapData(Yii::$app->Utilities->getListCarModel($strCarModel)->all(),'id','model_name');
                echo Json::encode(['output'=>$out, 'selected'=>'']);
                return;
             }
         }

         echo Json::encode(['output'=>'', 'selected'=>'']);

    }

}
