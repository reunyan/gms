<?php
use yii\helpers\Html;
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php if (!Yii::$app->user->isGuest) { ?>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?php echo Yii::$app->user->identity->username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <p>
                                <?php echo Yii::$app->user->identity->firstname; ?>
                                <?php echo Yii::$app->user->identity->lastname; ?>
                                (<?php echo Yii::$app->user->identity->nickname; ?>)
                                <small><?php echo Yii::$app->user->identity->dep->dep_name; ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <!-- <div class="pull-left">
                                <?
                                // Html::a(
                                //     'Profile',
                                //     ['/user/profile/show', 'id'=>Yii::$app->user->identity->id],
                                //     ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                               // ) ?>
                            </div> -->
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>
