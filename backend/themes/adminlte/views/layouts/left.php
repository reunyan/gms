<aside class="main-sidebar">
    <section class="sidebar">
        <?php if (!Yii::$app->user->isGuest) {
            $arrMenu = [
                    ['label' => Yii::t('app','Calendar Repair Cars'), 'icon' => 'calendar', 'url' => ['/repaircar/calendar-repair']],
                    ['label' => Yii::t('app','List Repair Cars'), 'icon' => 'car', 'url' => ['/repaircar']],
                    ['label' => Yii::t('app','List Sent Cars'), 'icon' => 'car', 'visible' => Yii::$app->Authcontrol->UserIsAdmin(), 'url' => ['/repaircar/sent-repair-car']],
                    ['label' => Yii::t('app','Report Repair Cars'), 'icon' => 'list', 'url' => ['/repaircar/report-repair-car']],
                    ['label' => Yii::t('app','List Billing'), 'icon' => 'btc', 'url' => ['/billing'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','List Billing By Insurance'), 'icon' => 'btc', 'url' => ['/billing/billing-by-insurance'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Report Billing'), 'icon' => 'list', 'url' => ['/billing/report-billing'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Report Claim Cancel'), 'icon' => 'list', 'url' => ['/repaircar/report-claim-cancel'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage User'), 'icon' => 'user-circle', 'url' => ['/admin/user-manage'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage Insurance'), 'icon' => 'cog', 'url' => ['/insurance'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage Car Brand'), 'icon' => 'car', 'url' => ['/carbrand'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage Car Model'), 'icon' => 'car', 'url' => ['/carmodel'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage Car Color'), 'icon' => 'car', 'url' => ['/carcolor'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                    ['label' => Yii::t('app','Manage Part Shop'), 'icon' => 'home', 'url' => ['/partshop'], 'visible' => Yii::$app->Authcontrol->UserIsAdmin()],
                ];
        } else {
             $arrMenu = [
                ['label' => Yii::t('app','Sign In'), 'icon' => 'unlock-alt', 'url' => ['/site/login']],
             ];
        } ?>

        <?php echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => $arrMenu,
            ]
        ) ?>

    </section>

</aside>
