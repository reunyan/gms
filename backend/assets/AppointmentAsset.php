<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppointmentAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/fullcalendar.min.css',
    ];
    public $js = [
        'js/helper.js',
        'js/moment.js',
        'js/fullcalendar.min.js',
        'js/th.js',
        'js/fullcalendar.init.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
