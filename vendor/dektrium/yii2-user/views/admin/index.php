<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<?= $this->render('/admin/_menu') ?>

<?php Pjax::begin() ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'headerRowOptions'=> ['class'=>'info'],
    'filterRowOptions'=> ['class'=>'info'],
    'layout'       => "{items}\n{pager}",
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        'username',
        [
            'label' => Yii::t('app', 'User Full Name'),
            'mergeHeader' => true,
            'value' => function($model){
                return (!empty($model->profile))?$model->profile->first_name.' '.$model->profile->last_name.' ('.$model->profile->nick_name.')':null;
            }
        ],
        [
            'label' => Yii::t('app', 'User Role'),
            'mergeHeader' => true,
            'value' => function($model){
                return (!empty($model->profile) && !empty($model->profile->role))?$model->profile->role->role_name:null;
            }
        ],
        [
          'attribute' => 'last_login_at',
          'mergeHeader' => true,
          'value' => function ($model) {
            if (!$model->last_login_at || $model->last_login_at == 0) {
                return Yii::t('user', 'Never');
            } else if (extension_loaded('intl')) {
                return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
            } else {
                return date('Y-m-d G:i:s', $model->last_login_at);
            }
          },
        ],
        [
            'label' => Yii::t('user', 'Block status'),
            'mergeHeader' => true,
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'template' => '{update} {delete}',
            'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="fa fa-edit"></span>', $url, [
                                    'title' => Yii::t('app', 'edit'),
                                    'class' => 'btn btn-warning btn-xs',
                        ]);
                    },

                    'delete' => function ($url, $model) {
                        return Html::a('<span class="fa fa-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'delete'),
                                    'class' => 'btn btn-danger btn-xs',
                        ]);
                    },
            ]
        ],
    ],
]); ?>

<?php Pjax::end() ?>
