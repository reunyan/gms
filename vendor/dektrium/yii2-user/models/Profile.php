<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace dektrium\user\models;

use Yii;
use dektrium\user\traits\ModuleTrait;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\TuComp;
use common\models\TuCompBr;
use common\models\TuDep;
use common\models\TuRole;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string  $name
 * @property string  $public_email
 * @property string  $gravatar_email
 * @property string  $gravatar_id
 * @property string  $location
 * @property string  $website
 * @property string  $bio
 * @property string  $timezone
 * @property User    $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class Profile extends ActiveRecord
{
    use ModuleTrait;
    /** @var \dektrium\user\Module */
    protected $module;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /** @inheritdoc */
    public function init()
    {
        $this->module = \Yii::$app->getModule('user');
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'comp_id', 'comp_br_id', 'dep_id', 'role_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['nick_name'], 'string', 'max' => 150],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TuComp::className(), 'targetAttribute' => ['comp_id' => 'id']],
            [['comp_br_id'], 'exist', 'skipOnError' => true, 'targetClass' => TuCompBr::className(), 'targetAttribute' => ['comp_br_id' => 'id']],
            [['dep_id'], 'exist', 'skipOnError' => true, 'targetClass' => TuDep::className(), 'targetAttribute' => ['dep_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => TuRole::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'    => 'User ID',
            'first_name' => 'ชื่อ',
            'last_name'  => 'นามสกุล',
            'nick_name'  => 'ชื่อเล่น',
            'comp_id'    => 'บริษัท',
            'comp_br_id' => 'สาขา',
            'dep_id'     => 'แผนก',
            'role_id'    => 'สิทธิใช้งาน',
            'created_at' => Yii::t('app','Created At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_at' => Yii::t('app','Updated At'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(TuComp::className(), ['id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompBr()
    {
        return $this->hasOne(TuCompBr::className(), ['id' => 'comp_br_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDep()
    {
        return $this->hasOne(TuDep::className(), ['id' => 'dep_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(TuRole::className(), ['id' => 'role_id']);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }
}
