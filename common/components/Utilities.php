<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

use common\models\TuInsure;
use common\models\TuProvince;
use common\models\TmBrand;
use common\models\TmModel;
use common\models\TmColor;
use common\models\TuRun;
use common\models\TuRef;
use common\models\TuUser;
use common\models\TmCarType;
use common\models\TuPartShop;

class Utilities extends Component
{
    public function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];

        foreach ($datas as $key => $value) {
            if (trim($value->{$fieldId}) != '') {
                array_push($obj, ['id'=>$value->{$fieldId},'name'=>$value->{$fieldName}]);
            }
        }

        return $obj;
    }

    public function GetErrorModel($model)
    {
        $strError = '';
        foreach ($model->getErrors() as $key => $value) {
            $strError .= $value[0].', ';
        }
        return $strError;
    }

    public function getValueTurun($run_type, $run_code)
    {
        $model = TuRun::find()
                ->where(['run_type' => $run_type])
                ->andWhere(['run_code'=> $run_code])
                ->one();

        return $model;
    }

    public function getLateNumberTurun($run_type, $run_code)
    {
        $modelRun =  $this->getValueTurun($run_type, $run_code);

        if (empty($modelRun)) {
            $modelNew           = new TuRun();
            $modelNew->run_type = $run_type;
            $modelNew->run_code = $run_code;
            $modelNew->run_no   = 1;
            $modelNew->save();
            return 1;
        } else {
            $intCurent        = (intval($modelRun->run_no) + 1);
            $modelRun->run_no = $intCurent;
            $modelRun->save();

            return $intCurent;
        }
    }

    public function getJobNumber()
    {
        $intLastNo    = $this->getLateNumberTurun('JOBNO', date('y-m'));
        $intRunNumber = date('y').'-'.substr('00'.date('m'), -2).'-'.substr('0000'.$intLastNo, -4);
        return $intRunNumber;
    }

    public function getInvoiceNumber()
    {
        $strYear       = substr(intval(date('Y')) + 543, -2);
        $strMonth      = substr('00'.intval(date('m')), -2);
        $intLastNo     = $this->getLateNumberTurun('INV', $strYear.$strMonth);
        $intRunNumber  = $strYear.$strMonth.'-'.substr('000'.$intLastNo, -3);
        return $intRunNumber;
    }    

    public function getBillingNumber()
    {
        $strYear       = substr(intval(date('Y')) + 543, -2);
        $strMonth      = substr('00'.intval(date('m')), -2);
        $intLastNo     = $this->getLateNumberTurun('BIL', $strYear.$strMonth);
        $intRunNumber  = 'BIL-'.$strYear.$strMonth.'-'.substr('000'.$intLastNo, -3);
        return $intRunNumber;
    }

    public function getDescReferance($ref_type, $ref_code, $ref_value)
    {
        $model = TuRef::find()
                ->where(['ref_type' => $ref_type])
                ->andWhere(['ref_code'=> $ref_code])
                ->andWhere(['ref_value'=> $ref_value])
                ->one();
        if (empty($model)) {
            return '';
        } else {
            return $model->ref_desc;
        }
    }

    public function getListReferance($ref_type, $ref_code)
    {
        return TuRef::find()
                ->where(['ref_type' => $ref_type])
                ->andWhere(['ref_code'=> $ref_code])
                ->orderBy(['ref_seq'=>SORT_ASC]);
    }

    public function getItemReferance($bolNoSelect=true, $ref_type, $ref_code)
    {
        $arrData = ArrayHelper::map($this->getListReferance($ref_type, $ref_code)->asArray()->all(), 'ref_value', 'ref_desc');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }
        return $arrayData;
    }

    public function getItemCarClaimType($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'REPAIRCAR', 'CARCLAIMTYPE');
    }

    public function getItemInsureType($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'REPAIRCAR', 'INSURETYPE');
    }

    public function getItemInRepairType($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'REPAIRCAR', 'CARSTOP');
    }

    public function getItemPaymentType($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'REPAIRCAR', 'PAYMENTTYPE');
    }

    public function getItemPartType($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'PART', 'PARTTYPE');
    }

    public function getItemRepairStatus($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListReferance('REPAIRCAR', 'STATUS')->andWhere(['ref_value'=>[1,2,3,4,5]])->asArray()->all(), 'ref_value', 'ref_desc');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }
        return $arrayData;
    }

    public function getItemPartOwner($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'PART', 'OWNER');
    }

    public function getItemPartReturn($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'PART', 'RETURN');
    }

    public function getItemRepairStatusCreate($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListReferance('REPAIRCAR', 'STATUS')->andWhere(['ref_value'=>[1,2,3]])->asArray()->all(), 'ref_value', 'ref_desc');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }
        return $arrayData;
    }

    public function getItemBillingStatus($bolNoSelect=true)
    {
        return $this->getItemReferance($bolNoSelect, 'BILLING', 'STATUS');
    }

    public function getListEmployee($intUserID=null)
    {
        $model = TuUser::find()
                ->select(['id', "CONCAT(firstname,' ', lastname,' ', nickname) AS full_name"])
                ->andFilterWhere(['id'=>$intUserID])
                ->orderBy(['firstname'=>SORT_ASC]);

        return $model;
    }

    public function getItemEmployeeName($bolNoSelect=true, $intUserID=null)
    {
        $arrData = ArrayHelper::map($this->getListEmployee($intUserID)->asArray()->all(), 'id', 'full_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListInsure()
    {
        $model = TuInsure::find()
                ->where(['isactive'=>'Y'])
                ->orderBy(['insure_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemInsureName($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListInsure()->asArray()->all(), 'id', 'insure_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListProvince()
    {
        $model = TuProvince::find()
                ->orderBy(['province_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemProvinceName($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListProvince()->asArray()->all(), 'id', 'province_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListCarBrand()
    {
        $model = TmBrand::find()
                ->orderBy(['brand_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemCarBrandName($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListCarBrand()->asArray()->all(), 'id', 'brand_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListCarModel($intBrandID=null)
    {
        $model = TmModel::find()
                ->andFilterWhere(['brand_id'=>$intBrandID])
                ->orderBy(['model_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemCarModelName($bolNoSelect=true, $intBrandID=null)
    {
        $arrData = ArrayHelper::map($this->getListCarModel($intBrandID)->asArray()->all(), 'id', 'model_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListCarColor()
    {
        $model = TmColor::find()
                ->orderBy(['color_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemCarColorName($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListCarColor()->asArray()->all(), 'id', 'color_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function arrayCheckPic()
    {
        return ['Y'=>'เทียบ', 'N'=>'ไม่'];
    }

    public function getItemCheckPic($bolNoSelect=true)
    {
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $this->arrayCheckPic();
        } else {
            $arrayData = $this->arrayCheckPic();
        }
        return $arrayData;
    }

    public function getListCompany()
    {
        $model = \common\models\TuComp::find()
                ->orderBy(['comp_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemCompany($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListCompany()->asArray()->all(), 'id', 'comp_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListCompanyBranch()
    {
        $model = \common\models\TuCompBr::find()
                ->orderBy(['br_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemCompanyBranch($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListCompanyBranch()->asArray()->all(), 'id', 'br_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListDepartment()
    {
        $model = \common\models\TuDep::find()
                ->orderBy(['dep_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemDepartment($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListDepartment()->asArray()->all(), 'id', 'dep_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getListRole()
    {
        $model = \common\models\TuRole::find();

        return $model;
    }

    public function getItemRole($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListRole()->asArray()->all(), 'id', 'role_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getItemIsActive()
    {
        return ['Y'=>'ใช้งาน', 'N'=>'ไม่ใช้งาน'];
    }

    public function getIsActiveDesc($value)
    {
        return ($value=='Y')?'<span class="label label-success">ใช้งาน</span>':'<span class="label label-danger">ไม่ใช้งาน</span>';
    }

    public function getListCarType()
    {
        $model = TmCarType::find()
                ->orderBy(['seqno'=>SORT_ASC]);

        return $model;
    }

    public function getItemCarType($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListCarType()->asArray()->all(), 'id', 'type_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getItemClaimBillingProcess()
    {
        return ['B'=>'วางบิล', 'I'=>'แจ้งหนี้'];
    }

    public function getListPartshop()
    {
        $model = TuPartShop::find()
                ->where(['isactive'=>'Y'])
                ->orderBy(['shop_name'=>SORT_ASC]);

        return $model;
    }

    public function getItemPartshop($bolNoSelect=true)
    {
        $arrData = ArrayHelper::map($this->getListPartshop()->asArray()->all(), 'id', 'shop_name');
        if ($bolNoSelect) {
            $arrayData = [''=>'ระบุ'] + $arrData;
        } else {
            $arrayData = $arrData;
        }

        return $arrayData;
    }

    public function getOrderPartNo()
    {
        $intLastNo    = $this->getLateNumberTurun('PARTORDERNO', date('y-m'));
        $intRunNumber = 'PO-'.date('y').substr('00'.date('m'), -2).'-'.substr('0000'.$intLastNo, -4);
        return $intRunNumber;
    }

}