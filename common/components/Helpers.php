<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;

class Helpers extends Component
{

    public static function convCode($str, $conv = false) {
        if (!$conv) {
            return @iconv('tis-620', 'utf-8', trim($str));
        } else {
            return @iconv('utf-8', 'tis-620', trim($str));
        }
    }

    public static function thaiFormatDate($date) {
        $toTime           = strtotime($date);
        $thai_day_arr     = Helpers::listDay();
        $thai_month_arr   = Helpers::listMonth();
        $thai_date_return = "วัน".$thai_day_arr[intval(date("w",$toTime))];
        $thai_date_return .= "ที่ ".date("j",$toTime);
        $thai_date_return .= " เดือน".$thai_month_arr[intval(date("n",$toTime))];
        $thai_date_return .= " พ.ศ.".(date("Y",$toTime)+543);

        return $thai_date_return;
    }

	public static function excelFormatDate($dateString = '', $fromFormat = 'd/m/Y', $toFormat = 'Y-m-d\T00:00:00:000') {
        if($dateString == '') return '';

		$dt = date_create_from_format($fromFormat, $dateString);
		return date_format($dt, $toFormat);
    }

    public static function listDay($language = 'TH', $format = 'L', $placeholder = false)
    {
        $arrDay = array();
        if ($language == 'TH'){

            if ($format == 'L') {
                $arrDay = array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");;
            }

            if ($format == 'S') {
                    $arrDay = array("อา","จ","อ","พ","พฤ","ศ","ส");
            }
        }

        if ($placeholder) {
            $arrDay = ['00'=>'ระบุวัน']+$arrDay;
        }

        return $arrDay;
    }

    public static function listMonth($language = 'TH', $format = 'L', $placeholder = false)
    {
        $arrMonth = array();
        if ($language == 'TH'){

            if ($format == 'L') {
                $arrMonth = array('1' => 'มกราคม', '2' => 'กุมภาพันธ์', '3' => 'มีนาคม', '4' => 'เมษายน', '5' => 'พฤษภาคม', '6' => 'มิถุนายน', '7' => 'กรกฏาคม', '8' => 'สิงหาคม', '9' => 'กันยายน', '10' => 'ตุลาคม', '11' => 'พฤษจิกายน', '12' => 'ธันวาคม');
            }

            if ($format == 'S') {
                    $arrMonth = array('1' => 'ม.ค.', '2' => 'ก.พ.', '3' => 'มี.ค.', '4' => 'เม.ย.', '5'      => 'พ.ค.', '6' => 'มิ.ย.', '7' => 'ก.ค.', '8' => 'ส.ค.', '9' => 'ก.ย.', '10' => 'ต.ค.', '11' => 'พ.ย.', '12' => 'ธ.ค.');
            }
        }

        if ($placeholder) {
            $arrMonth = ['00'=>'ระบุเดือน']+$arrMonth;
        }

        return $arrMonth;
    }

    public static function listYear($intDecrease = 0, $intIncrease = 0, $placeholder = false)
    {
        $intYearStart = intval(date('Y'));
        $intYearEnd   = intval(date('Y')) + $intIncrease;

        if ($intDecrease > 0){
            $intYearStart = $intYearStart - $intDecrease;
        }

        $arrYear = array();
        for ($i=$intYearStart; $i <= $intYearEnd; $i++) {
            $arrYear += [$i=>$i];
        }

        if ($placeholder) {
            $arrYear = [0=>'ระบุปี']+$arrYear;
        }

        return $arrYear;
    }

    public static function getDateTHDMY($Date, $symbol) {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[0];
            $month   = $arrMonthTH[intval($subDate[1])];
            $year    = intval($subDate[2]) + 543;

            return $day . " " . $month . " " . $year;
        } else {
            return "-";
        }
    }

    public static function getFullDateTHDMY($date, $symbol) {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        if (!empty($date)) {

            $datetime = explode(' ', $date);
            $datesep = explode($symbol, $datetime[0]);

            $time = explode(".", $datetime[1]);

            $datereturn = $datesep[2] . " " . $arrMonthTH[intval($datesep[1])] . " " . intval($datesep[0] + 543) . " " . $time[0];
        } else {
            $datereturn = " - ";
        }
        return $datereturn;
    }

    public static function getDateYMDtoDMY($Date, $symbol) {
        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day = $subDate[2];
            $month = $subDate[1];
            $year = $subDate[0];
            $strDate = $day . "/" . $month . "/" . $year;

            return $strDate;
        } else {
            return "-";
        }
    }

    public static function getDateYMDtoDMYTH($Date, $symbol) {
        if (($Date != "") && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            $day     = $subDate[2];
            $month   = $subDate[1];
            $year    = $subDate[0];
            $strDate = $day . "/" . $month . "/" . $year;

            return Helpers::getDateTHDMY($strDate, "/");
        } else {
            return "-";
        }
    }



    public static function convMYSqlDateDMYtoYMD($Date, $symbol) {
        if (($Date !== '') && (strlen(trim($Date)) >= 8)) {
            $subDate = explode($symbol, $Date);
            if (count($subDate) == 3) {
                $day     = trim($subDate[0]);
                $month   = trim($subDate[1]);
                $year    = trim($subDate[2]);
                if (intval($year) > intval(date('Y')+500)) {
                    $year = intval($year)-543;
                }
                $strDate = $year . "-" . $month . "-" . $day;

                return $strDate;
            }
            return '';
        } else {
            return '';
        }
    }

    public static function getReplaceMemoText($Text) {
        $str = str_replace(" ", "&nbsp;", str_replace(chr(13), "<br>", $Text));
        return $str;
    }

    public static function con_date($datetime) {
        if (trim($datetime) == "") {
            return "";
        }
        $datetime = trim($datetime);
        $dt = explode(" ", $datetime);
        $date = explode("-", $dt[0]);
        $time = isset($dt[1]) ? $dt[1] : "";
        if (isset($time) && $time != "") {
            return $date[2] . "-" . $date[1] . "-" . $date[0] . " " . $time;
        } else {
            return $date[2] . "-" . $date[1] . "-" . $date[0];
        }
    }

    public static function thaiAmount($num) {
		$num = str_replace(',', '', $num);
		$th_num = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า');
		$th_pos = array('1' => 'หน่วย', '2' => 'สิบ', '3' => 'ร้อย', '4' => 'พัน', '5' => 'หมื่น', '0' => 'แสน', '7' => 'ล้าน');
		$txt = '';

		$arr_num = str_split($num);
		$int_len = count($arr_num);
		$decimal = false;
		$arr_decimal = array();

		if (in_array('.', $arr_num)) {
			$decimal_pos = array_search('.', $arr_num);
			$start_decimal_pos = $int_len - ($decimal_pos + 1);

			for ($i = ($decimal_pos + 1); $i < $int_len; $i++) {
				$arr_decimal[$start_decimal_pos] = $arr_num[$i];
				$start_decimal_pos--;
			}

			$int_len = $decimal_pos;
			$decimal = true;
		}

		foreach ($arr_num as $val) {
			$arr_int[$int_len] = $val;
			$int_len--;
			if ($int_len == 0)
				break;
		}

		foreach ($arr_int as $key => $val) {
			$num_pos = $key % 6;
			if ($val == 0) {
				if ($key > 6 && $num_pos == 1)
					$txt .= 'ล้าน';
				else
					continue;
			} else {
				if ($val == 1 AND $num_pos == 2) {
					$txt .= 'สิบ';
				} else if ($val == 2 AND $num_pos == 2) {
					$txt .= 'ยี่สิบ';
				} else if ($num_pos != 1) {
					$txt .= $th_num[$val] . $th_pos[$num_pos];
				} else {
					if (count($arr_int) > 1 && $val == 1 && $num_pos == 1 && isset($arr_int[$key+1]) && $arr_int[$key+1] > 0)
						$txt .= 'เอ็ด';
					else
						$txt .= $th_num[$val];
				}

				if ($key > 6 && $num_pos == 1) {
					$txt .= 'ล้าน';
				}
			}

			$prev_num = $val;
		}

		$txt .= 'บาท';

		$countDecimal = 0;
		foreach ($arr_decimal as $key => $val) {
			if ($val == 0) {
				continue;
			} else {
				if ($val == 1 AND $key == 2) {
					$txt .= 'สิบ';
				} else if ($val == 2 AND $key == 2) {
					$txt .= 'ยี่สิบ';
				} else if ($key > 1) {
					$txt .= $th_num[$val] . $th_pos[$key];
				} else {
					$txt .= $th_num[$val];
				}

				$countDecimal++;
			}
		}

		if ($countDecimal > 0) {
			$txt .= 'สตางค์';
		} else {
			$txt .= 'ถ้วน';
		}

		return $txt;
	}

    public function convert_datedb_to_string($numdate) {
        $arrMonthTH = Helpers::listMonth('TH', 'L', false);

        $dateresault = explode("-", substr($numdate, 0, -13));
        $timeresault = explode(" ", substr($numdate, 0, -4));
        $dateretern = $dateresault[2] . " " . $arrMonthTH[intval($dateresault[1])] . " " . ($dateresault[0] + 543) . " " . $timeresault[1];
        if (($dateresault[0] + 543) > 2500) {
            return $dateretern;
        } else {
            return "";
        }
    }

    public function getChecked($RowValue, $strValue) {
        if ($RowValue == $strValue) {
            return "checked";
        } else {
            return "";
        }
    }

    public function arrayquery_to_array($resultQuery)
    {
        $arrVal = array();
        foreach ($resultQuery as $valResult) {
            array_push($arrVal, implode(',', $valResult));
        }
        return $arrVal;
    }

    public function dataActiverecordtoDataProvider($dataActiverecord)
    {
        $arrColumn = array();

        foreach ($dataActiverecord as $valResult) {
                $arrColumn = array_keys($valResult);
        }

        $dataProvider = new ArrayDataProvider([
                        'allModels' => $dataActiverecord,
                        'sort' => [
                            'attributes' => $arrColumn,
                        ],
        ]);

        return $dataProvider;
    }

    public function DateDiff($strDate1,$strDate2)
    {
        return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
    }

    public function TimeDiff($strTime1,$strTime2)
    {
        return (strtotime($strTime2) - strtotime($strTime1)) / (60 * 60); // 1 Hour =  60*60
    }

    public function checkNumber($numberValue) {
        if (trim($numberValue) == '') {
            return 0;
        } else {
            return $numberValue;
        }
    }

    public function ReplaceCarLicense($licenseno)
    {
        return trim(str_replace(' ', '', str_replace('-', '', $licenseno)));
    }

    public function numberFormat($number,$decimal = NULL)
    {
        if(strstr($number, '.'))
        {
            $numbers = explode('.',$number);

            if($decimal && $numbers[1] <> 0)
                $number = number_format($numbers[0].'.'.$numbers[1],$decimal);
            else
                $number = number_format($numbers[0]);
        }
        else
        {
            if($decimal && $number <> 0)
                $number = number_format($number,$decimal);
            else
                $number = number_format($number);
        }

        return $number;
    }

    public function PhoneFormat($phone_number)
    {
        $phone_number = str_replace('-', '', $phone_number);
        if(strlen($phone_number > 8))
        {
            $number_1 = substr($phone_number, 0, 3);
            $number_2 = substr($phone_number, 3, 3);
            $number_3 = substr($phone_number, 6);
            $format_number = $number_1.'-'.$number_2.'-'.$number_3;
        }
        else {
            $format_number = $phone_number;
        }

        return $format_number;
    }

    public function SortNumber($array)
    {
        //Logic Qiuck Sort
        if (count($array) == 0)
            return array();

        $pivot = $array[0];
        $left = $right = array();

        for ($i = 1; $i < count($array); $i++) {
            if ($array[$i] < $pivot)
                $left[] = $array[$i];
            else
                $right[] = $array[$i];
        }

        return array_merge($this->quicksort($left), array($pivot), $this->quicksort($right));
    }

    public function getWeeksofMonth($date, $rollover='sunday')
    {
        $cut = substr($date, 0, 8);
        $daylen = 86400;

        $timestamp = strtotime($date);
        $first = strtotime($cut . "00");
        $elapsed = ($timestamp - $first) / $daylen;

        $weeks = 1;

        for ($i = 1; $i <= $elapsed; $i++)
        {
            $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
            $daytimestamp = strtotime($dayfind);

            $day = strtolower(date("l", $daytimestamp));

            if($day == strtolower($rollover))  $weeks ++;
        }

        return $weeks;
    }

}
