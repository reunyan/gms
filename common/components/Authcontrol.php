<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\data\ArrayDataProvider;

use common\models\Tumenu;
use common\models\Tumenusub;
use common\models\Tumenusubin;
use common\models\TuauthMainMenu;
use common\models\TuauthSubMenu;
use common\models\TuauthSubinMenu;

class Authcontrol extends Component
{
    public function UserIsAdmin()
    {
        return (Yii::$app->user->identity->role_id==1);
    }

	/*public function getMainMenu()
    {
        return Tumenu::find()
                ->where(['isactive'=>'Y'])
                ->orderBy(['menu_code'=>SORT_ASC]);
    }

	public function getSubMenu($intMainMenuID)
    {
        return Tumenusub::find()
                ->where(['tumenu_id'=>$intMainMenuID])
                ->andWhere(['isactive'=>'Y'])
                ->orderBy(['submenu_code'=>SORT_ASC]);
    }

	public function getSubInMenu($intSubMenuID)
    {
        return Tumenusubin::find()
                ->where(['tumenusub_id'=>$intSubMenuID])
                ->andWhere(['isactive'=>'Y'])
                ->orderBy(['subin_code'=>SORT_ASC]);
    }

    public function getListAuthMainMenu()
    {
        return TuauthMainMenu::find()
                ->select([
                    'department_id',
                    'position_id',
                    "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
                ])
                ->groupBy(['department_id','position_id']);
    }

    public function getManageAuthMainMenu($compTypeID, $depID, $posID)
    {
        return $this->getListAuthMainMenu()
                ->where(['comp_type_id' => $compTypeID])
                ->andWhere(['department_id' => $depID])
                ->andWhere(['position_id' => $posID])
                ->one();
    }

    public function getListAuthSubMenu()
    {
        return TuauthSubMenu::find()
                ->select([
                    'department_id',
                    'position_id',
                    "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
                ])
                ->groupBy(['department_id','position_id']);
    }

    public function getManageAuthSubMenu($compTypeID, $depID, $posID)
    {
        return $this->getListAuthSubMenu()
                ->where(['comp_type_id' => $compTypeID])
                ->andWhere(['department_id' => $depID])
                ->andWhere(['position_id' => $posID])
                ->one();
    }

    public function getListAuthSubinMenu()
    {
        return TuauthSubinMenu::find()
                ->select([
                    'department_id',
                    'position_id',
                    "GROUP_CONCAT(menu_id SEPARATOR ', ') AS listmenu"
                ])
                ->groupBy(['department_id','position_id']);
    }

    public function getManageAuthSubinMenu($compTypeID, $depID, $posID)
    {
        return $this->getListAuthSubinMenu()
                ->where(['comp_type_id' => $compTypeID])
                ->andWhere(['department_id' => $depID])
                ->andWhere(['position_id' => $posID])
                ->one();
    }

    public function getListAuthUserMainMenu()
    {
        return Tumenu::find()
                ->rightJoin('tuauth_mainmenu', 'tuauth_mainmenu.menu_id = tumenu.id')
                ->where(['tuauth_mainmenu.comp_type_id' => Yii::$app->user->identity->company_type])
                ->andWhere(['tuauth_mainmenu.department_id' => Yii::$app->user->identity->department_id])
                ->andWhere(['tuauth_mainmenu.position_id' => Yii::$app->user->identity->position_id])
                ->andWhere(['isactive'=>'Y'])
                ->orderBy(['menu_code'=>SORT_ASC])
                ->all();
    }

    public function getListAuthUserSubMenu($intMainMenuID)
    {
        return Tumenusub::find()
                ->rightJoin('tuauth_submenu', 'tuauth_submenu.menu_id = tumenusub.id')
                ->where(['tumenu_id'=>$intMainMenuID])
                ->andWhere(['tuauth_submenu.comp_type_id' => Yii::$app->user->identity->company_type])
                ->andWhere(['tuauth_submenu.department_id' => Yii::$app->user->identity->department_id])
                ->andWhere(['tuauth_submenu.position_id' => Yii::$app->user->identity->position_id])
                ->andWhere(['isactive'=>'Y'])
                ->orderBy(['submenu_code'=>SORT_ASC])
                ->all();
    }

    public function getListAuthUserSubinMenu($intSubMenuID)
    {
        return Tumenusubin::find()
                ->rightJoin('tuauth_subinmenu', 'tuauth_subinmenu.menu_id = tumenusubin.id')
                ->where(['tumenusub_id'=>$intSubMenuID])
                ->andWhere(['tuauth_subinmenu.comp_type_id' => Yii::$app->user->identity->company_type])
                ->andWhere(['tuauth_subinmenu.department_id' => Yii::$app->user->identity->department_id])
                ->andWhere(['tuauth_subinmenu.position_id' => Yii::$app->user->identity->position_id])
                ->andWhere(['isactive'=>'Y'])
                ->orderBy(['subin_code'=>SORT_ASC])
                ->all();
    }*/

}

?>