<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tm_model".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property string $model_name
 * @property string $isactive
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class TmModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tm_model';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['model_name'], 'string', 'max' => 150],
            [['isactive'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'brand_id'   => 'ยี่ห้อรถ',
            'model_name' => 'รุ่นรถ',
            'isactive'   => Yii::t('app','Isactive'),
            'created_at' => Yii::t('app','Created At'),
            'created_by' => Yii::t('app','Created By'),
            'updated_at' => Yii::t('app','Updated At'),
            'updated_by' => Yii::t('app','Updated By'),
        ];
    }

    public function getTmBrand()
    {
        return $this->hasOne(TmBrand::className(), ['id' => 'brand_id']);
    }
}
