<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

use common\models\JmRepairClaim;

/**
 * JmRepairClaimSearch represents the model behind the search form about `common\models\JmRepairClaim`.
 */
class JmRepairClaimSearch extends JmRepairClaim
{
    public $dateBillingStart;
    public $dateBillingEnd;
    public $dateReceiveStart;
    public $dateReceiveEnd;
    public $dateClaimCancelStart;
    public $dateClaimCancelEnd;
    public $car_license_no;
    public $job_no;
    public $insurance_id;
    public $in_repair_date_start;
    public $in_repair_date_end;
    public $in_repair_emp;
    public $process_date;
    public $dateBeforeBillingStart;
    public $dateBeforeBillingEnd;
    public $receive_at_start;
    public $receive_at_end;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jm_repair_id', 'created_by', 'updated_by', 'billing_status', 'in_repair_emp'], 'integer'],
            [['policy_no', 'notify_no', 'claim_no', 'created_at', 'updated_at', 'billing_date', 'invoice_no',
             'dateBillingStart', 'dateBillingEnd', 'dateReceiveStart', 'dateReceiveEnd', 'car_license_no', 'job_no', 
             'insurance_id', 'in_repair_date_start', 'in_repair_date_end', 'first_claim_amt', 'before_billing_date', 
             'process_date', 'select_process_status','dateBeforeBillingStart','dateBeforeBillingEnd',
            'dateClaimCancelStart','dateClaimCancelEnd','receive_at_start','receive_at_end'], 'safe'],
            [['repair_amt', 'part_amt'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JmRepairClaim::find()->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')
                                    ->andWhere(['jm_repair_claim.isactive'=>'Y'])
                                    ->andWhere(['jm_repair.payment_type'=>2])
                                    ->andWhere(['IS NOT', 'billing_status', null]);

        // add conditions that should always apply here
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'              => $this->id,
            'jm_repair_id'    => $this->jm_repair_id,
            'first_claim_amt' => $this->first_claim_amt,
            'repair_amt'      => $this->repair_amt,
            'part_amt'        => $this->part_amt,
            'created_at'      => $this->created_at,
            'created_by'      => $this->created_by,
            'updated_at'      => $this->updated_at,
            'updated_by'      => $this->updated_by,
            'billing_status'  => $this->billing_status,
            'before_billing_date'    => $this->before_billing_date,
            'billing_date'    => $this->billing_date,
            'receive_date'    => $this->receive_date,
            'insurance_id'    => $this->insurance_id,
        ]);

        $query->andFilterWhere(['like', 'policy_no', $this->policy_no])
            ->andFilterWhere(['like', 'notify_no', $this->notify_no])
            ->andFilterWhere(['like', 'invoice_no', $this->invoice_no])
            ->andFilterWhere(['like', 'claim_no', $this->claim_no])
            ->andFilterWhere(['like', 'jm_repair.car_license_no', $this->car_license_no])
            ->andFilterWhere(['like', 'jm_repair.job_no', $this->job_no])
            ->andFilterWhere(['jm_repair.in_repair_emp' => $this->in_repair_emp]);
        
        if (!empty($this->dateBeforeBillingStart) && !empty($this->dateBeforeBillingEnd)) {
            $query->andFilterWhere(['between', 'before_billing_date', date('Y-m-d', strtotime($this->dateBeforeBillingStart)),  date('Y-m-d', strtotime($this->dateBeforeBillingEnd))]);
        }
        if (!empty($this->dateBillingStart) && !empty($this->dateBillingEnd)) {
            $query->andFilterWhere(['between', 'billing_date', date('Y-m-d', strtotime($this->dateBillingStart)),  date('Y-m-d', strtotime($this->dateBillingEnd))]);
        }
        if (!empty($this->dateReceiveStart) && !empty($this->dateReceiveEnd)) {
            $query->andFilterWhere(['between', 'receive_date', date('Y-m-d', strtotime($this->dateReceiveStart)),  date('Y-m-d', strtotime($this->dateReceiveEnd))]);
        }
        if (!empty($this->in_repair_date_start) && !empty($this->in_repair_date_end)) {
            $query->andFilterWhere(['between', 'in_repair_date', date('Y-m-d', strtotime($this->in_repair_date_start)),  date('Y-m-d', strtotime($this->in_repair_date_end))]);
        }
        if (!empty($this->receive_at_start) && !empty($this->receive_at_end)) {
            $query->andFilterWhere(['between', 'DATE(receive_at)', date('Y-m-d', strtotime($this->receive_at_start)),  date('Y-m-d', strtotime($this->receive_at_end))]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function searchBilling($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = JmRepairClaim::find()
                    ->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')
                    ->andWhere(['jm_repair_claim.isactive'=>'Y'])
                    ->andWhere(['jm_repair.payment_type'=>2])
                    ->andWhere(['IS NOT', 'billing_status', null]);
        // add conditions that should always apply here
        // $query->where(['status_repair'=>3]);

         // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jm_repair_id'   => $this->jm_repair_id,
            'repair_amt'     => $this->repair_amt,
            'part_amt'       => $this->part_amt,
            'created_at'     => $this->created_at,
            'created_by'     => $this->created_by,
            'updated_at'     => $this->updated_at,
            'updated_by'     => $this->updated_by,
            'billing_status' => $this->billing_status,
            'billing_date'   => $this->billing_date,
            'before_billing_date'   => $this->before_billing_date,
            'insurance_id'   => $this->insurance_id,
        ]);

        $query->andFilterWhere(['like', 'policy_no', trim($this->policy_no)])
            ->andFilterWhere(['like', 'notify_no', trim($this->notify_no)])
            ->andFilterWhere(['like', 'invoice_no', trim($this->invoice_no)])
            ->andFilterWhere(['like', 'claim_no', trim($this->claim_no)])
            ->andFilterWhere(['like', 'jm_repair.car_license_no', trim($this->car_license_no)])
            ->andFilterWhere(['like', 'jm_repair.job_no', trim($this->job_no)]);

        if (!empty($this->dateBeforeBillingStart) && !empty($this->dateBeforeBillingEnd)) {
            $query->andFilterWhere(['between', 'before_billing_date', date('Y-m-d', strtotime($this->dateBeforeBillingStart)),  date('Y-m-d', strtotime($this->dateBeforeBillingEnd))]);
        }
        if (!empty($this->dateBillingStart) && !empty($this->dateBillingEnd)) {
            $query->andFilterWhere(['between', 'billing_date', date('Y-m-d', strtotime($this->dateBillingStart)),  date('Y-m-d', strtotime($this->dateBillingEnd))]);
        }
        // echo '<pre>';print_r($query->createCommand()->getRawSql());echo '</pre>';exit;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function numStatusWait()
    {
        // return JmRepairClaim::find()->andWhere(['billing_status'=>1])->count();
        return JmRepairClaim::find()->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')->andWhere(['jm_repair_claim.isactive'=>'Y'])->andWhere(['jm_repair.payment_type'=>2])->andWhere(['billing_status'=>1])->count();
    }

    public function numStatusOnInv()
    {
        // return JmRepairClaim::find()->andWhere(['billing_status'=>4])->count();
        return JmRepairClaim::find()->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')->andWhere(['jm_repair_claim.isactive'=>'Y'])->andWhere(['jm_repair.payment_type'=>2])->andWhere(['billing_status'=>4])->count();
    }
    
    public function numStatusOn()
    {
        // return JmRepairClaim::find()->andWhere(['billing_status'=>2])->count();
        return JmRepairClaim::find()->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')->andWhere(['jm_repair_claim.isactive'=>'Y'])->andWhere(['jm_repair.payment_type'=>2])->andWhere(['billing_status'=>2])->count();
    }

    public function numStatusComplete()
    {
        // return JmRepairClaim::find()->andWhere(['billing_status'=>3])->count();
        return JmRepairClaim::find()->innerJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id')->andWhere(['jm_repair_claim.isactive'=>'Y'])->andWhere(['jm_repair.payment_type'=>2])->andWhere(['billing_status'=>3])->count();
    }

    public function ClaimByInsurance()
    {
        $query = JmRepairClaim::find();
        $query->select(['billing_date', 'insurance_id', 'count(jm_repair_claim.id) as count_claim', 'insure_name as insurance', 'SUM(repair_amt) as sum_repair_amt', 'SUM(part_amt) as sum_part_amt']);
        $query->leftJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id');
        $query->leftJoin('tu_insure', 'tu_insure.id = jm_repair.insurance_id');
        $query->andWhere(['jm_repair_claim.isactive'=>'Y']);
        $query->andWhere(['billing_status'=>2]);

        if (!empty($this->dateBillingStart) && !empty($this->dateBillingEnd)) {
            $query->andFilterWhere(['between', 'billing_date', date('Y-m-d', strtotime($this->dateBillingStart)),  date('Y-m-d', strtotime($this->dateBillingEnd))]);
        }
        $query->andFilterWhere(['insurance_id'=>$this->insurance_id]);
        $query->orderBy(['billing_date'=>SORT_ASC, 'insurance_id'=>SORT_ASC]);
        $query->groupBy(['billing_date', 'insurance_id']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    public function ClaimByInsuranceDetail()
    {
        $query = new Query();
        // compose the query
        $query->select(['billing_date', 'insurance_id', 'insure_name as insurance', 'repair_amt', 'part_amt', 'policy_no', 'car_claim_type', 'car_license_no', 'invoice_no', 'job_no', 'claim_no', 'policy_no', 'jm_repair_claim.first_claim_amt']);
        $query->from('jm_repair_claim');
        $query->leftJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id');
        $query->leftJoin('tu_insure', 'tu_insure.id = jm_repair.insurance_id');
        $query->andWhere(['jm_repair_claim.isactive'=>'Y']);
        $query->andWhere(['billing_status'=>2]);
        $query->andFilterWhere(['billing_date' => $this->billing_date]);
        $query->andFilterWhere(['insurance_id' => $this->insurance_id]);
        $query->orderBy(['invoice_no'=>SORT_ASC]);

        return $query->all();
    }

    public function SearchClaimCancel()
    {
        $query = new Query();
        $query->select(['policy_no', 'claim_no', 'jm_repair_claim.first_claim_amt', 'repair_amt', 'part_amt'
                        , 'jm_repair_claim.updated_at', 'jm_repair_claim.updated_by'
                        ,'jm_repair.job_no', 'jm_repair.car_license_no'
                        ,'tu_user.firstname','tu_user.nickname'
                    ]);
        $query->from('jm_repair_claim');
        $query->leftJoin('jm_repair', 'jm_repair.id = jm_repair_claim.jm_repair_id');
        $query->leftJoin('tu_user', 'tu_user.id = jm_repair_claim.updated_by');
        $query->andWhere(['jm_repair_claim.isactive'=>'N']);

        if (!empty($this->dateClaimCancelStart) && !empty($this->dateClaimCancelEnd)) {
            $query->andFilterWhere(['between', 'jm_repair_claim.updated_at', date('Y-m-d', strtotime($this->dateClaimCancelStart)),  date('Y-m-d', strtotime($this->dateClaimCancelEnd))]);
        }
        $query->andFilterWhere(['LIKE', 'jm_repair.job_no', $this->job_no]);
        $query->andFilterWhere(['LIKE', 'policy_no', $this->policy_no]);
        $query->andFilterWhere(['LIKE', 'claim_no', $this->claim_no]);
        $query->andFilterWhere(['LIKE', 'jm_repair.car_license_no', $this->car_license_no]);
        $query->orderBy(['jm_repair_claim.updated_at'=>SORT_ASC]);
        // echo '<pre>';print_r($query->createCommand()->getRawSql());echo '</pre>';exit;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
}
