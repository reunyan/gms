<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tu_comp_br".
 *
 * @property integer $id
 * @property integer $tu_comp_id
 * @property string $br_name
 * @property string $br_short_name
 * @property string $br_name_en
 * @property string $br_short_name_en
 * @property string $br_addr
 * @property string $br_telno
 * @property string $br_mobileno
 * @property string $br_faxno
 * @property string $br_email
 * @property string $br_tax_id
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class TuCompBr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tu_comp_br';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tu_comp_id'], 'required'],
            [['tu_comp_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['br_name', 'br_name_en', 'br_email'], 'string', 'max' => 150],
            [['br_short_name', 'br_short_name_en'], 'string', 'max' => 100],
            [['br_addr'], 'string', 'max' => 255],
            [['br_telno', 'br_mobileno', 'br_faxno', 'br_tax_id'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'tu_comp_id'       => 'Tu Comp ID',
            'br_name'          => 'Br Name',
            'br_short_name'    => 'Br Short Name',
            'br_name_en'       => 'Br Name En',
            'br_short_name_en' => 'Br Short Name En',
            'br_addr'          => 'Br Addr',
            'br_telno'         => 'Br Telno',
            'br_mobileno'      => 'Br Mobileno',
            'br_faxno'         => 'Br Faxno',
            'br_email'         => 'Br Email',
            'br_tax_id'        => 'Br Tax ID',
            'created_at'       => Yii::t('app','Created At'),
            'created_by'       => Yii::t('app','Created By'),
            'updated_at'       => Yii::t('app','Updated At'),
            'updated_by'       => Yii::t('app','Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(TuComp::className(), ['id' => 'tu_comp_id']);
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->attribute       = $this->attribute;
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */
}
