<?php

namespace common\models;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "jm_repair_part_order".
 *
 * @property int $id
 * @property int $jm_repair_id
 * @property string $order_part_no
 * @property int $order_part_seqno
 * @property string $order_part_date
 * @property int $order_part_by
 * @property int $tu_part_shop_id
 * @property string $isactive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class JmRepairPartOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jm_repair_part_order';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jm_repair_id'], 'required'],
            [['jm_repair_id', 'order_part_seqno', 'order_part_by', 'tu_part_shop_id', 'created_by', 'updated_by'], 'integer'],
            [['order_part_date', 'created_at', 'updated_at'], 'safe'],
            [['order_part_no'], 'string', 'max' => 20],
            [['isactive'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jm_repair_id' => Yii::t('app', 'Jm Repair ID'),
            'order_part_no' => Yii::t('app', 'Order Part No'),
            'order_part_seqno' => Yii::t('app', 'Order Part Seqno'),
            'order_part_date' => Yii::t('app', 'Order Part Date'),
            'order_part_by' => Yii::t('app', 'Order Part By'),
            'tu_part_shop_id' => Yii::t('app', 'Tu Part Shop ID'),
            'isactive' => Yii::t('app', 'Isactive'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function getPartShop()
    {
        return $this->hasOne(TuPartShop::className(), ['id' => 'tu_part_shop_id']);
    }
    
    public function getUser()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'order_part_by']);
    }
    
    public function getPartorderItem()
    {
        return $this->hasMany(JmRepairPartOrderItem::className(), ['jm_repair_part_order_id' => 'id']);
    }
}
