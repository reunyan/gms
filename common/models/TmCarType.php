<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tm_car_type".
 *
 * @property int $id
 * @property string $type_name
 * @property int $seqno
 * @property string $isactive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class TmCarType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tm_car_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seqno', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type_name'], 'string', 'max' => 150],
            [['isactive'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_name' => Yii::t('app', 'Type Name'),
            'seqno' => Yii::t('app', 'Seqno'),
            'isactive' => Yii::t('app', 'Isactive'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
