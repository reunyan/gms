<?php

namespace common\models;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "jm_repair_part_order_item".
 *
 * @property int $id
 * @property int $jm_repair_part_order_id
 * @property int $jm_repair_part_id
 * @property string $isactive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class JmRepairPartOrderItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jm_repair_part_order_item';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jm_repair_part_order_id', 'jm_repair_part_id'], 'required'],
            [['jm_repair_part_order_id', 'jm_repair_part_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['isactive'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jm_repair_part_order_id' => Yii::t('app', 'Jm Repair Part Order ID'),
            'jm_repair_part_id' => Yii::t('app', 'Jm Repair Part ID'),
            'isactive' => Yii::t('app', 'Isactive'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
    
    public function getRepairPartOrder()
    {
        return $this->hasOne(JmRepairPartOrder::className(), ['id' => 'jm_repair_part_order_id']);
    }

    public function getRepairPart()
    {
        return $this->hasOne(JmRepairPart::className(), ['id' => 'jm_repair_part_id']);
    }
}
