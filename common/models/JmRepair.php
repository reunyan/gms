<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jm_repair".
 *
 * @property integer $id
 * @property string $job_no
 * @property string $car_license_no
 * @property string $car_license_province
 * @property integer $car_brand_id
 * @property integer $car_model_id
 * @property integer $car_series_id
 * @property integer $car_color_id
 * @property integer $car_type_id
 * @property integer $insurance_id
 * @property integer $insurance_type
 * @property integer $car_claim_type
 * @property string $contact_repair_date
 * @property string $contact_repair_firstname
 * @property string $contact_repair_lastname
 * @property string $contact_repair_tel
 * @property string $appoint_repair_date
 * @property integer $in_repair_emp
 * @property string $in_repair_date
 * @property string $in_repair_firstname
 * @property string $in_repair_lastname
 * @property string $in_repair_tel
 * @property integer $in_repair_type
 * @property string $appoint_sent_date
 * @property integer $sent_car_emp
 * @property string $sent_car_date
 * @property string $sent_car_firstname
 * @property string $sent_car_lastname
 * @property string $sent_car_tel
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * * @property integer $first_claim_amt
* @property integer $other_amt
 *
 * @property TuUser $createdBy
 * @property TuUser $updatedBy
 * @property JmRepairClaim[] $jmRepairClaims
 */
class JmRepair extends \yii\db\ActiveRecord
{
    public $CarDetail;
    public $status_repair_name;
    public $labor_amt;
    public $part_amt;
    public $contact_repair_fullname;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jm_repair';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_license_no'], 'required'],
            [['car_license_province', 'car_brand_id', 'car_model_id', 'car_series_id', 'car_color_id', 'payment_type', 'insurance_id', 'insurance_type', 'car_claim_type', 'in_repair_emp', 'in_repair_type', 'sent_car_emp', 'first_claim_amt', 'other_amt', 'created_by', 'updated_by', 'status_repair', 'status_invoice', 'num_repair', 'receive_money_by', 'car_type_id'], 'integer'],
            [['contact_repair_date', 'appoint_repair_date', 'in_repair_date', 'appoint_sent_date', 'sent_car_date', 'created_at', 'updated_at', 'receive_money_at'], 'safe'],
            [['other_amt_desc'], 'string'],
            [['job_no'], 'string', 'max' => 20],
            [['car_license_no'], 'string', 'max' => 10],
            [['contact_repair_firstname', 'contact_repair_lastname', 'contact_repair_tel', 'in_repair_firstname', 'in_repair_lastname', 'in_repair_tel', 'sent_car_firstname', 'sent_car_lastname', 'sent_car_tel'], 'string', 'max' => 50],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => TuUser::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => TuUser::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['receive_money_by'], 'exist', 'skipOnError' => true, 'targetClass' => TuUser::className(), 'targetAttribute' => ['receive_money_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                       => 'ID',
            'job_no'                   => 'Job No',
            'car_license_no'           => 'เลขที่ทะเบียนรถ',
            'car_license_province'     => 'จังหวัดทะเบียนรถ',
            'car_brand_id'             => 'ยี่ห้อรถ',
            'car_model_id'             => 'รุ่นรถ',
            'car_series_id'            => 'แบบรถ',
            'car_color_id'             => 'สีรถ',
            'car_type_id'              => 'ประเภทรถ',
            'payment_type'             => 'การชำระเงิน',
            'insurance_id'             => 'บริษัทประกันภัย',
            'insurance_type'           => 'ประเภทบริษัทประกัน',
            'car_claim_type'           => 'ประเภทรถซ่อม',
            'contact_repair_date'      => 'วันที่ติดต่อซ่อม',
            'contact_repair_firstname' => 'ชื่อผู้ติดต่อ',
            'contact_repair_lastname'  => 'สกุลผู้ติดต่อ',
            'contact_repair_tel'       => 'เบอร์ติดต่อ',
            'appoint_repair_date'      => 'วันที่นัดเข้าซ่อม',
            'in_repair_emp'            => 'พนักงานรับรถ',
            'in_repair_date'           => 'วันที่จอดซ่อม',
            'in_repair_firstname'      => 'ชื่อผู้นำรถเข้าซ่อม',
            'in_repair_lastname'       => 'นามสกุลผู้นำรถเข้าซ่อม',
            'in_repair_tel'            => 'เบอร์ติดต่อ',
            'in_repair_type'           => 'การจอดซ่อม',
            'appoint_sent_date'        => 'วันที่นัดรับรถซ่อมเสร็จ',
            'sent_car_emp'             => 'พนักงานส่งรถ',
            'sent_car_date'            => 'วันที่ส่งรถ',
            'sent_car_firstname'       => 'ชื่อผู้รับรถ',
            'sent_car_lastname'        => 'นามสกุลผู้รับรถ',
            'sent_car_tel'             => 'เบอร์ติดต่อ',
            'first_claim_amt'          => 'ค่า Ex.',
            'other_amt'                => 'เพิ่มเติม',
            'other_amt_desc'           => 'รายละเอียดเพิ่มเติม',
            'status_repair'            => 'สถานะซ่อม',
            'status_repair_name'       => 'สถานะซ่อม',
            'status_invoice'           => 'สถานะวางบิล',
            'created_at'               => Yii::t('app','Created At'),
            'created_by'               => Yii::t('app','Created By'),
            'updated_at'               => Yii::t('app','Updated At'),
            'updated_by'               => Yii::t('app','Updated By'),
            'CarDetail'                => 'ข้อมูลรถ',
            'num_repair'               => 'จำนวนชิ้นซ่อม',
            'receive_money_at'         => 'รับเงินเมื่อ',
            'receive_money_by'         => 'รับเงินโดย',
            'labor_amt'                => 'ค่าแรง',
            'part_amt'                 => 'ค่าอะไหล่',
            'contact_repair_fullname'  => 'ชื่อผู้ติดต่อ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarProvince()
    {
        return $this->hasOne(TuProvince::className(), ['id' => 'car_license_province']);
    }

    public function getCreatedBy()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'created_by']);
    }

    public function getReceiveMoneyBy()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'receive_money_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'updated_by']);
    }

    public function getCarBrand()
    {
        return $this->hasOne(TmBrand::className(), ['id' => 'car_brand_id']);
    }

    public function getCarModel()
    {
        return $this->hasOne(TmModel::className(), ['id' => 'car_model_id']);
    }

    public function getCarColor()
    {
        return $this->hasOne(TmColor::className(), ['id' => 'car_color_id']);
    }

    public function getInsurance()
    {
        return $this->hasOne(TuInsure::className(), ['id' => 'insurance_id']);
    }

    public function getRepairDetail()
    {
        return $this->hasMany(JmRepairDetail::className(), ['jm_repair_id' => 'id']);
    }

    public function getRepairPart()
    {
        return $this->hasMany(JmRepairPart::className(), ['jm_repair_id' => 'id']);
    }

    public function getJmRepairClaims()
    {
        return $this->hasMany(JmRepairClaim::className(), ['jm_repair_id' => 'id']);
    }

    public function getReciveCarEmp()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'in_repair_emp']);
    }

    public function getCarType()
    {
        return $this->hasOne(TmCarType::className(), ['id' => 'car_type_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->job_no == '') {
                if (!in_array($this->status_repair, [1,5,6])) {
                    $this->job_no = Yii::$app->Utilities->getJobNumber();
                }
            }

            $this->contact_repair_date = (!empty($this->contact_repair_date))?date('Y-m-d',strtotime($this->contact_repair_date)):null;
            $this->appoint_repair_date = (!empty($this->appoint_repair_date))?date('Y-m-d',strtotime($this->appoint_repair_date)):null;
            $this->in_repair_date      = (!empty($this->in_repair_date))?date('Y-m-d',strtotime($this->in_repair_date)):null;
            $this->appoint_sent_date   = (!empty($this->appoint_sent_date))?date('Y-m-d',strtotime($this->appoint_sent_date)):null;
            $this->sent_car_date       = (!empty($this->sent_car_date))?date('Y-m-d',strtotime($this->sent_car_date)):null;
            $this->first_claim_amt     = (!empty($this->first_claim_amt))?str_replace(',', '', $this->first_claim_amt):null;  

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->contact_repair_date     = (!empty($this->contact_repair_date))?date('d-m-Y',strtotime($this->contact_repair_date)):null;
        $this->appoint_repair_date     = (!empty($this->appoint_repair_date))?date('d-m-Y',strtotime($this->appoint_repair_date)):null;
        $this->in_repair_date          = (!empty($this->in_repair_date))?date('d-m-Y',strtotime($this->in_repair_date)):null;
        $this->appoint_sent_date       = (!empty($this->appoint_sent_date))?date('d-m-Y',strtotime($this->appoint_sent_date)):null;
        $this->sent_car_date           = (!empty($this->sent_car_date))?date('d-m-Y',strtotime($this->sent_car_date)):null;

        $this->CarDetail               = (!empty($this->carBrand))?$this->carBrand->brand_name:'';
        $this->CarDetail               = (!empty($this->carModel))?$this->CarDetail.' '.$this->carModel->model_name:$this->CarDetail;
        $this->CarDetail               = (!empty($this->carColor))?$this->CarDetail.' '.$this->carColor->color_name:$this->CarDetail;
        $this->CarDetail               = (!empty($this->carType))?$this->CarDetail.' '.$this->carType->type_name:$this->CarDetail;

        $this->status_repair_name      = (!empty($this->status_repair))?Yii::$app->Utilities->getDescReferance('REPAIRCAR', 'STATUS', $this->status_repair):'';
        $this->contact_repair_fullname = $this->contact_repair_firstname.' '.$this->contact_repair_lastname;

         $intSumLabor  = 0;
        foreach ($this->jmRepairClaims as $valLabor) {
            $intSumLabor = $intSumLabor + $valLabor->repair_amt;
        }
        $this->labor_amt =  $intSumLabor;

        $intSumPart = 0;
        foreach ($this->jmRepairClaims as $valPart) {
            $intSumPart = $intSumPart + $valPart->part_amt;
        }
        $this->part_amt =  $intSumPart;

        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->status_repair == 4) {
            $modelClaim = JmRepairClaim::find()->where(['jm_repair_id'=>$this->id])->andWhere(['IS', 'billing_status', null])->all();
            if (!empty($modelClaim)) {
                JmRepairClaim::updateAll(['billing_status'=>1], 'jm_repair_id = '.$this->id.' AND billing_status IS NULL');
            }
        }
    }
}