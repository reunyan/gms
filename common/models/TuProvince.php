<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tu_province".
 *
 * @property integer $id
 * @property string $province_name
 * @property string $provice_short
 * @property string $part
 */
class TuProvince extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tu_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provice_short', 'part'], 'required'],
            [['province_name'], 'string', 'max' => 100],
            [['provice_short'], 'string', 'max' => 3],
            [['part'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'province_name' => 'Province Name',
            'provice_short' => 'Provice Short',
            'part' => 'Part',
        ];
    }
}
