<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use kartik\mpdf\Pdf;

use common\models\JmRepair;
use common\models\JmRepairSearch;
use common\models\JmRepairDetail;
use common\models\JmRepairPart;
use common\models\JmRepairClaim;

/**
 * Site controller
 */
class RepaircarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'create', 'update', 'view', 'add-repair-detail', 'delete-repair-detail', 'add-repair-part', 'delete-repair-part', 'view-recive-repair', 'print-recive-repair', 'add-repair-claim', 'delete-repair-claim', 'list-order-job', 'calendar-repair', 'add-repair-appoint', 'report-repair-car', 'view-repair-appoint', 'sent-repair-car', 'save-receive-money'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel  = new JmRepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_repair' => [2,3]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSentRepairCar()
    {
        $searchModel  = new JmRepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['status_repair' => [4]]);

        return $this->render('sent-repair-car', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReportRepairCar()
    {
        $searchModel = new JmRepairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['<>','status_repair', 1]);

        return $this->render('report-repair-car', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JmRepair model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        $dataPost = Yii::$app->request->post();
        return $this->renderAjax('view', [
            'model' => $this->findModel($dataPost['id']),
        ]);
    }

    public function actionCalendarRepair()
    {
        $events      = array();
        $model       = new JmRepair();
        $searchModel = new JmRepairSearch();
        $modelRepair = $searchModel->queryCarInRepair();
        // echo '<pre>';print_r($modelRepair);echo '</pre>';exit;

        foreach ($modelRepair as $valData) {
            $Event        = new \yii2fullcalendar\models\Event();
            $Event->id    = $valData->id;
            $Event->title = $valData->car_license_no.' จำนวน '.intval($valData->num_repair).' ชิ้น';
            $Event->start = date('Y-m-d',strtotime($valData->appoint_repair_date));
            $events[]     = $Event;
        }

        $modelSend = $searchModel->queryCarAppointSend();
        foreach ($modelSend as $valSend) {
            $Event        = new \yii2fullcalendar\models\Event();
            $Event->color = '#ff1a1a';
            $Event->id    = $valSend->id;
            $Event->title = $valSend->car_license_no.' นัดส่ง';
            $Event->start = date('Y-m-d',strtotime($valSend->appoint_sent_date));
            $events[]     = $Event;
        }

        return $this->render('calendar-repair', [
            'model'=>$model,
            'events'=>$events,
        ]);
    }

    public function actionAddRepairAppoint()
    {
        $dataPost = Yii::$app->request->post();
        if (!empty($dataPost)) {
            $model                           = new JmRepair();
            $model->appoint_repair_date      = $dataPost['appoint_date'];
            $model->car_license_no           = $dataPost['appoint_license_no'];
            $model->contact_repair_firstname = $dataPost['appoint_first_name'];
            $model->contact_repair_lastname  = $dataPost['appoint_last_name'];
            $model->contact_repair_tel       = $dataPost['appoint_telno'];
            $model->insurance_id             = $dataPost['repair_insure'];
            $model->num_repair               = $dataPost['num_repair'];
            $model->status_repair            = '1';
            if ($model->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function actionViewRepairAppoint()
    {
        $dataPost = Yii::$app->request->post();
        $model    = JmRepairClaim::find()->where(['id'=>$dataPost['id']])->one();
        return $this->renderAjax('view-repair-appoint', [
            'model' => $model,
        ]);
    }

    public function actionViewReciveRepair($id)
    {
        return $this->render('view-recive-repair', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPrintReciveRepair($id)
    {
        $model        =  $this->findModel($id);
        $content      = $this->renderPartial('_print-recive-repair', ['model' => $model]);
        $strTitleName = 'ใบรับรถเข้าซ่อม';
        $strFileName  = 'receipt_car_'.$model->job_no;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' =>Pdf::FORMAT_A4,
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-2{font-size:22px}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>false,
            ],
            'filename' => $strFileName.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
        // return $this->renderPartial('_pdf-print-request-receipt', ['model' => $model]);
    }

    /**
     * Creates a new JmRepair model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JmRepair();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view-recive-repair', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing JmRepair model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model       = $this->findModel($id);
        $modelDetail = new JmRepairDetail();
        $modelPart   = new JmRepairPart();
        $modelClaim  = new JmRepairClaim();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            $listRepairDetail = $this->findListRepairDetail($id);
            $listRepairPart   = $this->findListRepairPart($id);
            $listRepairClaim  = $this->findListRepairClaim($id);
            return $this->render('update', [
                'model'            => $model,
                'modelDetail'      => $modelDetail,
                'listRepairDetail' => $listRepairDetail,
                'modelPart'        => $modelPart,
                'listRepairPart'   => $listRepairPart,
                'modelClaim'       => $modelClaim,
                'listRepairClaim'  => $listRepairClaim,
            ]);
        }
    }

    /**
     * Deletes an existing JmRepair model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JmRepair model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JmRepair the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairDetail($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairDetail::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return new JmRepairDetail();
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairPart($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairPart::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findListRepairClaim($id)
    {
        if (($model = JmRepair::findOne($id)) !== null) {
            if (($model = JmRepairClaim::find()->where(['jm_repair_id'=>$id])->all()) !== null) {
                return $model;
            } else {
                return [];
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAddRepairDetail()
    {
        $dataPost             = Yii::$app->request->post();
        $model                = new JmRepairDetail();
        $model->jm_repair_id  = $dataPost['repair_id'];
        $model->repair_name   = $dataPost['repair_name'];
        // $model->repair_price  = $dataPost['repair_price'];
        $model->repair_remark = $dataPost['repair_remark'];
        if ($model->save()) {
            $listRepairDetail = $this->findListRepairDetail($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-detail', [
                'listRepairDetail' => $listRepairDetail,
            ]);
        }
    }

    public function actionDeleteRepairDetail()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairDetail::findOne($dataPost['id']);
        $repair_id = $model->jm_repair_id;
        if ($model->delete()) {
            $listRepairDetail = $this->findListRepairDetail($repair_id);
            return $this->renderPartial('_list-repair-detail', [
                'listRepairDetail' => $listRepairDetail,
            ]);
        }
    }

    public function actionAddRepairPart()
    {
        $dataPost             = Yii::$app->request->post();
        $model                = new JmRepairPart();
        $model->jm_repair_id  = $dataPost['repair_id'];
        $model->part_name     = $dataPost['part_name'];
        $model->part_type     = $dataPost['part_type'];
        $model->check_pic     = $dataPost['check_pic'];
        $model->part_owner    = $dataPost['part_owner'];
        $model->part_return   = $dataPost['part_return'];
        // $model->part_price = $dataPost['part_price'];
        $model->part_remark   = $dataPost['part_remark'];
        if ($model->save()) {
            $listRepairPart = $this->findListRepairPart($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-part', [
                'listRepairPart' => $listRepairPart,
            ]);
        }
    }

    public function actionDeleteRepairPart()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairPart::findOne($dataPost['id']);
        $repair_id = $model->jm_repair_id;
        if ($model->delete()) {
            $listRepairPart = $this->findListRepairPart($repair_id);
            return $this->renderPartial('_list-repair-part', [
                'listRepairPart' => $listRepairPart,
            ]);
        }
    }

    public function actionAddRepairClaim()
    {
        $dataPost            = Yii::$app->request->post();
        $model               = new JmRepairClaim();
        $model->jm_repair_id = $dataPost['repair_id'];
        $model->policy_no    = $dataPost['policy_no'];
        $model->claim_no     = $dataPost['claim_no'];
        $model->repair_amt   = $dataPost['repair_amt'];
        $model->part_amt     = $dataPost['part_amt'];
        if ($model->save()) {
            $listRepairClaim = $this->findListRepairClaim($dataPost['repair_id']);
            return $this->renderPartial('_list-repair-claim', [
                'listRepairClaim' => $listRepairClaim,
            ]);
        }
    }

    public function actionDeleteRepairClaim()
    {
        $dataPost  = Yii::$app->request->post();
        $model     = JmRepairClaim::findOne($dataPost['id']);
        $repair_id = $model->jm_repair_id;
        if ($model->delete()) {
            $listRepairClaim = $this->findListRepairClaim($repair_id);
            return $this->renderPartial('_list-repair-claim', [
                'listRepairClaim' => $listRepairClaim,
            ]);
        }
    }

    public function actionListOrderJob($id)
    {
        $model = $this->findModel($id);
        // return $this->render('_print-list-order-job', ['model' => $model]);
        $content      = $this->renderPartial('_print-list-order-job', ['model' => $model]);
        $strTitleName = 'ใบสั่งงาน';
        $strFileName  = 'list_order_'.$model->job_no;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' =>Pdf::FORMAT_A4,
            'marginLeft' => 3,
            'marginRight' => 2,
            'marginTop' => 1,
            'marginBottom' => false,
            'marginHeader' => false,
            'marginFooter' => 1,
            // portrait orientation //ORIENT_LANDSCAPE, ORIENT_PORTRAIT
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@backend/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
             // set mPDF properties on the fly
            'options' => [
                'title' => $strTitleName,
            ],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>false,
                'SetFooter'=>[$model->job_no.' [{PAGENO}]'],
            ],
            'filename' => $strFileName.'.pdf',
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionSaveReceiveMoney()
    {
        $dataPost                = Yii::$app->request->post();
        $model                   = JmRepair::findOne($dataPost['id']);
        $model->receive_money_at = date('Y-m-d H:i:s');
        $model->receive_money_by = Yii::$app->user->getId();
        if ($model->save()) {
            return true;
        } else {
            return "<pre>".$model->getErrors()."</pre>";
        }
    }
}
