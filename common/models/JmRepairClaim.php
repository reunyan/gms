<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jm_repair_claim".
 *
 * @property integer $id
 * @property integer $jm_repair_id
 * @property string $policy_no
 * @property string $notify_no
 * @property string $claim_no
 * @property string $repair_amt
 * @property string $part_amt
 * @property integer $billing_status
 * @property string $billing_date
 * @property string $billing_at
 * @property string $billing_by
 * @property string $receive_date
 * @property string $receive_at
 * @property string $receive_by
 * @property string $invoice_no
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $before_billing_date
 * @property string $billing_no
 * @property string $isactive
 *
 * @property JmRepair $jmRepair
 */
class JmRepairClaim extends \yii\db\ActiveRecord
{
    public $sum_repair_amt;
    public $sum_part_amt;
    public $insurance_id;
    public $insurance;
    public $count_claim;
    public $process_date;
    public $select_process_status;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jm_repair_claim';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jm_repair_id'], 'required'],
            [['jm_repair_id', 'billing_status', 'created_by', 'updated_by', 'billing_by', 'receive_by'], 'integer'],
            [['repair_amt', 'part_amt'], 'number'],
            [['billing_date', 'billing_at', 'receive_date', 'receive_at', 'created_at', 'updated_at', 'first_claim_amt', 'before_billing_date', 'select_process_status', 'isactive'], 'safe'],
            [['policy_no', 'notify_no', 'claim_no'], 'string', 'max' => 50],
            [['invoice_no', 'billing_no'], 'string', 'max' => 20],
            [['jm_repair_id'], 'exist', 'skipOnError' => true, 'targetClass' => JmRepair::className(), 'targetAttribute' => ['jm_repair_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'jm_repair_id'    => 'รายการ',
            'policy_no'       => 'เลขที่กรมธรรม์',
            'notify_no'       => 'เลขที่รับแจ้ง',
            'claim_no'        => 'เลขที่เคลม',
            'repair_amt'      => 'จำนวนเงินค่าแรง',
            'part_amt'        => 'จำนวนเงินอะไหล่',
            'created_at'      => Yii::t('app','Created At'),
            'created_by'      => Yii::t('app','Created By'),
            'updated_at'      => Yii::t('app','Updated At'),
            'updated_by'      => Yii::t('app','Updated By'),
            'billing_status'  => 'สถานะวางบิล',
            'billing_date'    => 'วันที่วางบิล',
            'billing_at'      => 'วันที่กดวางบิล',
            'billing_by'      => 'ผู้กดวางบิล',
            'receive_date'    => 'วันที่รับเงิน',
            'receive_at'      => 'วันที่กดรับเงิน',
            'receive_by'      => 'ผู้กดรับเงิน',
            'invoice_no'      => 'เลขที่ Invoice',
            'insurance_id'    => 'บริษัทประกันภัย',
            'insurance'       => 'บริษัทประกันภัย',
            'sum_repair_amt'  => 'ค่าแรงรวม',
            'sum_part_amt'    => 'ค่าอะไหล่รวม',
            'count_claim'     => 'จำนวนเคลม',
            'first_claim_amt' => 'ค่า Ex.',
            'billing_no'      => 'เลขที่ใบแจ้งหนี้',
            'before_billing_date'   => 'วันที่แจ้งหนี้',
            'process_date'    => 'วันที่',
            'select_process_status' => 'สถานะวางบิล',
            'isactive' => 'สถานะใช้งาน',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJmRepair()
    {
        return $this->hasOne(JmRepair::className(), ['id' => 'jm_repair_id']);
    }

    public function getBillingStatus()
    {
        return $this->hasOne(TuRef::className(), ['ref_value' => 'billing_status'])->andWhere(['ref_type'=>'BILLING'])->andWhere(['ref_code'=>'STATUS']);
    }

    public function getReceiveBy()
    {
        return $this->hasOne(TuUser::className(), ['id' => 'receive_by']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $modelOld = JmRepairClaim::find()->where(['id'=>$this->id])->one();

            if (!empty($modelOld) && ($modelOld->billing_status == 1 || $modelOld->billing_status == 4) && $this->billing_status == 2 && empty($this->invoice_no)) {
                $this->invoice_no   = Yii::$app->Utilities->getInvoiceNumber();
            }

            if (!empty($modelOld) && $modelOld->billing_status == 1 && $this->billing_status == 4 && empty($this->billing_no)) {
                $this->billing_no   = Yii::$app->Utilities->getBillingNumber();
            }

            $this->billing_date         = (!empty($this->billing_date))?date('Y-m-d', strtotime($this->billing_date)):null;
            $this->receive_date         = (!empty($this->receive_date))?date('Y-m-d', strtotime($this->receive_date)):null;
            $this->before_billing_date  = (!empty($this->before_billing_date))?date('Y-m-d', strtotime($this->before_billing_date)):null;

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->billing_date         = (!empty($this->billing_date))?date('d-m-Y', strtotime($this->billing_date)):null;
        $this->billing_at           = (!empty($this->billing_at))?date('d-m-Y H:i', strtotime($this->billing_at)):null;
        $this->receive_date         = (!empty($this->receive_date))?date('d-m-Y', strtotime($this->receive_date)):null;
        $this->receive_at           = (!empty($this->receive_at))?date('d-m-Y H:i', strtotime($this->receive_at)):null;
        $this->before_billing_date  = (!empty($this->before_billing_date))?date('d-m-Y', strtotime($this->before_billing_date)):null;

        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $modelRepair = JmRepair::findOne($this->jm_repair_id);
        if (!empty($modelRepair)) {
            $intSumFirstclaimAmt          = JmRepairClaim::find()->where(['jm_repair_id'=>$this->jm_repair_id])->sum('first_claim_amt');
            $modelRepair->first_claim_amt =  $intSumFirstclaimAmt;
            $modelRepair->save();
        }
    }
}
