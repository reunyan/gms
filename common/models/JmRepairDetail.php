<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jm_repair_detail".
 *
 * @property integer $id
 * @property integer $jm_repair_id
 * @property string $repair_name
 * @property string $repair_price
 * @property string $repair_remark
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class JmRepairDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jm_repair_detail';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jm_repair_id', 'created_by', 'updated_by'], 'integer'],
            [['repair_price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['repair_name'], 'string', 'max' => 150],
            [['repair_remark'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'jm_repair_id'  => 'ข้อมูลซ่อม',
            'repair_name'   => 'รายการซ่อม',
            'repair_price'  => 'ราคา',
            'repair_remark' => 'หมายเหตุ',
            'created_at'    => Yii::t('app','Created At'),
            'created_by'    => Yii::t('app','Created By'),
            'updated_at'    => Yii::t('app','Updated At'),
            'updated_by'    => Yii::t('app','Updated By'),
        ];
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->attribute       = $this->attribute;
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */
}
