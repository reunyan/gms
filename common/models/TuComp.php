<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tu_comp".
 *
 * @property integer $id
 * @property string $comp_name
 * @property string $comp_short_name
 * @property string $comp_name_en
 * @property string $comp_short_name_en
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 */
class TuComp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tu_comp';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['comp_name', 'comp_name_en'], 'string', 'max' => 150],
            [['comp_short_name', 'comp_short_name_en'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'comp_name'          => 'Comp Name',
            'comp_short_name'    => 'Comp Short Name',
            'comp_name_en'       => 'Comp Name En',
            'comp_short_name_en' => 'Comp Short Name En',
            'created_at'         => Yii::t('app','Created At'),
            'created_by'         => Yii::t('app','Created By'),
            'updated_at'         => Yii::t('app','Updated At'),
            'updated_by'         => Yii::t('app','Updated By'),
        ];
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->attribute       = $this->attribute;
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */
}
