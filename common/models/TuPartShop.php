<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tu_part_shop".
 *
 * @property int $id
 * @property string $shop_name
 * @property string $address
 * @property string $tax_no
 * @property string $contact_name
 * @property string $contact_telno
 * @property string $isactive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class TuPartShop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tu_part_shop';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['shop_name'], 'string', 'max' => 150],
            [['address'], 'string', 'max' => 250],
            [['tax_no'], 'string', 'max' => 20],
            [['contact_name'], 'string', 'max' => 200],
            [['contact_telno'], 'string', 'max' => 50],
            [['isactive'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'shop_name' => Yii::t('app', 'Shop Name'),
            'address' => Yii::t('app', 'Address'),
            'tax_no' => Yii::t('app', 'Tax No'),
            'contact_name' => Yii::t('app', 'Contact Name'),
            'contact_telno' => Yii::t('app', 'Contact Telno'),
            'isactive' => Yii::t('app', 'Isactive'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }
}
