<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "jm_repair_part".
 *
 * @property integer $id
 * @property integer $jm_repair_id
 * @property string $part_name
 * @property integer $part_type
 * @property string $part_price
 * @property string $part_remark
 * @property string $check_pic
 * @property integer $part_owner
 * @property string $part_return
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property JmRepair $jmRepair
 */
class JmRepairPart extends \yii\db\ActiveRecord
{
    public $part_type_name;
    public $check_pic_desc;
    public $part_owner_name;
    public $part_return_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jm_repair_part';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jm_repair_id', 'part_type', 'part_owner', 'created_by', 'updated_by'], 'integer'],
            [['part_price'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['part_name'], 'string', 'max' => 150],
            [['part_remark'], 'string', 'max' => 250],
            [['check_pic', 'part_return'], 'string', 'max' => 1],
            [['jm_repair_id'], 'exist', 'skipOnError' => true, 'targetClass' => JmRepair::className(), 'targetAttribute' => ['jm_repair_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'jm_repair_id' => 'รายการ',
            'part_name'    => 'รายการอะไหล่',
            'part_type'    => 'ประเภท',
            'part_price'   => 'ราคา',
            'part_remark'  => 'หมายเหตุ',
            'check_pic'    => 'รูปเทียบ',
            'part_owner'   => 'ผู้จัด',
            'part_return'  => 'คืนซาก',
            'created_at'   => Yii::t('app','Created At'),
            'created_by'   => Yii::t('app','Created By'),
            'updated_at'   => Yii::t('app','Updated At'),
            'updated_by'   => Yii::t('app','Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJmRepair()
    {
        return $this->hasOne(JmRepair::className(), ['id' => 'jm_repair_id']);
    }

    public function afterFind()
    {
        $this->part_type_name   = (!empty($this->part_type))?Yii::$app->Utilities->getDescReferance('PART', 'PARTTYPE', $this->part_type):'';
        $this->check_pic_desc   = (!empty($this->check_pic))?Yii::$app->Utilities->arrayCheckPic()[$this->check_pic]:'';
        $this->part_owner_name  = (!empty($this->part_owner))?Yii::$app->Utilities->getDescReferance('PART', 'OWNER', $this->part_owner):'';
        $this->part_return_name = (!empty($this->part_return))?Yii::$app->Utilities->getDescReferance('PART', 'RETURN', $this->part_return):'';

        parent::afterFind();
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->attribute       = $this->attribute;
    }
    *
    */
}
