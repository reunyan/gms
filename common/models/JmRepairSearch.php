<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

use common\models\JmRepair;

/**
 * JmRepairSearch represents the model behind the search form about `common\models\JmRepair`.
 */
class JmRepairSearch extends JmRepair
{
    public $contact_repair_date_start;
    public $contact_repair_date_end;
    public $appoint_repair_date_start;
    public $appoint_repair_date_end;
    public $appoint_sent_date_start;
    public $appoint_sent_date_end;
    public $in_repair_date_start;
    public $in_repair_date_end;
    public $sent_car_date_start;
    public $sent_car_date_end;
    public $payment_amt;
    public $status_receive_money;
    public $search_contact_repair_name;
    public $data_year;
    public $data_month;
    public $data_date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'car_license_province', 'car_brand_id', 'car_model_id', 'car_series_id', 'car_color_id', 'insurance_id', 'insurance_type', 'car_claim_type', 'in_repair_emp', 'in_repair_type', 'sent_car_emp', 'created_by', 'updated_by'], 'integer'],
            [['job_no', 'car_license_no', 'contact_repair_date', 'contact_repair_firstname', 'contact_repair_lastname', 'contact_repair_tel', 'appoint_repair_date', 'in_repair_date', 'in_repair_firstname', 'in_repair_lastname', 'in_repair_tel', 'appoint_sent_date', 'sent_car_date', 'sent_car_firstname', 'sent_car_lastname', 'sent_car_tel', 'created_at', 'updated_at', 'status_repair', 'status_invoice', 'contact_repair_date_start', 'contact_repair_date_end', 'appoint_repair_date_start', 'appoint_repair_date_end', 'appoint_sent_date_start', 'appoint_sent_date_end', 'in_repair_date_start', 'in_repair_date_end', 'payment_amt', 'first_claim_amt', 'sent_car_date_start', 'sent_car_date_end', 'status_receive_money', 'receive_money_at', 'receive_money_by', 'data_date', 'data_month', 'data_year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JmRepair::find();

        // add conditions that should always apply here

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // if (empty($params)) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     $query->where('0=1');
        //     return $dataProvider;
        // }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'                   => $this->id,
            'car_license_province' => $this->car_license_province,
            'car_brand_id'         => $this->car_brand_id,
            'car_model_id'         => $this->car_model_id,
            'car_series_id'        => $this->car_series_id,
            'car_color_id'         => $this->car_color_id,
            'insurance_id'         => $this->insurance_id,
            'insurance_type'       => $this->insurance_type,
            'car_claim_type'       => $this->car_claim_type,
            'contact_repair_date'  => $this->contact_repair_date,
            'appoint_repair_date'  => $this->appoint_repair_date,
            'in_repair_emp'        => $this->in_repair_emp,
            'in_repair_date'       => $this->in_repair_date,
            'in_repair_type'       => $this->in_repair_type,
            'appoint_sent_date'    => $this->appoint_sent_date,
            'sent_car_emp'         => $this->sent_car_emp,
            'sent_car_date'        => $this->sent_car_date,
            'created_at'           => $this->created_at,
            'created_by'           => $this->created_by,
            'updated_at'           => $this->updated_at,
            'updated_by'           => $this->updated_by,
            'status_repair'        => $this->status_repair,
            'status_invoice'       => $this->status_invoice,
            'first_claim_amt'       => $this->first_claim_amt,
        ]);

        $query->andFilterWhere(['like', 'job_no', $this->job_no])
            ->andFilterWhere(['like', 'car_license_no', $this->car_license_no])
            ->andFilterWhere(['like', 'contact_repair_firstname', $this->contact_repair_firstname])
            ->andFilterWhere(['like', 'contact_repair_lastname', $this->contact_repair_lastname])
            ->andFilterWhere(['like', 'contact_repair_tel', $this->contact_repair_tel])
            ->andFilterWhere(['like', 'in_repair_firstname', $this->in_repair_firstname])
            ->andFilterWhere(['like', 'in_repair_lastname', $this->in_repair_lastname])
            ->andFilterWhere(['like', 'in_repair_tel', $this->in_repair_tel])
            ->andFilterWhere(['like', 'sent_car_firstname', $this->sent_car_firstname])
            ->andFilterWhere(['like', 'sent_car_lastname', $this->sent_car_lastname])
            ->andFilterWhere(['like', 'sent_car_tel', $this->sent_car_tel])
            ->andFilterWhere(['OR', ['like', 'sent_car_firstname', $this->search_contact_repair_name], ['like', 'sent_car_lastname', $this->search_contact_repair_name]]);

        $date_in_repair_date_start    = (!empty($this->in_repair_date_start))?date('Y-m-d', strtotime($this->in_repair_date_start)):null;
        $date_in_repair_date_end      = (!empty($this->in_repair_date_end))?date('Y-m-d', strtotime($this->in_repair_date_end)):null;
        $date_sent_car_date_start     = (!empty($this->sent_car_date_start))?date('Y-m-d', strtotime($this->sent_car_date_start)):null;
        $date_sent_car_date_end       = (!empty($this->sent_car_date_end))?date('Y-m-d', strtotime($this->sent_car_date_end)):null;
        $date_appoint_sent_date_start = (!empty($this->appoint_sent_date_start))?date('Y-m-d', strtotime($this->appoint_sent_date_start)):null;
        $date_appoint_sent_date_end   = (!empty($this->appoint_sent_date_end))?date('Y-m-d', strtotime($this->appoint_sent_date_end)):null;
        $date_contact_repair_date_start   = (!empty($this->contact_repair_date_start))?date('Y-m-d', strtotime($this->contact_repair_date_start)):null;
        $date_contact_repair_date_end   = (!empty($this->contact_repair_date_end))?date('Y-m-d', strtotime($this->contact_repair_date_end)):null;
        $date_appoint_repair_date_start   = (!empty($this->appoint_repair_date_start))?date('Y-m-d', strtotime($this->appoint_repair_date_start)):null;
        $date_appoint_repair_date_end   = (!empty($this->appoint_repair_date_end))?date('Y-m-d', strtotime($this->appoint_repair_date_end)):null;

        $query->andFilterWhere(['between', 'in_repair_date', $date_in_repair_date_start, $date_in_repair_date_end]);
        $query->andFilterWhere(['between', 'sent_car_date', $date_sent_car_date_start, $date_sent_car_date_end]);
        $query->andFilterWhere(['between', 'appoint_sent_date', $date_appoint_sent_date_start, $date_appoint_sent_date_end]);
        $query->andFilterWhere(['between', 'contact_repair_date', $date_contact_repair_date_start, $date_contact_repair_date_end]);
        $query->andFilterWhere(['between', 'appoint_repair_date', $date_appoint_repair_date_start, $date_appoint_repair_date_end]);

        if (!empty($this->payment_amt)) {
            switch ($this->payment_amt) {
                case 1: //ONLY first_claim_amt
                    $query->andWhere(['IS NOT', 'first_claim_amt', NULL]);
                break;

                case 2: //ONLY other_amt
                    $query->andWhere(['IS NOT', 'other_amt', NULL]);
                break;

                case 3: //first_claim_amt AND other_amt
                    $query->andWhere('(first_claim_amt IS NOT NULL OR other_amt IS NOT NULL)');
                break;
            }
        }

        if (!empty($this->status_receive_money)) {
            switch ($this->status_receive_money) {
                case 1: //ONLY received
                    $query->andWhere(['IS NOT', 'receive_money_at', NULL]);
                break;

                case 2: //ONLY not receive
                    $query->andWhere(['IS', 'receive_money_at', NULL]);
                break;
            }

        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ],
            'sort' => [
                'defaultOrder' => [
                    'job_no' => SORT_DESC,
                ]
            ],
        ]);

        return $dataProvider;
    }

    public function queryCarInRepair()
    {
        // $queryCarIn = JmRepair::find()->select(['car_license_no'])->where(['status_repair'=>[2,3,4,5]])->distinct();
        $model      = JmRepair::find()
                    ->where(['IS NOT', 'appoint_repair_date',null])
                    ->andWhere(['status_repair'=>1])
                    // ->andWhere(['NOT IN', 'car_license_no', $queryCarIn])
                    ->orderBy(['appoint_repair_date'=>SORT_ASC])->all();
        return $model;
    }

    public function queryCarAppointSend()
    {
        $model       = JmRepair::find()
                        // ->where(['>=', 'appoint_sent_date', date('Y-m-d')])
                        ->andWhere(['status_repair'=>[2,3]])
                        ->orderBy(['appoint_repair_date'=>SORT_ASC])
                        ->all();
        return $model;
    }

    public function countCarAppointInRepair()
    {
        $data  = [];
        $array = [];
        $query = new Query();
        $query->select(['MONTH(appoint_repair_date) as month_in, COUNT(appoint_repair_date) as num_in']);
        $query->from('jm_repair');
        $query->andWhere(['IS NOT', 'appoint_repair_date', null]);
        $query->andFilterWhere(['YEAR(appoint_repair_date)' => $this->data_year]);
        $query->andFilterWhere(['MONTH(appoint_repair_date)' => $this->data_month]);
        $query->groupBy(['MONTH(appoint_repair_date)']);
        $query->orderBy(['MONTH(appoint_repair_date)'=>SORT_ASC]);

        foreach ($query->all() as $key => $value) {
            $data[$value['month_in']] = intval($value['num_in']);
        }

        for ($i=0; $i <= 11 ; $i++) {
            $k = $i+1;
            $array[$i] = (!empty($data[$k]))?$data[$k]:0;
        }

        return $array;
    }

    public function countCarAppointOutRepair()
    {
        $data  = [];
        $array = [];
        $query = new Query();
        $query->select(['MONTH(appoint_sent_date) as month_out, COUNT(appoint_sent_date) as num_out']);
        $query->from('jm_repair');
        $query->andWhere(['IS NOT', 'appoint_sent_date', null]);
        $query->andFilterWhere(['YEAR(appoint_sent_date)' => $this->data_year]);
        $query->andFilterWhere(['MONTH(appoint_sent_date)' => $this->data_month]);
        $query->groupBy(['MONTH(appoint_sent_date)']);
        $query->orderBy(['MONTH(appoint_sent_date)'=>SORT_ASC]);

        foreach ($query->all() as $key => $value) {
            $data[$value['month_out']] = intval($value['num_out']);
        }

        for ($i=0; $i <= 11 ; $i++) {
            $k = $i+1;
            $array[$i] = (!empty($data[$k]))?$data[$k]:0;
        }

        return $array;
    }

    public function countCarInRepair()
    {
        $data  = [];
        $array = [];
        $query = new Query();
        $query->select(['MONTH(in_repair_date) as month_in, COUNT(in_repair_date) as num_in']);
        $query->from('jm_repair');
        $query->andWhere(['IS NOT', 'in_repair_date', null]);
        $query->andFilterWhere(['YEAR(in_repair_date)' => $this->data_year]);
        $query->andFilterWhere(['MONTH(in_repair_date)' => $this->data_month]);
        $query->groupBy(['MONTH(in_repair_date)']);
        $query->orderBy(['MONTH(in_repair_date)'=>SORT_ASC]);

        foreach ($query->all() as $key => $value) {
            $data[$value['month_in']] = intval($value['num_in']);
        }

        for ($i=0; $i <= 11 ; $i++) {
            $k = $i+1;
            $array[$i] = (!empty($data[$k]))?$data[$k]:0;
        }

        return $array;
    }

    public function countCarOutRepair()
    {
        $data  = [];
        $array = [];
        $query = new Query();
        $query->select(['MONTH(sent_car_date) as month_out, COUNT(sent_car_date) as num_out']);
        $query->from('jm_repair');
        $query->andWhere(['IS NOT', 'sent_car_date', null]);
        $query->andFilterWhere(['YEAR(sent_car_date)' => $this->data_year]);
        $query->andFilterWhere(['MONTH(sent_car_date)' => $this->data_month]);
        $query->groupBy(['MONTH(sent_car_date)']);
        $query->orderBy(['MONTH(sent_car_date)'=>SORT_ASC]);

        foreach ($query->all() as $key => $value) {
            $data[$value['month_out']] = intval($value['num_out']);
        }

        for ($i=0; $i <= 11 ; $i++) {
            $k = $i+1;
            $array[$i] = (!empty($data[$k]))?$data[$k]:0;
        }

        return $array;
    }

    public function countCarStatusRepairDaily($repair_type)
    {
        $date  = date('Y-m-d');
        $query = new Query();
        $query->select(['id']);
        $query->from('jm_repair');
        switch (intval($repair_type)) {
            case 1:
                $query->andWhere(['DATE(appoint_repair_date)' => $date]);
            break;

            case 2:
                $query->andWhere(['DATE(appoint_sent_date)' => $date]);
            break;

            case 3:
                // $query->andWhere(['DATE(in_repair_date)' => $date]);
                $query->andWhere(['in_repair_type'=>1]);
                $query->andWhere(['IN','status_repair',[2,3]]);//2 = เข้าซ่อม , 3=ระหว่างซ่อม
            break;

            case 4:
                $query->andWhere(['DATE(sent_car_date)' => $date]);
            break;
        }

        return $query->count();
    }
}
