<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tu_insure".
 *
 * @property integer $id
 * @property string $insure_name
 * @property string $insure_addr
 * @property string $isactive
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 */
class TuInsure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tu_insure';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insure_name'], 'required'],
            [['insure_addr'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['insure_name'], 'string', 'max' => 200],
            [['isactive'], 'string', 'max' => 1],
            [['created_by', 'updated_by'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'insure_name' => 'ชื่อบริษัทประกันภัย',
            'insure_addr' => 'ที่อยู่',
            'isactive'    => Yii::t('app','Isactive'),
            'created_at'  => Yii::t('app','Created At'),
            'created_by'  => Yii::t('app','Created By'),
            'updated_at'  => Yii::t('app','Updated At'),
            'updated_by'  => Yii::t('app','Updated By'),
        ];
    }

    /**
    **
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->attribute       = $this->attribute;
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->attribute       = $this->attribute;
    }

    public function afterFind()
    {
        $this->attribute       = $this->attribute;

        parent::afterFind();
    }
    *
    */
}
