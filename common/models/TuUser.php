<?php
namespace common\models;

use Yii;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tu_user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $nickname
 * @property int $comp_id
 * @property int $comp_br_id
 * @property int $dep_id
 * @property int $role_id
 * @property string $isactive
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class TuUser extends ActiveRecord implements IdentityInterface
{
    public $full_name;
    public $auth_key;
    public $password_hash;
    public $password_reset_token;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tu_user';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => Yii::$app->user->getId(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['comp_id', 'comp_br_id', 'dep_id', 'role_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'isactive'], 'safe'],
            [['username'], 'string', 'max' => 30],
            [['password','auth_key'], 'string', 'max' => 50],
            [['firstname', 'lastname', 'password_hash', 'password_reset_token'], 'string', 'max' => 150],
            [['nickname'], 'string', 'max' => 100],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => 'Username',
            'password' => 'Password',
            'full_name' => 'ชื่อ-นามสกุล',
            'firstname' => 'ชื่อ',
            'lastname' => 'นามสกุล',
            'nickname' => 'ชื่อเล่น',
            'comp_id' => 'บริษัท',
            'comp_br_id' => 'สาขา',
            'dep_id' => 'หน่วยงาน',
            'role_id' => 'หน้าที่',
            'isactive' => Yii::t('app', 'Isactive'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Update By'),
        ];
    }

    public function getComp()
    {
        return $this->hasOne(TuComp::className(), ['id' => 'comp_id']);
    }

    public function getCompBr()
    {
        return $this->hasOne(TuCompBr::className(), ['id' => 'comp_br_id']);
    }

    public function getDep()
    {
        return $this->hasOne(TuDep::className(), ['id' => 'dep_id']);
    }

    public function getRole()
    {
        return $this->hasOne(TuRole::className(), ['id' => 'role_id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert))
        {
            if ($this->isNewRecord) {
                $this->password = md5($this->password);
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        $this->full_name = $this->firstname.' '.$this->lastname.'('.$this->nickname.')';

        parent::afterFind();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'isactive' => 'Y']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'isactive' => 'Y']);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return ($password == $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
